function clusterprojplot(spikes)


%Plotting all data projections into pairs of 1st ndim PCA dimensions
figure
set(gcf, 'Position', [250 150 1000 800])
for ind1=1:spikes.info.pca.ndim-1
	for ind2=ind1+1:spikes.info.pca.ndim
		subplot(spikes.info.pca.ndim,spikes.info.pca.ndim,(ind1-1)*spikes.info.pca.ndim+ind2)
		hold on
		for ind3=1:max(spikes.assigns)
			plot(spikes.waveforms_pca(spikes.assigns==ind3,ind2), spikes.waveforms_pca(spikes.assigns==ind3,ind1), '.', 'MarkerSize', 6, 'Color', spikes.info.kmeans.colors(ind3,:))
		end
	end
end
for ind1=1:spikes.info.pca.ndim
	subplot(spikes.info.pca.ndim,spikes.info.pca.ndim,(ind1-1)*(spikes.info.pca.ndim+1)+1)
	[yv,xv]=hist(spikes.waveforms_pca(:,ind1),20);
	if ind1~=spikes.info.pca.ndim, plot(yv,xv, 'k'), else plot(xv,yv, 'k'), end
end

%Make color legend for cluster labels
subplot(spikes.info.pca.ndim,spikes.info.pca.ndim,spikes.info.pca.ndim+1)
for ind=1:max(spikes.assigns), hold on, plot(0, ind, '.', 'MarkerSize', 12, 'Color', spikes.info.kmeans.colors(ind,:)), end
aa=get(gca, 'Position'); aa(3)=aa(3)/4; aa(4)=(aa(4)+aa(2))-.2; aa(2)=.2;
ylim([.5 max(spikes.assigns)+.5]), ylabel('Cluster #')
set(gca, 'Position', aa, 'xtick', [])
