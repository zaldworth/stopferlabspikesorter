function [fres1,RPV]=contam_er(spikes,ndx)

% fres1=contam_er(spikes,ndx)
%
% Function to examine false positives due to contaminating spikes in refractory period of cluster

for ind=1:length(ndx), [ev(1,ind),lb,ub,RPV(1,ndx(ind))] = ss_rpv_contamination_z(spikes,ndx(ind));, end

fres1=abs(ev)';