function [zs3,indices,pos1]=pickfromsupermod(subs_dj,supmod,sup_spks,ind1,nch,chanleng,nclusts,v,dinv,r);


%wavtmp=ones([1 1 1 nch*chanleng]);
%wavtmp(1,1,:,:)=subs_dj(sup_spks(ind1),:);
%wavtmp2=repmat(wavtmp,[nclusts nclusts 2*chanleng-1 1]);
%res1=wavtmp2-supmod; %4-D residual
%ztmp=zeros(nclusts,nclusts,2*chanleng-1); %initialize variable
%res2=zeros(nclusts,nclusts,2*chanleng-1,r); %initialize variable
%for ind2=1:nclusts
%	for ind3=1:nclusts
%		if ind2==ind3 %can't have same unit account for both waveforms
%			ztmp(ind2,ind3,:)=inf*ones(1,2*chanleng-1); 
%			res2(ind2,ind3,:,:)=inf*ones(2*chanleng-1,r); 
%		else %can't have same unit account for both waveforms
%			res2(ind2,ind3,:,:)=squeeze(res1(ind2,ind3,:,:))*v; %subtract mean and project into pca space
%		end %can't have same unit account for both waveforms
%		for ind4=1:2*chanleng-1
%			ztmp(ind2,ind3,ind4)=squeeze(res2(ind2,ind3,ind4,:))'*dinv*squeeze(res2(ind2,ind3,ind4,:));
%		end
%	end
%end
%
%[restmp1,shftndx]=min(ztmp,[],3);
%[restmp2,clst2ndx]=min(restmp1,[],2);
%[restmp3,clst1ndx]=min(restmp2);
%indices=[clst1ndx clst2ndx(clst1ndx) shftndx(clst1ndx,clst2ndx(clst1ndx))];
%zs3=restmp3;




wavtmp2=repmat(subs_dj(sup_spks(ind1),:),size(supmod,1),1);
res1=wavtmp2-supmod;
res1=res1*v;
%ztmp=zeros(size(supmod,1),1);
%for ind=1:size(supmod,1)
%	ztmp(ind)=res1(ind,:)*dinv*res1(ind,:)';
%end
ztmp=sum((res1*dinv).*res1,2);
[zs3,pos1]=min(ztmp);
[indices(3),rem]=mod2(pos1,nclusts*nclusts);
[indices(2),rem]=mod2(rem,nclusts);
[indices(1)]=mod2(rem,1);
indices(indices==0)=nclusts;

