function funout=gmmfilt(mix,signal,gmmfun)
% function funout=gmmfilt(mix,signal,gmmfun)
% use gmm output functions (activ, post, prob) as filters on time series

% use buffer to change the time series to a list of samples?
% ? how to pass gmmfun? as @fun?


% for now, assume signal is 1d...
% also at some point - return final state of buffer, and pass initial state
% so that it can be used for block filtering; when otherwise the buffered
% signal will be too large to fit in memory.
samples = buffer(signal, mix.nin, mix.nin-1);
funout = feval(gmmfun,mix,samples')';