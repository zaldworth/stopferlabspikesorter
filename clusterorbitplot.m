function clusterorbitplot(spikes,opts)

% clusterorbitplot(spikes,opts)
%
% Function for manually examining clustering of waveforms in 3-d PCA space.  Allows user to
% interactively rotate the clusters by clicking the mouse.  Parameters of rotation can be
% changed by pressing keyboard button rather than mouse-clicking.


%set defaults
if nargin<2||isempty(opts), opts.plotlab='nonclust'; end
if ~isfield(opts, 'nd1'), opts.nd1=1; end
if ~isfield(opts, 'nd2'), opts.nd2=2; end
if ~isfield(opts, 'nd3'), opts.nd3=3; end
if ~isfield(opts, 'dphi'), opts.dphi=1; end
if ~isfield(opts, 'dthe'), opts.dthe=0; end

%unpack parameters
nd1=opts.nd1;
nd2=opts.nd2;
nd3=opts.nd3;
dphi=opts.dphi;
dthe=opts.dthe;

figure
set(gcf, 'Position', [600 250 750 650])
if strcmp(opts.plotlab, 'nonclust')
	plot3(spikes.waveforms_pca(:,nd1),spikes.waveforms_pca(:,nd2),spikes.waveforms_pca(:,nd3), 'k.', 'MarkerSize', 8)
elseif strcmp(opts.plotlab, 'clust')
	nclusts=opts.nclusts;
	idx=opts.idx;
	cmat=opts.cmat;
	for ind1=1:nclusts
		hold on
		plot3(spikes.waveforms_pca(idx==ind1,nd1),spikes.waveforms_pca(idx==ind1,nd2),spikes.waveforms_pca(idx==ind1,nd3), '.', 'MarkerSize', 8, 'Color', cmat(ind1,:))
	end
else
	error('Incorrect plot label, use either ''nonclust'' or ''clust''');
end

aa(1,:)=get(gca, 'XLim');
aa(2,:)=get(gca, 'YLim');
aa(3,:)=get(gca, 'ZLim');

view(3)
grid on
set(gca, 'CameraViewAngle', 10)
status_orb=1;

while status_orb==1 

	figure(gcf)
	camorbit(dphi,dthe)
	set(gca, 'XLim', aa(1,:), 'YLim', aa(2,:), 'ZLim', aa(3,:), 'CameraViewAngle', 10)
	opt_menu=waitforbuttonpress;

	if (opt_menu==1) % if keyboard is pressed
		disp('Type');
		disp('0	Continue as before ...');
		disp('1	for reverse direction of rotation.');
		disp('2	for changing speed of rotation.');
		disp('3	for changing axis of rotation.');
		disp('4	for free rotation for one revolution then ending program.');
		disp('5	for ending program.');
		opt=input('Option? ');
		if (opt==0) % continue option
		elseif (opt==1) 
			if dphi~=0, dphi=-dphi; end
			if dthe~=0, dthe=-dthe; end
		elseif (opt==2)
			disp('Enter new rotoation step size (in degrees).');
			opt2=input('Option? ');
			if dphi~=0, dphi=opt2; end
			if dthe~=0, dthe=opt2; end
		elseif (opt==3)
			dphi1=dthe;
			dthe=dphi;
			dphi=dphi1;
			clear dphi1
		elseif (opt==4)
			for ind=1:round(360/max(abs([dphi dthe])))
				figure(gcf)
				camorbit(dphi,dthe)
				set(gca, 'XLim', aa(1,:), 'YLim', aa(2,:), 'ZLim', aa(3,:), 'CameraViewAngle', 10)
				pause(.05), figure(gcf)
			end
			status_orb=0;
		elseif (opt==5)
			status_orb=0;
		else disp('Sorry, invalid option; try again.');
		end %if (opt==1) 
	end %if (optmenu==1)
	
	figure(gcf)
end		



%Mac- button 2=shft-cmmd-click; button 3=ctrl-click