function twoD_istac_plot_tot(spikes,ndx)

% twoD_istac_plot_tot(spikes,ndx)
%
% Function to show distributions of waveforms along two top iSTAC dimensions for all pairwise combinations of 
% clusters in 'ndx'.

if nargin<2||isempty(ndx), ndx=1:spikes.params.kmeans_clustersize;, end %default- look at all clusters

figure
set(gcf, 'Position', [125 100 1400 875])
for ind1=1:length(ndx)-1, for ind2=ind1+1:length(ndx)
	mu1=mean(spikes.waveforms_clust{ndx(ind1)})'; A1=cov(spikes.waveforms_clust{ndx(ind1)}); %1st cluster mean & covar
	mu0=mean(spikes.waveforms_clust{ndx(ind2)})'; A0=cov(spikes.waveforms_clust{ndx(ind2)}); %2nd cluster mean & covar
	[vecs, vals, GaussParams] = compiSTAC_AD(mu1,A1,mu0,A0,2); %Just look at top 2-D projection
keyboard
	subplot(length(ndx)-1,length(ndx)-1,(ind1-1)*(spikes.params.kmeans_clustersize-1)+ind2-1)
	plot(spikes.waveforms_clust{ndx(ind1)}*vecs(1,:), spikes.waveforms_clust{ndx(ind1)}*vecs(2,:), '.', 'MarkerSize', 6, 'Color', spikes.info.kmeans.colors(ndx(ind1),:))
	hold on
	plot(spikes.waveforms_pca(spikes.assigns==ind3,ind2), spikes.waveforms_pca(spikes.assigns==ind3,ind1), '.', 'MarkerSize', 6, 'Color', spikes.info.kmeans.colors(ndx(ind2),:))
	if ind1==1, title(['#' num2str(ndx(ind2))]), end
	if ind2==ind1+1, ylabel(['#' num2str(ndx(ind1))]), end
	set(gca, 'XTickLabel', [], 'YTickLabel', [])
end, end


subplot(length(ndx)-1, length(ndx)-1, length(ndx))
for ind1=1:length(ndx), hold on, barh(ind1, 1, .5, 'FaceColor', spikes.info.kmeans.colors(ndx(ind1),:)), end
aa=get(gca, 'Position'); aa(3)=aa(3)/4;
ylim([.5 length(ndx)+.5]), title('Cluster #')
set(gca, 'Position', aa, 'xtick', [], 'ytick', 1:length(ndx), 'yticklabel', ndx)
