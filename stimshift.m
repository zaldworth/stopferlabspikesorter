function newstim=stimshift(stim2,subrange,anchor,sndx)
% function newstim=stimshift(stim2,subrange,anchor,sndx)
% stim2: set of observations, obs x dim x chan
% subrange: the size of the realigned observations to return
% anchor: end point of subrange in original range (column number)
% sndx: list of shift indices, obtained by e.g. dejitter, or realign2
%
% newstim: shifted stim2 in subrange. subrange is needed since observations
% are shifted back and forth according to sndx, and cannot cross the ends
% of available data.

newstim=zeros(size(stim2,1),subrange, size(stim2,3));
for i1=1:length(sndx)   
    newstim(i1,:,:)=stim2(i1, (-subrange:-1)+(anchor+sndx(i1)+1),:);
end
