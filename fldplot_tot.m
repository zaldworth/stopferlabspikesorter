function dsts = fldplot_tot(spikes, ndx)

% fldplot_tot(spikes, ndx)
%
% Function to distributions of waveforms along FLD dimensions for all pairwise combinations of 
% clusters in 'ndx'.

if nargin<2||isempty(ndx), ndx=1:spikes.params.kmeans_clustersize;, end %default- look at all clusters

figure
set(gcf, 'Position', [125 100 1400 875])
for ind1=1:length(ndx)-1, for ind2=ind1+1:length(ndx)
	subplot(length(ndx)-1,length(ndx)-1,(ind1-1)*(length(ndx)-1)+ind2-1)
	d1(ind1,ind2-1)=fldplot(spikes,ndx(ind1),ndx(ind2));
% 	if ind1==1, title(['#' num2str(ndx(ind2))]), end
% 	if ind2==ind1+1, ylabel(['#' num2str(ndx(ind1))]), end
	
	mydist  = d1(ind1,ind2-1);	
	if mydist < 3  
		mycolor = [1 0 0];
	elseif mydist < 4
		mycolor = [1 0.4 0.2];
	elseif mydist < 5
		mycolor = [1 0.8 0.3];
	else
		mycolor = 'none';
	end
	title([num2str(ndx(ind2)) '-' num2str(ndx(ind1)) ', d=' num2str(mydist,'%.1f')],'fontsize',6+10/length(ndx),'backgroundcolor',mycolor); 
	set(gca, 'XTickLabel', [], 'YTickLabel', [])
end, end

for ind=1:length(ndx)
	dsts1(ind,:)=[d1(1:ind-1,max([1 ind-1]))' d1(min([end ind]),ind:end)];
	dsts(ind)=min(dsts1(ind,:));
end

if length(ndx)>2  
	subplot(length(ndx)-1, length(ndx)-1, length(ndx))
	for ind1=1:length(ndx), hold on, barh(ind1, 1, .5, 'FaceColor', spikes.info.kmeans.colors(ndx(ind1),:)), end
	aa=get(gca, 'Position'); aa(3)=aa(3)/4; aa(4)=(aa(4)+aa(2))-.2; aa(2)=.2;
	ylim([.5 length(ndx)+.5]), title('Cluster #')
	for i = 1:length(ndx)
		ylabels{i}= [num2str(ndx(i)) ': ' num2str(dsts(i),'%.1f')];
	end
	set(gca, 'Position', aa, 'xtick', [], 'ytick', 1:length(ndx), 'yticklabel', ylabels)
end

