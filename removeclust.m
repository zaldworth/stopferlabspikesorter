function spikes1=removeclust(spikes,clustndx);

% spikes1=removeclust(spikes,clustndx);
%
% Function to delete selected cluster and associated waveforms/times
% from spikes structure.
%
%

%Delete waveforms and assigns & time stamps, update spikes structure
spikes1=spikes;
idx1=spikes.assigns; %list of original assigns
idx3=find(idx1==clustndx); %list of assigns belonging to cluster to merge

for ind=clustndx+1:spikes.params.kmeans_clustersize %for each cluster after 2nd member of merge
	idx1(idx1==ind)=idx1(idx1==ind)-1; %decrease cluster number by 1
end %for each cluster after 2nd member of merge
idx1(idx3)=[]; %delete assigns
spikes1.assigns=idx1'; %reduced vector of indices assigning spikes to clusters
spikes1.waveforms(idx3,:,:)=[]; %reduced set of waveforms
spikes1.waveforms_pca(idx3,:,:)=[]; %reduced set of projected waveforms
spikes1.waveforms_clust(clustndx)=[]; %reduced set of clustered waveforms
spikes1.waveforms_clust_pca(clustndx)=[]; %reduced set of projected, clustered waveforms
spikes1.unwrapped_times(idx3)=[]; %reduced vector of unwrapped (trial-less) spike times
spikes1.spiketimes(idx3)=[]; %reduced vector of spike times within each trial
spikes1.trials(idx3)=[]; %reduced vector of spike trials
spikes1.files(idx3)=[]; %reduced vector of spike files
spikes1.info.detect.event_channel(idx3)=[]; %reduced indices of which channel is largest per spike

spikes1.labels=[1:spikes.params.kmeans_clustersize-1; ones(1,spikes.params.kmeans_clustersize-1)]'; %Need to look at this to figure out- Cx2 (C=# of clusters)
spikes1.params.kmeans_clustersize=spikes.params.kmeans_clustersize-1; %decreasing cluster number by 1
colormap jet
clrs=colormap;
cndx=round(1:(63/(spikes.params.kmeans_clustersize-1)):64);  % made the same as splitclusts and pouzatsort1 to keep colors consistent
spikes1.info.kmeans.colors=clrs(cndx,:); %color matrix storing color values (1x3) for each cluster

if isfield(spikes1, 'firstsupndx') %If this is an output from classification (instead of an output from model building)
	[jnk,rmvndx1]=intersect(spikes1.firstsupndx, idx3); %first spike of superposition indices belonging to cluster to remove
	[jnk,rmvndx2]=intersect(spikes1.secndsupndx, idx3); %second spike of superpoition indices belonging to cluster to remove
	spikes1.firstsupndx(rmvndx1)=[]; %get rid of these indices
	spikes1.secndsupndx(rmvndx2)=[]; %get rid of these indices
	[jnk,keepndx]=intersect(spikes.unwrapped_times, spikes1.unwrapped_times); %mapping the kept spikes back into the original indices
	[jnk,spikes1.firstsupndx]=intersect(keepndx,spikes1.firstsupndx); %mapping old sup. indices to new values
	[jnk,spikes1.secndsupndx]=intersect(keepndx,spikes1.secndsupndx); %mapping old sup. indices to new values
end %If this is an output from classification (instead of an output from model building)

%plot_clustmodwaves(spikes1);