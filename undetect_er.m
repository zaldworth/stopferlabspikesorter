function fres3=undetect_er(spikes,ndx)

% fres3=undetect_er(spikes,ndx)
%
% Function to examine false negative spikes per cluster, depending on proximity of waveforms to thresholding
% criteria



for ind=1:length(ndx) %for each cluster
	[p(ind),mu(ind),stdev(ind),n(ind,:),x(ind,:)] = ss_undetected_z(spikes,ndx(ind));
%	[p(ind),mu(ind),stdev(ind),n(ind,:),x(ind,:)] = undetected(spikes.waveforms(spikes.assigns==ind,:,:),spikes.params.thresh,spikes.params.detect_method);
end

fres3=p';
%Also make figure

%set up number of columns in figure
if length(ndx)>1&length(ndx)<=5, ncols=1;
elseif length(ndx)>5&length(ndx)<=10, ncols=2;
elseif length(ndx)>10&length(ndx)<=15, ncols=3;
elseif length(ndx)>15&length(ndx)<=20, ncols=4;
else error('Too many (or zero) clusters')
end

nrows=ceil(length(ndx)/ncols);

figure
set(gcf, 'Position', [500 150 900 875])
for ind=1:length(ndx)
	subplot(nrows,ncols,ind)
	h1=bar(x(ind,:), n(ind,:), 1);
	set(h1, 'FaceColor', [.6 .6 .6])
	hold on
	plot(x(ind,:), normpdf(x(ind,:),mu(ind),stdev(ind))*diff(x(ind,1:2))*sum(n(ind,:)), 'r', 'LineWidth', 2)
	title(['Clust # ' num2str(ndx(ind)) ', N=' num2str(size(spikes.waveforms_clust{ndx(ind)},1))], 'Color', spikes.info.kmeans.colors(ndx(ind),:))
	set(gca, 'XColor', spikes.info.kmeans.colors(ndx(ind),:), 'YColor', spikes.info.kmeans.colors(ndx(ind),:))
	if ind>length(ndx)-ncols, xlabel('SD from thresh'), end
	axis tight
end
