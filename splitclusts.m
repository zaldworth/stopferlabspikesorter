function spikes1=splitclusts(spikes,clustndx,clustmeth,nclusts1);

% spikes1=splitclusts(spikes,clustndx,clustmeth,nclusts1);
%
% Function to split selected cluster into two or more clusters, either via kmeans or Gaussian EM
%
% Modified on Nov 19, 2012 by ZNA- can now split into any desired number of clusters
% Modified on Nov 29, 2012 by ZNA- now performs split on PCA calculated just within cluster

rng(0); % initialize random number generator to fixed state

if nargin<3||isempty(clustmeth), clustmeth=spikes.info.clustmeth;, end %default value
if nargin<4||isempty(nclusts1), nclusts1=2; end %default value

oldndx=setdiff(1:spikes.params.kmeans_clustersize, clustndx); %index of original clusters, minus cluster to be split

%Set up new PCAs, project into new space (same dimmensionality as old PCA space)- New 2012/11/29 ZNA
[v,s]=eig(cov(spikes.waveforms_clust_pca{clustndx})); %new PCA space
waveforms_pca=spikes.waveforms_clust_pca{clustndx}*v(:,1:spikes.info.pca.ndim); %spike waveforms projected into pca space

%Do splitting
%nclusts1=2;
randnstate=randn('state'); %get state of random number generator
if strmatch(clustmeth, 'kmeans') %Kmeans in projected space
	tic, idx2=kmeans(waveforms_pca,nclusts1,'Replicates',200); toc
	
elseif strmatch(clustmeth, 'EM') %EM algorithm, projected space
	tic, options=foptions; %setting up default options for matlab's optimization routine
	options(14)=5; %running kmeans algorithm 5 iterations
	mix=gmm(spikes.info.pca.ndim,nclusts1,'full',options); %initialize mixture models to have 'nclusts' centers, each of 'ndim' dimmensionality, and 'spherical' covariance structure'
	mix=gmminit_z(mix,waveforms_pca,options); %initial data layout from kmeans
	options(14)=200; %running EM algorithm for  max. 30 iterations
	mix=gmmem(mix,waveforms_pca,options); %optimize models using EM algorithm
	[post,act]=gmmpost(mix, waveforms_pca); %obtaining posterior probabilities from model
	[jnk,idx2]=max(post,[],2);, toc %turning posterior probabilities into labels
		
else error('Improper clustering method, please use ''kmeans'' or ''EM''')
end

subs_good=reshape(spikes.waveforms, size(spikes.waveforms,1), size(spikes.waveforms,2)*size(spikes.waveforms,3)); %aligned spike waveforms

%Sort clusters from largest to smallest
nclusts=spikes.params.kmeans_clustersize+(nclusts1-1); %increasing cluster number by nclusts1-1
idx1=spikes.assigns; %list of original assigns
idx3=find(idx1==clustndx); %list of assigns belonging to splt cluster
%%%%
for ind=1:(nclusts1-1) %for each of the split clusters after the first
	idx1(idx3(idx2==ind))=nclusts-(ind-1); %2nd member of split cluster is assigned largest clusternumber
end %for each of the split clusters after the first
%%%%
%for ind=1:nclusts, szval(ind)=sum(abs(median(subs_good(idx1==ind,:))));, end %size of each cluster
for ind1=1:nclusts
	qq=median(subs_good(idx1==ind1,:));
	qq2=reshape(qq,size(spikes.waveforms,2),length(spikes.opts.rthresh));
	qq3=max(qq2*spikes.opts.mf);
	szval(ind1)=max(qq3);
end %size of each cluster
clear id_ord
[jnk,id_ord]=sort(szval,'descend'); %sort clusters by size
%%%%%
clsts(1)=find(id_ord==[clustndx]); %1st new cluster
for ind=1:(nclusts1-1) %for each of the split clusters after the first
	clsts(ind+1)=find(id_ord==[nclusts-(ind-1)]); %additional new clusters
end %for each of the split clusters after the first
clsts  % print the indices of the newly creates clusters on terminal
%%%%%%
[jnk,newndx]=setdiff(id_ord, clsts); %index of new clusters, minus newly formed split clusters

idx=idx1;
for ind=1:nclusts
	idx(idx1==id_ord(ind))=ind;
end %apply new label to clusters
%keyboard
%Update Kleinfeld structure
spikes1=spikes;
spikes1.assigns=idx'; %indices of labels for cluster ID for each waveform
spikes1.labels=[1:nclusts; ones(1,nclusts)]'; %Need to look at this to figure out- Cx2 (C=# of clusters)
colormap jet
clrs=colormap;
cndx=round(1:(63/(nclusts-1)):64);
spikes1.info.kmeans.colors=clrs(cndx,:); %color matrix storing color values (1x3) for each cluster
spikes1.params.kmeans_clustersize=nclusts; %number or miniclusters
spikes1.waveforms_clust(id_ord(newndx))=spikes.waveforms_clust(oldndx); %just copy over old cluster waveforms
spikes1.waveforms_clust_pca(id_ord(newndx))=spikes.waveforms_clust_pca(oldndx); %just copy over old cluster waveforms



%ID single largest event channel for entire cluster
if length(spikes.info.spkchan)>1
	for ind=1:nclusts1 %for each of 2 split clusters
		subs1=spikes1.waveforms(find(spikes1.assigns==clsts(ind)),:);
		subs1=reshape(subs1,size(subs1,1),size(subs1,2)/length(spikes1.info.detect.thresh), length(spikes1.info.detect.thresh));
		subs2=squeeze(mean(subs1,1));
		[j1,chan_nd]=max(squeeze(max(spikes1.info.mf*subs2,[],1))./spikes1.info.detect.thresh,[],2);
		spikes1.info.detect.event_channel(find(spikes1.assigns==clsts(ind)))=chan_nd;
	end
	clear subs1 subs2 chan_nd
else
	spikes1.info.detect.event_channel=ones(size(spikes.assigns));
end

%Align spike waveforms (dejitter) within clusters
for ind=1:nclusts, subs_sort{ind}=subs_good(idx==ind,:);, end
for ind=1:nclusts1
	spikes1=dejitterclusts(spikes1,clsts(ind));
	spikes1.waveforms_clust_pca{clsts(ind)}=spikes1.waveforms_clust{clsts(ind)}*spikes.info.pca.v(:,1:spikes.info.pca.ndim);
end

%spikes1.waveforms_clust=subs_clust; %clustered waveforms, aligned per cluster;
%spikes1.waveforms_clust_pca=subs_clust_pca; %clustered waveforms, aligned per cluster, projected into PCA space;
% code for checking spilt/merge errors
for ind=1:nclusts, nspa(ind)=length(find(spikes1.assigns==ind)); end
for ind=1:nclusts, nspb(ind)=size(spikes1.waveforms_clust{ind},1); end
if ~isequal(nspa,nspb)
	warning('Cluster index error in splitclusts');
end

