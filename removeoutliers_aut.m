function spikes1=removeoutliers_aut(spikes,threshv,clustndx,threshtype)

% spikes1=removeoutliers_aut(spikes,threshv,clustndx, threshtype)
%
% Function to semi-automatically remove (as outliers) waveforms from clusters based on mahalanobis distance
% from model variance or euclidean distance from model mean.
% Inputs:
% spikes- structure array, output of spike sorting or classification.
% threshv- (optional) threshold value, chi-squared percentile if using mahalanobis distance, 
%	or scalar value for distance if using euclidean distance (default: manual selection)
% clustndx- index of which clusters to analyze (default:  all clusters in spikes array)
% threshtype- which distance method to use, 'mahalanobis' or 'euclidean' (default: 'mahalanobis')
%
% May 2012, ZNA

rng(0); % initialize random number generator to fixed state

spikes1=spikes;
if nargin<2||isempty(threshv), figure, end
if nargin<3||isempty(clustndx), clustndx=1:max(spikes1.assigns);, end %default- do all clusters
if nargin<4||isempty(threshtype), threshtype='mahalanobis';, end %default- mahalanobis distance

for ind=1:length(clustndx) %for each cluster
	status_out=1; %status of output
	while status_out==1 
		idx=find(spikes1.assigns==clustndx(ind)); %list of assigns belonging to splt cluster
		if nargin<2||isempty(threshv) %if thresh value not given
			clf
			subplot(2,2,2)
			plot(spikes.waveforms_clust{clustndx(ind)}')
			title(['Original data, N=' num2str(size(spikes.waveforms_clust{clustndx(ind)},1))])					
			subplot(2,2,1)
			if strcmp(threshtype, 'mahalanobis') %if using mahalanobis distance
				[z,dof]=plot_distances_z(spikes1,clustndx(ind),1);
			elseif strcmp(threshtype, 'euclidean') %if using euclidean distance
				z = []; % use this instead of 'clear z' to avoid crashing in case of empty clusters
				for ind1=1:size(spikes1.waveforms_clust{clustndx(ind)},1)
					z(ind1)=pdist([mean(spikes1.waveforms_clust{clustndx(ind)},1); spikes1.waveforms_clust{clustndx(ind)}(ind1,:)], threshtype);
				end
				hist(z, round(sqrt(length(z))))
			elseif strcmp(threshtype, 'thresh') %if using amplitude distance from threshold
				
			    select = get_spike_indices(spikes,clustndx(ind));      
			    waveforms = spikes.waveforms(select,:,:)*spikes.info.mf; %multiply by +1 or -1
				threshes = spikes.info.detect.thresh;
				ev_chans = spikes.info.detect.event_channel(select); %Which channel indices to look at
			    th(1,1,:) = threshes;
			    waveforms = waveforms ./ repmat( th, [size(waveforms,1) size(waveforms,2) 1] );
			    wtmp=zeros(size(waveforms,1),size(waveforms,2));
			    for ind1=1:size(waveforms,1), wtmp(ind1,:)=squeeze(waveforms(ind1,:,ev_chans(ind1)));, end
			    z = -max( wtmp, [], 2 );
				hist(z, round(sqrt(length(z))))
				
			else %otherwise error
				error('Please choose ''mahalanobis'', ''euclidean'',  or ''thresh'' for input ''threshtype''')
			end %if using mahalanobis distance
		

			title(['Clust # ' num2str(clustndx(ind)) ', N=' num2str(size(spikes1.waveforms_clust{clustndx(ind)},1))], 'Color', spikes1.info.kmeans.colors(clustndx(ind),:))					
			disp('Select horizontal limit (to the right will be outliers)')
			pause(1), figure(gcf)
			[xval,jnk]=ginput(1);			
			yls=get(gca, 'YLim');
			xls=get(gca, 'XLim');
			hold on
			plot([xval xval], yls, 'r--', 'LineWidth', 2)
		else %if threshvalue IS given
			if strcmp(threshtype, 'mahalanobis') %if using mahalanobis distance
				h1=figure;
				[z,dof]=plot_distances_z(spikes1,clustndx(ind),1);
				close(h1);
				xval=chi2inv(threshv,dof);
			elseif strcmp(threshtype, 'euclidean') %if using euclidean distance
				xval=threshv;
%				clear z
				z = []; % use this instead of 'clear z' to avoid crashing in case of empty clusters
				for ind1=1:size(spikes1.waveforms_clust{clustndx(ind)},1)
					z(ind1)=pdist([mean(spikes1.waveforms_clust{clustndx(ind)},1); spikes1.waveforms_clust{clustndx(ind)}(ind1,:)], threshtype); % specified that mean is taken along 1st dimension, otherwise it crashes
				end
			elseif strcmp(threshtype, 'thresh') %if using amplitude distance from threshold

				xval=threshv;
			    select = get_spike_indices(spikes,clustndx(ind));      
			    waveforms = spikes.waveforms(select,:,:)*spikes.info.mf; %multiply by +1 or -1
				threshes = spikes.info.detect.thresh;
				ev_chans = spikes.info.detect.event_channel(select); %Which channel indices to look at
			    th(1,1,:) = threshes;
			    waveforms = waveforms ./ repmat( th, [size(waveforms,1) size(waveforms,2) 1] );
			    wtmp=zeros(size(waveforms,1),size(waveforms,2));
			    for ind1=1:size(waveforms,1), wtmp(ind1,:)=squeeze(waveforms(ind1,:,ev_chans(ind1)));, end
			    z = -max( wtmp, [], 2 );
				
			else %otherwise error
				error('Please choose ''mahalanobis'', ''euclidean'',  or ''thresh'' for input ''threshtype''')
			end %if using mahalanobis distance

		end			
		spikes_tmp=spikes1; %temporary version of spikes object
		nd=find(z>xval); %values to throw out
		spikes_tmp.assigns(idx(nd))=[];
		spikes_tmp.spiketimes(idx(nd))=[];
		spikes_tmp.unwrapped_times(idx(nd))=[];
		spikes_tmp.trials(idx(nd))=[];
		spikes_tmp.files(idx(nd))=[];
		spikes_tmp.info.detect.event_channel(idx(nd))=[];
		spikes_tmp.waveforms(idx(nd),:,:)=[];
		spikes_tmp.waveforms_pca(idx(nd),:)=[];
		spikes_tmp.waveforms_clust{clustndx(ind)}(nd,:)=[];
		spikes_tmp.waveforms_clust_pca{clustndx(ind)}(nd,:)=[];
		disp(['Removing ' num2str(length(nd)) ' out of ' num2str(length(idx)) ' spikes in cluster ' num2str(clustndx(ind))]);  
		
		if isfield(spikes_tmp, 'firstsupndx') %If this is an output from classification (instead of an output from model building)
			[jnk,rmvndx1]=intersect(spikes_tmp.firstsupndx, idx(nd)); %first spike of superposition indices belonging to cluster to remove
			[jnk,rmvndx2]=intersect(spikes_tmp.secndsupndx, idx(nd)); %second spike of superpoition indices belonging to cluster to remove
			spikes_tmp.firstsupndx(rmvndx1)=[]; %get rid of these indices
			spikes_tmp.secndsupndx(rmvndx2)=[]; %get rid of these indices
			[jnk,keepndx]=intersect(spikes1.unwrapped_times, spikes_tmp.unwrapped_times); %mapping the kept spikes back into the original indices
			[jnk,spikes_tmp.firstsupndx]=intersect(keepndx,spikes_tmp.firstsupndx); %mapping old sup. indices to new values
			[jnk,spikes_tmp.secndsupndx]=intersect(keepndx,spikes_tmp.secndsupndx); %mapping old sup. indices to new values
		end %If this is an output from classification (instead of an output from model building)		
		
		
		
		if nargin<2||isempty(threshv) %if thresh value not given
			subplot(2,2,3)

			if strcmp(threshtype, 'mahalanobis') %if using mahalanobis distance
				plot_distances_z(spikes_tmp,clustndx(ind),1);
			elseif strcmp(threshtype, 'euclidean')%if using euclidean distance
				z2 = []; % use this instead of 'clear z' to avoid crashing in case of empty clusters
				for ind1=1:size(spikes_tmp.waveforms_clust{clustndx(ind)},1)
					z2(ind1)=pdist([mean(spikes1.waveforms_clust{clustndx(ind)}); spikes_tmp.waveforms_clust{clustndx(ind)}(ind1,:)],threshtype);
				end
				hist(z2, round(sqrt(length(z2))))
			else %if using amplitude distance from thresh
			
			    select = get_spike_indices(spikes_tmp,clustndx(ind));      
			    waveforms = spikes_tmp.waveforms(select,:,:)*spikes.info.mf; %multiply by +1 or -1
				threshes = spikes_tmp.info.detect.thresh;
				ev_chans = spikes_tmp.info.detect.event_channel(select); %Which channel indices to look at
			    th(1,1,:) = threshes;
			    waveforms = waveforms ./ repmat( th, [size(waveforms,1) size(waveforms,2) 1] );
			    wtmp=zeros(size(waveforms,1),size(waveforms,2));
			    for ind1=1:size(waveforms,1), wtmp(ind1,:)=squeeze(waveforms(ind1,:,ev_chans(ind1)));, end
			    z2 = -max( wtmp, [], 2 );
				hist(z2, round(sqrt(length(z2))))
				
			end %if using mahalanobis distance


			xlim(xls)
			title(['Proposed Clust # ' num2str(clustndx(ind)) ', N=' num2str(size(spikes_tmp.waveforms_clust{clustndx(ind)},1))], 'Color', spikes_tmp.info.kmeans.colors(clustndx(ind),:))
	
			subplot(2,2,4)
			plot(spikes_tmp.waveforms_clust{clustndx(ind)}')
			title(['Proposed clean data, N=' num2str(size(spikes_tmp.waveforms_clust{clustndx(ind)},1))])					

			disp('Outlier removal looks good?  Type...');
			disp('0	for using selected limit.');
			disp('1	for selecting new limit.');
			pause(1), figure(gcf)
			opt=input('Option? ');
			if (opt==0) % use current limit
				spikes1=spikes_tmp;
				status_out=0; %break out of loop
			elseif (opt==1) %select new limit- don't do anything, just iterate through while loop again
			else disp('Sorry, invalid option; try again.');
			end %if (opt==0) 
		else %if threshvalue IS given
			spikes1=spikes_tmp;
			status_out=0; %break out of loop
		end %if threshv not given
	end %while status_out==1
end %for each cluster

%Align spike waveforms (dejitter) within clusters
for ind=1:length(clustndx)
	spikes1=dejitterclusts(spikes1,clustndx(ind));
	spikes1.waveforms_clust_pca{clustndx(ind)}=spikes1.waveforms_clust{clustndx(ind)}*spikes.info.pca.v(:,1:spikes.info.pca.ndim);
end

