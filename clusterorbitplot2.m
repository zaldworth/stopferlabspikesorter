function f1=clusterorbitplot2(spikes, DoYouWantToSeeColors, clustndx) %NG: added clustndx option

% f1=clusterorbitplot2(spikes, DoYouWantToSeeColors, clustndx);
%
% Function for manually examining clustering of waveforms in 3-d PCA space.  Allows user to
% interactively rotate the clusters by dragging the mouse.

%set defaults
if nargin<2||isempty(DoYouWantToSeeColors) || DoYouWantToSeeColors==0 %NG
	opts.plotlab='nonclust'; 
	opts.nd1=1; 
	opts.nd2=2; 
	opts.nd3=3; 
	opts.dphi=1; 
	opts.dthe=0; 
else
	opts=mk_clustorbitopts(spikes); 
end

%unpack parameters
nd1=opts.nd1;
nd2=opts.nd2;
nd3=opts.nd3;
dphi=opts.dphi;
dthe=opts.dthe;

f1=figure;
set(gcf, 'Position', [600 250 750 650])
if strcmp(opts.plotlab, 'nonclust')
	plot3(spikes.waveforms_pca(:,nd1),spikes.waveforms_pca(:,nd2),spikes.waveforms_pca(:,nd3), 'k.', 'MarkerSize', 8)
elseif strcmp(opts.plotlab, 'clust')	
% 	nclusts=opts.nclusts; %NG changed this to select specific clusters
	if nargin<3||isempty(clustndx), clustndx = 1:opts.nclusts; end; %NG
	idx=opts.idx;
	cmat=opts.cmat;
	hold on
	for i=1:length(clustndx) %for each cluster  %NG
		ind1 = clustndx(i); %NG
		hold on
		wavs=spikes.waveforms_pca(idx==ind1,[nd1 nd2 nd3]);
		c_cent=mean(wavs,1); %cluster center
		plot3(wavs(:,1),wavs(:,2),wavs(:,3), '.', 'MarkerSize', 8, 'Color', cmat(ind1,:))
		t1=text(c_cent(1), c_cent(2), c_cent(3), num2str(ind1));
		set(t1, 'FontSize', 18);
		l1{i}=['Class ' num2str(ind1)]; %NG
	end
	legend(l1, 'Location', 'NorthEastOutside')
else
	error('Incorrect plot label, use either ''nonclust'' or ''clust''');
end

aa(1,:)=get(gca, 'XLim');
aa(2,:)=get(gca, 'YLim');
aa(3,:)=get(gca, 'ZLim');

view(3)
grid on
set(gca, 'CameraViewAngle', 10)

figure(f1)
rotate3d on 
