function dist = evalGaus(tdat, me, covar)

nd = size(covar, 1);
npts = size(tdat, 1);
norm = 1.0 / ( sqrt(det(covar)).*(2*pi).^(nd/2.0));
icov = inv(covar);
dist = zeros(npts, 1);
for i =[1:npts]
	x = tdat(i,:) - me;
	out = -.5* (x*icov)*x';
	dist(i) = log(norm)+out;
end

