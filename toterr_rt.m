function [fpos,fneg,fres1,fres2,fres3]=toterr_rt(spikes,ndx)

% [fpos,fneg,fres1,fres2,fres3]=toterr_rt(spikes,ndx)
%
% Function to calculate total per cluster false positive (added spikes) and
% false negative (missing spikes) rates for each cluster, using assumptions of
% Hill, Mehta, and Kleinfeld, J Neursci, 2011.


if nargin<2||isempty(ndx), ndx=1:max(spikes.assigns); end %if no indxex of units given

fres1=contam_er(spikes,ndx); %contamination false positives due to refractory violations
fres2=overlap_er(spikes,ndx); %false pos and false neg due to proximity of neighboring clusters
fres3=undetect_er(spikes,ndx); %false negatives due to spikes falling below threshold
% Drop censor error- we assume our resolution of superopositions takes care of this
%fres4=censor_er(spikes); %false negative due to spike falling into censor period following other spike

fpos=max([fres1/100 fres2(:,1)],[],2)*100; %total false positive rate, in percentage
fneg=(fres2(:,2)+1-(1-fres3))*100; %total false negative rate, in percentage
%fneg=(fres2(:,2)+1-(1-fres3).*(1-fres4))*100; %total false negative rate, in percentage

% changing from fraction to percentage
fres1=fres1*100; %contamination false positives due to refractory violations
fres2=fres2*100; %false pos and false neg due to proximity of neighboring clusters
fres3=fres3*100; %false negatives due to spikes falling below threshold
