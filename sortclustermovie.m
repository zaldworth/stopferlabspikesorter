function m=sortclustermovie(spikes)

% m=sortclustermovie(spikes)
%
% Function to make move showing progression of clusters in PCA space over duration of recording.
%
% September 4th, 2012, ZNA

%Regularizing projection dimensions
for ind=2:length(spikes)
	spikes(ind).info.pca.v=spikes(1).info.pca.v;
	spikes(ind).info.pca.s=spikes(1).info.pca.s;
end

for ind=1:length(spikes)
%	opts1=mk_clustorbitopts(spikes(ind));
	clusterorbitplot2(spikes(ind),1);
	set(gca, 'CameraPosition', [-39.6834 -599.8360  188.3382], 'CameraTarget', [20 20 17.5], 'XLim', [-20 60], 'YLim', [-20 60], 'ZLim', [-5 40])
	shg
	m(ind)=getframe(gcf);
	close
end






% 