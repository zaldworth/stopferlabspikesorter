function spikes1=removeoutliers_man(spikes,clustndx)

% spikes1=removeoutliers_man(spikes,clustndx)
%
% Function to manually remove (as outliers) individual waveforms from clusters.

spikes1=spikes;
status_out=1;
figure
while status_out==1 
	clf
	
	idx=find(spikes1.assigns==clustndx); %list of assigns belonging to splt cluster
	h1=plot(spikes1.waveforms_clust{clustndx}', '--');
	hold on
	plot(median(spikes1.waveforms_clust{clustndx}), 'k', 'LineWidth', 2)
	title(['Clust # ' num2str(clustndx) ', N=' num2str(size(spikes1.waveforms_clust{clustndx},1))], 'Color', spikes1.info.kmeans.colors(clustndx,:))
	set(gca, 'XColor', spikes1.info.kmeans.colors(clustndx,:), 'YColor', spikes1.info.kmeans.colors(clustndx,:))
	axis tight
	yls=get(gca, 'YLim');
	
	winx=((spikes1.params.window_size*spikes1.params.Fs/1e3):(spikes1.params.window_size*spikes1.params.Fs/1e3):(spikes1.params.window_size*spikes1.params.Fs/1e3)*(size(spikes1.waveforms, 3)-1))+1;
	plot(repmat(winx,2,1), repmat(yls',1,(size(spikes1.waveforms, 3)-1)), 'Color', spikes1.info.kmeans.colors(clustndx,:))

	disp('Select an outlier waveform')
	pause(1), figure(gcf)
	waitforbuttonpress
	h2=gco;
	nd=find(h1==h2);
	
	set(h2, 'LineWidth', 4, 'Color', [1 0 0])
	figure(gcf)

	disp('Type');
	disp('0	for remove waveform and continue to select next waveform.');
	disp('1	for remove waveform then ending program.');
	disp('2	for keeping waveform and continue to select next waveform.');
	disp('3	for keeping waveform then ending program.');
	opt=input('Option? ');
	if (opt==0) % continue option
		spikes1.assigns(idx(nd))=[];
		spikes1.spiketimes(idx(nd))=[];
		spikes1.unwrapped_times(idx(nd))=[];
		spikes1.trials(idx(nd))=[];
		spikes1.files(idx(nd))=[];
		spikes1.info.detect.event_channel(idx(nd))=[];
		spikes1.waveforms(idx(nd),:,:)=[];
		spikes1.waveforms_pca(idx(nd),:)=[];
		spikes1.waveforms_clust{clustndx}(nd,:)=[];
		spikes1.waveforms_clust_pca{clustndx}(nd,:)=[];		
		if isfield(spikes1, 'firstsupndx') %If this is an output from classification (instead of an output from model building)
			[jnk,rmvndx1]=intersect(spikes1.firstsupndx, idx(nd)); %first spike of superposition indices belonging to cluster to remove
			[jnk,rmvndx2]=intersect(spikes1.secndsupndx, idx(nd)); %second spike of superpoition indices belonging to cluster to remove
			spikes1.firstsupndx(rmvndx1)=[]; %get rid of these indices
			spikes1.secndsupndx(rmvndx2)=[]; %get rid of these indices
			[jnk,keepndx]=intersect(spikes.unwrapped_times, spikes1.unwrapped_times); %mapping the kept spikes back into the original indices
			[jnk,spikes1.firstsupndx]=intersect(keepndx,spikes1.firstsupndx); %mapping old sup. indices to new values
			[jnk,spikes1.secndsupndx]=intersect(keepndx,spikes1.secndsupndx); %mapping old sup. indices to new values
		end %If this is an output from classification (instead of an output from model building)						
	elseif (opt==1) 
		spikes1.assigns(idx(nd))=[];
		spikes1.spiketimes(idx(nd))=[];
		spikes1.unwrapped_times(idx(nd))=[];
		spikes1.trials(idx(nd))=[];
		spikes1.files(idx(nd))=[];
		spikes1.info.detect.event_channel(idx(nd))=[];
		spikes1.waveforms(idx(nd),:,:)=[];
		spikes1.waveforms_pca(idx(nd),:)=[];
		spikes1.waveforms_clust{clustndx}(nd,:)=[];
		spikes1.waveforms_clust_pca{clustndx}(nd,:)=[];
		if isfield(spikes1, 'firstsupndx') %If this is an output from classification (instead of an output from model building)
			[jnk,rmvndx1]=intersect(spikes1.firstsupndx, idx(nd)); %first spike of superposition indices belonging to cluster to remove
			[jnk,rmvndx2]=intersect(spikes1.secndsupndx, idx(nd)); %second spike of superpoition indices belonging to cluster to remove
			spikes1.firstsupndx(rmvndx1)=[]; %get rid of these indices
			spikes1.secndsupndx(rmvndx2)=[]; %get rid of these indices
			[jnk,keepndx]=intersect(spikes.unwrapped_times, spikes1.unwrapped_times); %mapping the kept spikes back into the original indices
			[jnk,spikes1.firstsupndx]=intersect(keepndx,spikes1.firstsupndx); %mapping old sup. indices to new values
			[jnk,spikes1.secndsupndx]=intersect(keepndx,spikes1.secndsupndx); %mapping old sup. indices to new values
		end %If this is an output from classification (instead of an output from model building)						
		clf
		idx=find(spikes1.assigns==clustndx); %list of assigns belonging to splt cluster
		h1=plot(spikes1.waveforms_clust{clustndx}', '--');
		hold on
		plot(median(spikes1.waveforms_clust{clustndx}), 'k', 'LineWidth', 2)
		title(['Clust # ' num2str(clustndx) ', N=' num2str(size(spikes1.waveforms_clust{clustndx},1))], 'Color', spikes1.info.kmeans.colors(clustndx,:))
		set(gca, 'XColor', spikes1.info.kmeans.colors(clustndx,:), 'YColor', spikes1.info.kmeans.colors(clustndx,:))
		axis tight
		yls=get(gca, 'YLim');
		winx=((spikes1.params.window_size*spikes1.params.Fs/1e3):(spikes1.params.window_size*spikes1.params.Fs/1e3):(spikes1.params.window_size*spikes1.params.Fs/1e3)*(size(spikes1.waveforms, 3)-1))+1;
		plot(repmat(winx,2,1), repmat(yls',1,(size(spikes1.waveforms, 3)-1)), 'Color', spikes1.info.kmeans.colors(clustndx,:))
		status_out=0;
	elseif (opt==2)
	elseif (opt==3)
		status_out=0;
	else disp('Sorry, invalid option; try again.');
	end %if (opt==0) 
	
	figure(gcf)
end		

%qq=tt