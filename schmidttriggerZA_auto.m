function [mIndx,mVal,thresh] = schmidttriggerZA_auto(current_seg,current_time,t1,t2);

% [mIndx,mVal,thresh] = schmidttriggerZA_auto(current_seg,current_time,t1,t2);
% 
% Automated version of schidttrigger_ZA.m, User input not necessary for
% defining thresholds.  Lower threshold set as 1.5*std of data, upper
% threshold set as 2*std of data.
% Function to extract spike times and values using a schmidt-trigger (2
% threshold) system.  In order for a local maximimum to be counted as a
% spike time, it has to exceed both a lower and upper threshold value.
% Before a second maxima can be counted as a second spike, the function has
% to drop below the lower threshold value before exceeding the upper
% threshold value.
% Inputs:
% current_seg- data segment to be thresheld
% current_time- vector of times for data to be thresheld
%   (default=1:length(current_seg)
% t1- lower threshold value (in standard deviations of data set)
% t2- upper threshold value (in standard deviations of data set)
% Outputs:
% mIndx- time stamps of spikes
% mVal- heights of spikes
% thresh- 2 elements consisting of lower and upper threshold values
% 
% 2002, TWG (some additions by ZNA)
 
mIndx=[];,mVal=[]; % initialize. It once failed to assign any value to mIndx and crashed
if nargin<2||isempty(current_time), current_time=1:length(current_seg);, end %default value

%Initialize variables
prelim1(1:length(current_seg)) = 0;
prelim2(1:length(current_seg)) = 0;

%Defining thresholds
thresh(1)=t1*std(current_seg)+mean(current_seg); %Setting lower threshold
thresh(2)=t2*std(current_seg)+mean(current_seg); %Setting upper threshold

%Getting values exceeding thresholds
prelim1(find(current_seg>thresh(1))) = 1; %Values exceeding lower threshold=1
prelim2(find(current_seg>thresh(2))) = 1; %Values exceeding upper threshold=1
on1 = find(diff(prelim1)==1)+1; %points where lower threshold first exceeded
on2 = find(diff(prelim2)==1)+1; %points where upper threshold first exceeded
off1 = find(diff(prelim1)==-1); %points where lower threshold crossed with negative slope
off2 = find(diff(prelim2)==-1); %points where upper threshold crossed with negative slope

%Get rid of spikes occuring before first crossing
if ~isempty(on1)  
	early1 = find(off1<on1(1));
	early2 = find(off2<on1(1));
	off1(early1) = [];
	off2(early2) = [];
end
	
%Get rid of spikes occurring after last 'uncrossing'
if length(off1)~=0 %only if there is at least one negative crossing
    late1 = find(on1>off1(end));
    late2 = find(on2>off1(end));
    on1(late1) = [];
    on2(late2) = [];

    for ind = 1:length(on1); %for every point between crossings of lower threshold
        flag(ind) = isempty(intersect(on1(ind):off1(ind),on2)); %flag points with zero which exist between lower
                                                                %crossing and which exceed upper corssing
    end %for every point crossing lower threshold

    flagOK = find(flag==0);%which points were flagged with zeros

    for ind  = 1:length(flagOK); %for all flagged points
       [mVal(ind) mIndx(ind)] = max(current_seg(on1(flagOK(ind)):off1(flagOK(ind)))); %Maxima height and time
       mIndx(ind) = mIndx(ind)+on1(flagOK(ind)); %Adjusting maxima time by threshold event time
    end %for all flagged points
else
    mIndx=[];,mVal=[]; %assign empties
end %only if there is at least one negative crossing
