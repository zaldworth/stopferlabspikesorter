function plot_allmodwaves(spikes)

% plot_allmodwaves(spikes)
%
% Function to plot model center for every cluster & for every recorded channel, along 
% with single-dim estimate of per-cluster variability

%Plotting mean spike waveform for all clusters for each channel
ndx=reshape(1:size(spikes.waveforms_clust{1},2),spikes.params.window_size*spikes.params.Fs/1e3,length(spikes.info.spkchan));
%for ind=1:max(spikes.assigns), subs_sort{ind}=subs_good(idx==ind,:);, end
figure
set(gcf, 'Position', [600 150 575 875])
yls=zeros(0,2); %initialize
for ind1=1:max(spikes.assigns) %for each cluster
	for ind2=1:length(spikes.info.spkchan) %for each spike channel
		subplot(length(spikes.info.spkchan),max(spikes.assigns),(ind2-1)*max(spikes.assigns)+ind1)
		plot((1:(spikes.params.window_size*spikes.params.Fs/1e3))/(spikes.params.Fs/1e3), median(spikes.waveforms_clust{ind1}(:,ndx(:,ind2))), 'r', 'LineWidth', 2)
		hold on, plot((1:(spikes.params.window_size*spikes.params.Fs/1e3))/(spikes.params.Fs/1e3), spikes.info.madk*mad(spikes.waveforms_clust{ind1}(:,ndx(:,ind2))), 'c', 'LineWidth', 2)
		if ind2==1, title(['#' num2str(ind1)]), end
		if ind2~=length(spikes.info.spkchan), set(gca, 'XTickLabel', []), end
		if ind2==length(spikes.info.spkchan), xlabel('T (ms)'), end
		if ind1==1, ylabel('Amp (SD=1)'), end
		if ind1~=1, set(gca, 'YTickLabel', []), end
		if ind1==max(spikes.assigns), set(gca, 'YAxisLocation', 'right'), ylabel(['site ' num2str(ind2)]), end
		axis tight, grid on
		set(gca, 'Color', [.95 .95 .95], 'XColor', [0 0 0], 'YColor', [0 0 0])
		yls(end+1,:)=get(gca, 'YLim');
	end
end
%xzoom(gcf, 0, 3)
yls2(1)=min(yls(:,1)); yls2(2)=max(yls(:,2));
yzoom(gcf, yls2(1), yls2(2))
set(gcf, 'Color', [1 1 1])