function bv=getbic(spikes,testwaves);

% bv=getbic(spikes,testwaves);
%
% Function to determine the bayesian information criteria for spike clusters.
% BIC defined as -2*ln(L)+k*ln(n), where:
% n = the number of data points in x, the number of observations, or equivalently, the sample size;
% k = the number of free parameters to be estimated. If the estimated model is a linear regression, k is the number of regressors, including the intercept;
% L = the maximized value of the likelihood function for the estimated model.
%
% May 31, 2012, ZNA
% Modified June 3rd, 2012, by ZNA- now can use separate test data set from model build

if nargin<2||isempty(testwaves), testwaves=spikes.waveforms_pca;, end %default- use same samples to build and test models

nsamps=size(testwaves,1); %number of observations in test data set
nclusts=max(spikes.assigns); %number of clusters
leng=size(testwaves,2);

%Build models from clustered waveforms
llr2=zeros(nclusts,nsamps); %initialize variable
for ind=1:nclusts %for ever model center
	ndx=find(spikes.assigns==ind);
	if length(ndx)>1 %if more than one element
		m.m(ind,:)=mean(spikes.waveforms_pca(ndx,:)); %build model mean
		m.c(:,:,ind)=cov(spikes.waveforms_pca(ndx,:)); %build model covar
	else %if only one element (this is bad)
		m.m(ind,:)=spikes.waveforms_pca(ndx,:); %build model mean- only one value
		m.c(:,:,ind)=zeros(leng,leng); %build model covar- no variance for single value
	end %if more than one element
	llr2(ind,:) = evalGaus(testwaves, m.m(ind,:), m.c(:,:,ind)); %log likelihood ratio
end %for ever model center

llr=max(llr2); %finding maximum likelihood of cluster

nparams=prod(size(m.m))+prod(size(m.c)); %number of free parameters


bv=-2*sum(llr)+nparams*log(nsamps); %sum of log-likelihoods is equal to log of products of likelihoods