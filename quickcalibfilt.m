function yest=quickcalibfilt(xdata,filtvect,numpts);

%  yest=quickcalibfilt(xdata,filtvect,numpts);
%
%  Function to filter input data 'xdata' to try and obtain an estimate
%  'yest' of an output data by using defined filter 'filtvect'.
%  Can offset 'yest' by user defined 'numpts' (default 'numpts'=0).
%  If input data is too longer than 11,000,000 points, then it is
%  chopped up into smaller pieces, each of which is filtered seperately.

if nargin<3, numpts=0;, end %default
lengparts=1.1e7; %Best guess for matlab's maximum variable size for filter


%First, buffer xdata, then filter
filtlength=length(filtvect);
buffdata=zeros(length(xdata)+2*filtlength,1);
buffdata(1:length(xdata))=xdata;

if length(buffdata)<lengparts, %data is short enough to do the easy way
    y1=filter(filtvect,1,buffdata);
else %data is too long for matlab- do the hard way
    numparts=floor(length(buffdata)/lengparts)+1; %How many parts?
    partsvect=1:lengparts; %vector for extracting each part
    zf=zeros(filtlength-1,1);, %setting up initial values
    for ind=1:numparts-1,
        %filtering by stops
        y1a=buffdata(partsvect+lengparts*(ind-1));
        [y1b,zf]=filter(filtvect,1,y1a,zf);
        y2{ind}=y1b;
        clear y1b
    end
    %Now get last part
    clear y1a
    y1a=buffdata(1+lengparts*(numparts-1):end); %last part is all the rest
    y1b=filter(filtvect,1,y1a,zf);
    y2{ind+1}=y1b;
    %putting all the pieces together
    y1=cell2mat(y2');
end %how short is stimulus?

%Now unbuffer, account for offsets induced by filterlength and by filter itself
numpoints=round(length(filtvect)/2)+numpts;
if abs(numpoints)>2*filtlength, numpoints=2*filtlength-1;, end %making sure we don't overrun buffer
yest=y1([0:length(xdata)-1]+numpoints);