function subs=getwinz(data,coords,len,offs)

% subs=getwinz(data,coords,len,offs)
% 
% Function to extract a series of windows from a data set
% Inputs:
% data- column vector of data from which windows are extracted
% coords- sample points into data for which to start collecting windows
% len- number of sample points to extract per window
% offs- offsets for each window (default offs=zeros)
% 
% written by AGD

if nargin<4||isempty(offs)
    offs=zeros(size(data,2),1);
end
offs=offs(:);  %vectorize

nchan=size(data,2);
exts=(1:len)-1;
coords=max(coords(:),1); %make sure they are a column vector
[X,Y]=meshgrid(exts,coords);
X = X+Y;
%size(X)
subs=zeros(length(coords),len*size(data,2));
%temp=zeros(length(exts),nchan);
if isempty(coords) 
	return;
end
for j1=1:nchan
    % hmm, what if I reverse this, doing it 1 chan at a time, with
    % indexing trick, then concatenating the channels...
    dataj1=data(:,j1);
    if size(X,1)>1
        subs(:,exts+((j1-1)*len+1))=dataj1(X+offs(j1));
    else
        subs(:,exts+((j1-1)*len+1))=dataj1(X+offs(j1))';
    end
end


