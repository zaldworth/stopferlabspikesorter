function filt_signal=nitinfilt(signal, lfro, hfro, fs)

%  filt_signal=nitinfilt(signal, lfro, hfro, fs);
%
%  Function for band passing a signal using 3-pole butterworth
%  Input variables:
%  lfro= low-frequency-roll-off- absolute minimum permissable value of 1 (in Hz)
%  hfro= high-frequency-roll-off (in Hz)
%  fs= sampling frequency of signal (in Hz)
%
%  2012-01-09, by ZNA, based on code by NG

test1=size(signal);
if test1(1)>test1(2) %column vector
    signal2=[ones(1,fs)*signal(1) signal' ones(1,fs)*signal(end)]; %buffer
else %row vector
    signal2=[ones(1,fs)*signal(1) signal ones(1,fs)*signal(end)]; %buffer
end %row or column vector
    
%define frequency vector in normalized units (1=fs/2)
lf=lfro/(fs/2); hf=hfro/(fs/2);

%building the filter
%[b, a]=butter(3, [lf hf], 'stop');
[b, a]=butter(3, [lf hf]);

%filtering the original signal
filt_signal2=filtfilt(b,a,signal2);
filt_signal=filt_signal2(fs+1:fs+length(signal));
if test1(1)>test1(2) %column vector
    filt_signal=filt_signal'; %changing back to column vector
end %column vector