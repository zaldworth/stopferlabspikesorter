function twoD_pca_plot_tot(spikes,ndx)

% twoD_pca_plot_tot(spikes,ndx)
%
% Function to show distributions of waveforms along two top iSTAC dimensions for all pairwise combinations of 
% clusters in 'ndx'.

if nargin<2||isempty(ndx), ndx=1:spikes.params.kmeans_clustersize;, end %default- look at all clusters

figure
set(gcf, 'Position', [125 100 1400 875])
for ind1=1:length(ndx)-1, for ind2=ind1+1:length(ndx)
%keyboard
	waveforms = [spikes.waveforms_clust{ndx(ind1)}; spikes.waveforms_clust{ndx(ind2)}]; 
	subplot(length(ndx)-1,length(ndx)-1,(ind1-1)*(length(ndx)-1)+ind2-1)	
	if size(waveforms,1)>1 
		[e1,e2] = eig(cov(waveforms)); %Get pair-specific PCAs  
		plot(spikes.waveforms_clust{ndx(ind1)}*e1(:,end), spikes.waveforms_clust{ndx(ind1)}*e1(:,end-1), '.', 'MarkerSize', 6, 'Color', spikes.info.kmeans.colors(ndx(ind1),:))
		hold on
		plot(spikes.waveforms_clust{ndx(ind2)}*e1(:,end), spikes.waveforms_clust{ndx(ind2)}*e1(:,end-1), '.', 'MarkerSize', 6, 'Color', spikes.info.kmeans.colors(ndx(ind2),:))
	end
	set(gca, 'XTickLabel', [], 'YTickLabel', [])	
	title([num2str(ndx(ind1)) ' - ' num2str(ndx(ind2))]); 
end, end

if length(ndx)>2   
	subplot(length(ndx)-1, length(ndx)-1, length(ndx))
	for ind1=1:length(ndx), hold on, barh(ind1, 1, .5, 'FaceColor', spikes.info.kmeans.colors(ndx(ind1),:)), end
	aa=get(gca, 'Position'); aa(3)=aa(3)/4; aa(4)=(aa(4)+aa(2))-.2; aa(2)=.2; 
	ylim([.5 length(ndx)+.5]), title('Cluster #')
	set(gca, 'Position', aa, 'xtick', [], 'ytick', 1:length(ndx), 'yticklabel', ndx)
end