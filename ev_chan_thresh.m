function evch=ev_chan_thresh(spikes,cndx)

% ev_chan_thresh(spikes,cndx);
%
% Function to plot the distribution of 'ev_channels' per given cluster
% Inputs:
% spikes- 'spikes' structure output from pouzatsort1.m or pouzatclass1.m
% cndx- indices of which clusters within spikes for which to show ev_channel.
%
% December 12, 2012

if nargin<2||isempty(cndx) cndx=1:max(spikes.assigns); end %default- use all cells

figure

evch=[];
for ind1=1:length(cndx)
	subplot(length(cndx),1,ind1)
	histv=hist(spikes.info.detect.event_channel(spikes.assigns==cndx(ind1)),1:length(spikes.info.spkchan));
	[jnk,evch(ind1)]=max(histv);
	bar(1:length(spikes.info.spkchan),histv)
	title(['Cluster # ' num2str(cndx(ind1))])
end