function [mean2,sndx,stims,mean1,num,pct,sout]=realign2_wrapper_ext(rawwaves,xfront,leng,tback,win,n,s,djlab);

% [mean2,sndx,stims,mean1,num,pct,sout]=realign2_wrapper_ext(rawwaves,xfront,leng,tback,win,n,s,djlab);
%
% Function for iteratively applying realign2.m or realign2m.m to data (especially
% useful for bootstrapping).
% Input variables:
% rawwaves- nsamps*leng*2chan data to be dejittered
% xfront- 'buffer' around desired data
% leng- number of sample points in extracted stimuli
% tback- time to anchor within extracted stimuli
% win- allowable time shift in sample points- if this is one element, then
%   allowable time shifts will be symmetric, otherwise use 2-element vector
%   specifying [negativeshifts positiveshifts]
% n- maximum allowed number of iterations of realign2.m to run
% s- sigma parameter, in sample points
% djlab- label for dejittering method, 'dprod' or 'fullgauss' (default='dprod')
% Output variables:
% mean2- mean of dejittered stimuli
% sndx- cumulative shifts for each sample over n iterations
% stims- nsamps*leng*2chan outputs (dejittered)
% mean1- mean of non-dejittered stimuli
% num- number of iterations required to obtain convergence
% pct- vector of change in percentage of mean variance of signal, over iterations
% sout- vector of output standard deviations for each iteration
%
% Modified June 10, 2012 by ZNA to adjust anchor point & allow for different dejittering methods

% [newmstim,sndx,newstim]=realign2m(stim,mstim,anchor, range[,s,zero_shift])
% [newmean,sndx,newstim]=realign2s(stim,mstim,anchor, range,s)
% [newmean,sndx,newstim]=realign2(stim,mstim,anchor, range[,s,zero_shift])

% to produce an initial model, define the model type, e.g.
%    mstim2 = gmm(dim_mean,1,'spherical');
% and then fit to an approriate portion of
%    opts=foptions;
%    mstim2=gmminit(mstim2,stim(:,(anchor-dim_mean-1):anchor),opts); 
% dim_mean is an extra parameter, making the size of the mean explicit.
% opts irrelevant for this case - mean and covariance re-assigned.

%Default value
if nargin<8||isempty(djlab), djlab='dprod'; end %default use dot product method- faster

%Hardcode parameters
crit=1e-5; %convergence criteria (pct change in variance between iterations)
sout(1)=s; %saving input penalty term

%Change stim input to gmm
mean2=mean(rawwaves(:,xfront+1:xfront+leng,:));
mean1=mean2;
pct=[];
absval=mean(var(rawwaves(:,xfront+1:xfront+leng,:))); %for next iteration
for ind=1:n, %for defined number of iterations
	if strcmp(djlab, 'dprod') %if doing dot product dejittering
	    if nargin<7, %if we don't have s as 6th parameter
	        [mean2,sndx,stims]=realign2(rawwaves,mean2,xfront+tback,win);
	    else    
	        [mean2,sndx,stims]=realign2(rawwaves,mean2,xfront+tback,win,s,1);
	    end
	elseif strcmp(djlab, 'fullgauss') %if doing gaussian dejittering
	    if nargin<7, %if we don't have s as 6th parameter
	        [mean2,sndx,stims]=realign2m(rawwaves,mean2,xfront+tback,win);
	    else    
	        [mean2,sndx,stims]=realign2m(rawwaves,mean2,xfront+tback,win,s,1);
	    end
	else error('Wrong dejittering method, please use ''dprod'' or ''fullgauss''')
	end
    absval(end+1)=mean(var(stims)); %mean of variance
    pct(end+1)=diff(absval(end-1:end))/absval(end-1);
    if abs(pct(end))<=crit, 
        disp(['Attained Convergence in ' num2str(ind) ' cycles'])
        num=ind; %how many iterations converge
        break
    end
%     s=std(sndx); %dynamically changing penalty term
    sout(ind+1)=std(sndx); %recording the changes to penalty term
end %for defined number of iterations
if ~exist('num','var'), disp(['Did not converge']), num=n;, end %if didn't break, went to max # iterations