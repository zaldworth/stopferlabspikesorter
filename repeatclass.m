function spikes_out=repeatclass(spikes,opts,totalreps,stepsize,thresh_pct,thresh_euc,frsttrial,p)

% spikes_out=repeatclass(spikes,opts,totalreps,stepsize,thresh_pct,thresh_euc,frsttrial,p);
%
% Function to iteratively apply model to classify data, and then update the model based on the
% classification output.
% Inputs:
% spikes- input model (will be adjusted over iterations)
% opts- opts structure for classification, should include 'xval', but need not include 'ntrials' nor 'bdndx' fields.
% totalreps- the total number of repetitions of the stimulus that will be analyzed
% stepsize- number of repetitions of the stimulus to analyze at each classification iteration
% thresh_pct- percentage threshold value (scale from 0:1) for automatic outlier removal
% thresh_euc- (optional) euclidean distance threshold value, should be checked before hand.
% frsttrial- (optional) first trial to use for analysis
% p- proportion of data from previous model to retain (default=0.4)
% Outputs:
% spikes_out- output spikes structure, nested per iteration.
%
% July 18, 2012, ZNA



%Set up parameters
if nargin<7||isempty(frsttrial) frsttrial=1; end %default- start at first trial
if nargin<8||isempty(p) p=.4; end %default- retain 40% of old data to make model
nsteps=ceil((totalreps-frsttrial+1)/stepsize); %how many iterations
tmpmod=spikes;


for ind=1:nsteps %for each iteration

	disp(['Starting step ' num2str(ind) ' of ' num2str(nsteps) ' ...'])

	%Set up classification options
	if ind<nsteps %if not last step
		opts.ntrials=stepsize*ind+(frsttrial-1); %adjust number of trials to analyze
	else %if last step
		opts.ntrials=totalreps; %adjust number of trials to analyze
	end %if not last step
	opts.bdndx=1:((ind-1)*stepsize+frsttrial-1); %adjust number of indices of trials to skip
	%xval should already be a field!!
	
	%Do classification
	spikes_out(ind)=pouzatclass1(tmpmod,opts); %classification step
	
	%Outlier Removal
	spikes_out(ind)=removeoutliers_aut(spikes_out(ind),thresh_pct); %remove outliers, mahalanobis
	if nargin>=6&~isempty(thresh_euc) %if euclidean distance also specified
		spikes_out(ind)=removeoutliers_aut(spikes_out(ind),thresh_euc,[],'euclidean'); %remove outliers, euclidean
	end %if euclidean distance also specified

	%Update model
	tmpmod=adjustmodel(tmpmod,spikes_out(ind),p);
end