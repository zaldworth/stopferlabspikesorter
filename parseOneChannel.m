function [data] = parseOneChannel(fpth, prefix, numtrials, skip_trials, channelNum)
%[data] = parseOneChannel(fpth,prefix,numtrials,skip_trials,channelNum)
%Reads the data from one channel of a multi-trial experiment directory
%Output is a matrix containing as many rows as the number of trials
%fpth		-> Path of the directory containing the data files
%prefix		-> File prefix (usually representing odor name and concentration) 
%numtrials	-> Number of trials 
%skip_trials-> Array indicating the index (base 1) of trials that need to be skipped. Leave it as [] if no skipping is needed
%channelNum -> The channel number (the extension at the end of the filename)
    
data=[];
for trial=1:numtrials
	if isempty(intersect(trial,skip_trials))
		filename = [fpth filesep prefix '_t',sprintf('%02.0f',trial),'.',sprintf('%02.0f',channelNum)];  
		FID=fopen(filename,'rb','ieee-be');
	    data=[data; fread(FID,[1,inf],'int16')];	    
	    fclose(FID);
	end
end

return
  


