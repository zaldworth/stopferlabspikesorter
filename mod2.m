function [n,m]=mod2(x,y)

% [n,m]=mod2(x,y)
%
% Function to determine index quotient and remainder after division for inputs x and y.
% Uses matlab built-in to find remainder after division.
% Inputs:
% x- scalar or vector numerator
% y- scalar or vector (same size as x) divisor
% Outputs:
% n- index quotient, i.e. ceil(x./y)
% m- remainder after division, i.e. x - (n-1).*y
% 
% June 13, 2012, ZNA

n = ceil(x./y);
m = x - floor(x./y).*y;

