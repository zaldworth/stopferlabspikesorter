function [opts,sndx_good,sndx_bad]=set_env1(subs_dj,opts,madk);




[jnk,spkndx]=sort(max(abs(subs_dj),[],2),1,'descend');
s_med=median(subs_dj)*opts.mf; %median of all waveforms
s_mad=mad(subs_dj,1)*madk; %MAD of all waveforms
if ~isfield(opts, 'threshv2')||isempty(opts.threshv2) %if threshv2 parameter not given
	tf=(1:round(opts.spwin(2)*opts.fs/1e3))/opts.fs*1e3-opts.spwin(1);
	tv1=[0 .1 .2 .3 .4];
	thv=repmat(tv1, round(opts.spwin(2)*opts.fs/1e3), 1); %.*opts.rthresh;
	f1=figure;
	set(f1, 'Position', [100 20 700 910])
	plot(tf, s_med(1:round(opts.spwin(2)*opts.fs/1e3)), 'k')
	hold on
	plot(tf,thv, '--')        
	legs{1}='Median spike';
	for ind=2:length(tv1)+1, legs{ind}=['threshv2=' num2str(tv1(ind-1))];, end
	set(gca, 'YTick', [])
	legend(legs, 'Location', 'North')
	figure(f1)
	opts.threshv2=input('Enter vertical threshold level: ');
%	keyboard
end %if threshv2 parameter not given

if ~isfield(opts, 'threshv')||isempty(opts.threshv) %if threshv parameter not given

    ndx_ab=find(s_med>opts.threshv2); %median values greater than 0
    ndx_ab=ndx_ab(ndx_ab<round(opts.spwin(2)*opts.fs/1e3)); %just for first window
    subs_r=subs_dj*opts.mf;
    subs_r(:,ndx_ab)=0; %waveforms rectified at zero for median values>0
	status_out=1; %status of output
	f2=figure;
	set(f2, 'Position', [100 20 700 910])
	while status_out==1


		tf=(1:round(opts.spwin(2)*opts.fs/1e3))/opts.fs*1e3-opts.spwin(1);
		tv1=[3 5 7 9];
		pport=5;
		tmpndx1=1:pport:size(subs_r,1);
		tmpndx=spkndx(1:floor(size(subs_r,1)/pport));
		p1=(1-normcdf(tv1,0,1))*100;
		thv=repmat(tv1, round(opts.spwin(2)*opts.fs/1e3), 1).*repmat(s_mad(1:round(opts.spwin(2)*opts.fs/1e3)), length(tv1), 1)'+repmat(s_med(1:round(opts.spwin(2)*opts.fs/1e3)), length(tv1), 1)';
		thv1=repmat(tv1, round(opts.spwin(2)*opts.fs/1e3), 1).*-repmat(s_mad(1:round(opts.spwin(2)*opts.fs/1e3)), length(tv1), 1)'+repmat(s_med(1:round(opts.spwin(2)*opts.fs/1e3)), length(tv1), 1)';
		thv(ndx_ab,:)=nan; thv1(ndx_ab,:)=nan;
		cla
		plot(tf, s_med(1:round(opts.spwin(2)*opts.fs/1e3)), 'k', 'LineWidth', 2)
		hold on
		plot(tf, thv, '--')
		plot(tf, subs_r(tmpndx,1:round(opts.spwin(2)*opts.fs/1e3))', '--', 'Color', [.8 .8 .8])
		plot(tf, s_med(1:round(opts.spwin(2)*opts.fs/1e3)), 'k', 'LineWidth', 2)
		plot(tf, thv, '--')
		plot(tf, thv1, '--')
		xlabel('Time to spike (ms)')
		ylabel('SD of data')
		legs{1}='Median spike';
		for ind=2:length(tv1)+1, legs{ind}=['thresh=' num2str(tv1(ind-1)) ', lose ~' num2str(p1(ind-1)) '% of ''good'' data'];, end
		legs{length(legs)+1}=['Largest ' num2str(1/pport*100) '% of total spike data'];
		legend(legs, 'Location', 'North')
		yls=get(gca, 'YLim');
		threshv_tmp=input('Enter vertical threshold level: ');

		r_max=max(subs_r-repmat(s_med+threshv_tmp*s_mad,size(subs_dj,1),1),[],2); %find samples that have values falling outside of med+th*mad envelope
		lowenv=s_med-threshv_tmp*s_mad;
		lowenv(ndx_ab)=-inf;
		r_min=min(subs_r-repmat(lowenv,size(subs_dj,1),1),[],2); %find samples that have values falling outside of med+th*mad envelope
		sndx_bad=find(r_max>0|r_min<0); %index of superposition events
		sndx_good=setdiff(1:size(subs_r,1),sndx_bad); %index of 'clean' events

%		subplot(1,2,2)
%		thv2=threshv_tmp*s_mad(1:round(opts.spwin(2)*opts.fs/1e3))+s_med(1:round(opts.spwin(2)*opts.fs/1e3));
%		thv3=-threshv_tmp*s_mad(1:round(opts.spwin(2)*opts.fs/1e3))+s_med(1:round(opts.spwin(2)*opts.fs/1e3));
%		thv2(ndx_ab)=nan; thv3(ndx_ab)=nan;
%		plot(tf, thv2, 'r', 'LineWidth', 2)
%		hold on
%		plot(tf, subs_r(intersect(sndx_good,tmpndx1),1:round(opts.spwin(2)*opts.fs/1e3))', '--', 'Color', [.8 .8 .8])
%		plot(tf, thv3, 'r', 'LineWidth', 2)
%		set(gca, 'YLim', yls)
%		clear legs2
%		legs2{1}='Proposed threshold';
%		legs2{2}=[num2str(1/pport*100) '% of non-outlier spike data'];
%		legend(legs2, 'Location', 'Best')
		figure(f2)


		disp(['Rejected ' num2str(length(sndx_bad)) ' out of ' num2str(length(sndx_bad)+length(sndx_good)) ' events as outliers'])
		disp('Type 0 for continuing with these values')
		disp('Type 1 for choosing different threshold value for outliers')
		opt=input('Option= ');


		if (opt==0) %use current values
			status_out=0; %break out of loop
		elseif (opt==1) %select new value
		else disp('Sorry, invalid option; try again.');		
		end %use current values



	end %while status_out==1
	opts.threshv=threshv_tmp;
else %if threshv parameter is given
    ndx_ab=find(s_med>opts.threshv2); %median values greater than 0
    subs_r=subs_dj*opts.mf;
    subs_r(:,ndx_ab)=0; %waveforms rectified at zero for median values>0
	r_max=max(subs_r-repmat(s_med+opts.threshv*s_mad,size(subs_dj,1),1),[],2); %find samples that have values falling outside of med+th*mad envelope
	sndx_bad=find(r_max>0); %index of superposition events
	sndx_good=find(r_max<=0); %index of 'clean' events
end %if threshv parameter not given




%spikes.params.threshv=opts.threshv; %scale value of mad above which to reject waveforms as outliers
%subs_good=subs_dj(sndx_good,:); clear subs_dj
%spikes.waveforms=reshape(subs_good, length(sndx_good), size(subs_good,2)/length(opts.spkchan), length(opts.spkchan)); %aligned spike waveforms
%spikes.outliers.times=sp1(sndx_bad); %outlier spike times
%sp1=sp1(sndx_good);
%spikes.unwrapped_times=sp1/opts.fs; %Not trial dependent
%spikes.info.detect.event_channel=spikes.info.detect.event_channel(sndx_good);
