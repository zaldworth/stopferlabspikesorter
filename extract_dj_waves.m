function [subs_dj]=extract_dj_waves(subs1,sndx,xf,rsmp)

% [subs_dj]=extract_dj_waves(subs1,sndx,xf,rsmp);
%
% Function to extract multi-dimensional dejittered waveforms from buffered, upampled, pre-jittered waveforms,
% given set of dejittering indices.



%Deriving values
num_spikes=size(subs1,1); %number of spikes
num_samps=size(subs1,2); %number of sample points/spikes
num_chans=size(subs1,3); %number of recorded channels
tgt_samps=num_samps-round(2*xf*rsmp); %aiming for this size

indx1=repmat(round(xf*rsmp)+1:round(xf*rsmp)+tgt_samps, [num_spikes 1]);
shfts=repmat(sndx', [1 tgt_samps]);
indx2=indx1+shfts;

subs_dj=zeros(num_spikes, tgt_samps, num_chans); %initialize
for ind=1:num_spikes, subs_dj(ind,:,:)=subs1(ind,indx2(ind,:),:);, end