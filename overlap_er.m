function fres2=overlap_er(spikes,ndx)

% fres2=overlap_er(spikes,ndx)
%
% Function to examine false positive and false negative spikes per cluster, depending on proximity of clusters to a
% single neighboring cluster



for ind1=1:length(ndx)
	for ind2=1:length(ndx)
		if ind1~=ind2,  q1(:,:,ind1,ind2)=gaussian_overlap(spikes.waveforms_pca(spikes.assigns==ndx(ind1),:),spikes.waveforms_pca(spikes.assigns==ndx(ind2),:));, end
	end
end

for ind1=1:length(ndx), fres2(ind1,1)=sum(squeeze(q1(1,1,ind1,:)));, fres2(ind1,2)=sum(squeeze(q1(1,2,ind1,:)));, end

