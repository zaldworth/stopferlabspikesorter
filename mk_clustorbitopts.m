function opts=mk_clustorbitopts(spikes)

% opts=mk_clustorbitopts(spikes)
%
% Function to make default 'opts' structure for clusterorbitopts.m function


opts.plotlab='clust'; %default- look at data color-coded by cluster
opts.nd1=1; %default x axis = pca dim #1
opts.nd2=2; %default y axis = pca dim #2
opts.nd3=3; %default z axis = pca dim #3
opts.dphi=1; %default rotation about z, in degrees
opts.dthe=0; %default rotation about x, in degrees
opts.nclusts=max(spikes.assigns); %default number of clusters
opts.idx=spikes.assigns; %default labelling of data events into clusters
opts.cmat=spikes.info.kmeans.colors; %default color coding per cluster
