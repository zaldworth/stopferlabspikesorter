function xzoom(figurenum,xmin,xmax)

% xzoom(figurenum,xmin,xmax);
% function sets all xlims in the figure 'figurenum' to the values xmin and xmax

aa=get(figurenum, 'Children');
bb=length(aa);
for ind=1:bb,
   axes(aa(ind))
   xlim([xmin xmax])
end
