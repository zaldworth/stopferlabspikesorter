function f1=pcaplot(events,v,s);

% f1=pcaplot(events,v,s);
%
% Function to show 3 characteristics of PCAs and projections onto PCA vectors:
% 1) The cumulative amount of variance explained by eigenvalues 1:j, as a function of j
% 2) The additional portion of variance explained by additional eigenvalues
% 3) The distribution of data projected onto eigenvectors (more gaussian-shaped distributions
%	imply less clusterability from respective eigenvectors)
% Inputs:
% events- data events to project, N data x m samples/data
% v- matrix of eigenvectors, sorted in descending order of eigenvalue
% s- matrix of eigenvalues, sorted in descending order of eigenvalues
% Outputs:
% f1- handle of figure window
%
% May 23, 2012, ZNA

numpcs=min([30 round(size(v,1)/2)]); %never plot more than 30 pca dimensions

%Project data onto eigs
aa=events*v;
xl1=max(max(aa));
xl2=min(min(aa));
xs=xl2:(xl1-xl2)/100:xl1;
for ind=1:14, aa2(ind,:)=hist(aa(:,ind),xs);, end

f1=figure;
set(f1, 'Position', [447 121 1081 853])
subplot(2,2,1)
plot(repmat([0 size(v,1)],3,1)', [.05 .05; .03 .03; .01 .01]', '--')
xlabel('PCA dim #')
hold on
plot(diag(s(1:numpcs,1:numpcs))/sum(diag(s)), '.')
ylabel('Prop of variance/PCA dim')
legend('5%', '3%', '1%', 'Location', 'East')
xlim([0 numpcs+1])

subplot(2,2,3)
plot(cumsum(diag(s(1:numpcs,1:numpcs)))/sum(diag(s)), '.')
xlabel('PCA dim #')
ylabel('Cumulative prop of variance')
xlim([0 numpcs+1])
ylim([0 1])

subplot(2,2,2)
plot(xs,aa2(1:7,:))
title('Dist of spikes projected onto 1st-7th PCAs')
legend('1st PCA', '2nd PCA', '3rd PCA', '4th PCA', '5th PCA', '6th PCA', '7th PCA', 'Location', 'EastOutside')

subplot(2,2,4)
plot(xs,aa2(8:14,:), '--')
title('Dist of spikes projected onto 8th-14th PCAs')
legend('8th PCA', '9th PCA', '10th PCA', '11th PCA', '12th PCA', '13th PCA', '14th PCA', 'Location', 'EastOutside')

