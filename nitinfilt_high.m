function filt_signal=nitinfilt_high(signal, lfro, fs)

%  filt_signal=nitinfilt_high(signal, lfro, fs);
%
%  Function for high passing a signal using 3-pole butterworth
%  Input variables:
%  lfro= low-frequency-roll-off (in Hz)
%  fs= sampling frequency of signal (in Hz)
%
%  2014-01-17, by ZNA, based on code by NG

%define frequency vector in normalized units (1=fs/2)
lf=lfro/(fs/2);

%building the filter
[b, a]=butter(3, lf, 'high');

%filtering the original signal
filt_signal=filtfilt(b,a,signal);
