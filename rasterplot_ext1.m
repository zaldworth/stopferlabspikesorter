function rasterplot_ext1(spikes,ndx,cndx)

% rasterplot_ext1(spikes,ndx,cndx)
% 
% Function to plot raster of responses to repeated stimulation.
%
% ~March 3rd, 2011 by ZNA
% Modified Jan 16, 2012 by ZNA- now for use with multiple cells
% Modified Apr 23, 2012 by ZNA to work with initially sorted kleinfeld data

%if nargin<1||isempty(prefix), prefix=uigetfile('*.*','Choose file with prefix'); prefix=prefix(1:end-7); end %get prefix manually
if nargin<2||isempty(ndx), ndx=1:max(spikes.trials); end %default- use all repeats
if nargin<3||isempty(cndx), cndx=1:size(spikes.labels,1);, end %default- use all cells
%[c,tnd]=intersect(trialndx,ndx); %which indices in trialndx do we want to plot?
nreps=length(ndx); %number of repeats to plot
nc=length(cndx); %number of cells

sts1=cell(nc,1); sts2=sts1; %initializing
for ind1=1:nc %for each recorded cell
	cellndx=find(spikes.assigns==cndx(ind1)); %index of spikes corresponding to specified cell
	for ind=1:nreps %for each stimulus repeat
		repndx=find(spikes.trials(cellndx)==ndx(ind)); %index of spikes corresponding to specified trial
	    sts1{ind1}(end+1:end+length(repndx))=ind; %repeat indices
	    sts2{ind1}(end+1:end+length(repndx))=spikes.spiketimes(cellndx(repndx)); %spike times w/in repeat
	end %for each stimulus repeat
end %for each recorded cell
% qq=tt
figure
plot([sts2{1}; sts2{1}], [sts1{1}-.4; sts1{1}+.4], 'k')
if nc>1 %if more than one cell
	cvect=[1 0 0; 0 0 1; 0 .5 0; 0 .67 .67; .67 0 .67; .67 .67 0; .4 .4 .4]; %selection of colors for plotting- current max of 8 cells
	for ind1=1:nc-1 %for each extra cell
		hold on
		plot([sts2{ind1+1}; sts2{ind1+1}], [sts1{ind1+1}-.4; sts1{ind1+1}+.4], 'Color', cvect(ind1,:))
	end %for each extra cell
end %if 2 cells
ylim([.5 nreps+.5])
xlabel('Time (s)')
ylabel('Repeat #')
