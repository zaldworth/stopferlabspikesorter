function multiSortedRaster(spikes,showUnits)
% multiSortedRaster(spikes,showUnits)
% Plots rasters for all units, trials and files, in the same plot.
% if showUnits (optional) is provided, only the specified units are shown
% (others are grayed out, but kept in the plot)
% NG

if nargin<2||isempty(showUnits), 
	showUnits = 1:spikes.params.kmeans_clustersize; 
end %default

numUnits = spikes.params.kmeans_clustersize;
numFiles = max(spikes.files);
numTrials = max(spikes.trials); % assume same number of trials for each file
maxDuration = max(spikes.info.detect.dur(:)); % maximum length of a trial in any of the files
cmat = spikes.info.kmeans.colors;

figure;
hold on;
for u_ind = 1:numUnits
	all_x = []; 
	all_y = [];
	for f_ind = 1:numFiles	
		for t_ind = 1:numTrials
			y = (u_ind-1)*(numTrials+1)+t_ind;			
			start_x = maxDuration*(f_ind-1);
			selectIndices = logical((spikes.files==f_ind) .* reshape((spikes.assigns==u_ind),1,[]) .* (spikes.trials==t_ind));
			x = start_x + spikes.spiketimes(selectIndices);
			all_x = [all_x x];
			all_y = [all_y y*ones(size(x))];
			if mod(t_ind,5)==0
				tmpcolor = [0.8 0.8 0.8];
			else
				tmpcolor = [0.95 0.95 0.95];
			end
			line(start_x+[0 maxDuration],[y y],'color',tmpcolor);	
		end		
	end	
	line([0 maxDuration*numFiles],[y+1 y+1],'color','k','LineWidth',2);	
	thisColor = [0 0 1]; % thisColor = cmat(u_ind,:);
	if isempty(find(showUnits == u_ind, 1))
		thisColor = 0.8*[1 1 1];
	end	
	plot([all_x; all_x], [all_y-0.35; all_y+0.35],'Color',thisColor); 
	text(-1*.2*numFiles,y-numTrials/2,num2str(u_ind),'color',cmat(u_ind,:));
end

all_xtics = [];
all_xticsLabels = [];
for f_ind = 1:numFiles	
	start_x = maxDuration*(f_ind-1);		
	xtics = start_x:2:f_ind*maxDuration-1;
	all_xtics = [all_xtics xtics];
	all_xticsLabels = [all_xticsLabels; num2str((xtics-start_x)')];	
	if numFiles > 1
		line([start_x start_x],[0 numUnits*(numTrials+1)],'color','k');	
		text(start_x+1,-.5,regexprep(spikes.opts.prefix{f_ind},{'_','^.*\'},{'',''}),'fontSize',6,'Rotation',0);
		text(start_x+1,-.5-y/25,regexprep(spikes.opts.prefix{f_ind},{'_','\\.*$'},{'',''}),'fontSize',6,'Rotation',0);
	end
end

set(gca,'YTick',[]);			
set(gca,'XTick',all_xtics);			
set(gca,'XTickLabel',all_xticsLabels);			
set(gca, 'Ydir', 'reverse')
set(gca,'TickDir','out'); 
set(gca,'TickLength',[0.002 0.002]);

xlabel('Time(s)');
ylim([0 numUnits*(numTrials+1)]);
if numFiles > 1
	set(gca,'position',[0.02 0.05 0.98 0.84]);
else
	set(gca,'position',[0.06 0.05 0.94 0.94]);
end
set(gcf, 'PaperPositionMode', 'manual');
width = max(numFiles+1,4);
height = max(numUnits+1,4);
set(gcf, 'PaperSize',[width height]);
set(gcf, 'PaperPosition', [0 0 width height]);	

end
