function plot_clustmodwaves(spikes,plotopt,plotMedians,showTime)  

% plot_clustmodwaves(spikes,plotopt)
%
% Function to plot per-cluster set of waveforms along with cluster center (median)
% for each cluster in data set- useful for visually identifying amount of contamination
% via outliers or superpositions.
% Inputs:
% spikes- output of pouzatsort or pouzatclust
% plotopt- scalar value for which subset of waveforms to examine:
%	1- all waveform
%	2- all non-superposition events only
%	3- all superposition events only
%	4- 1st spike of superpositions only
%	5- 2nd spike of superpositions only
%	(default=1)

% everywhere replaced max(spikes.assigns) with spikes.params.kmeans_clustersize

if nargin<2||isempty(plotopt), plotopt=1; end %default
if nargin<3||isempty(plotMedians), plotMedians=0; end %default  
if nargin<4||isempty(showTime), showTime=0; end %default  
if ~isfield(spikes, 'firstsupndx'), plotopt=1; end %can't plot superpositions without indices

%set up number of columns in figure
if spikes.params.kmeans_clustersize>=1 && spikes.params.kmeans_clustersize<=5, ncols=1; %changed to make sure it doesn't crash when there is only 1 cluster
elseif spikes.params.kmeans_clustersize>5 && spikes.params.kmeans_clustersize<=10, ncols=2;
elseif spikes.params.kmeans_clustersize>10 && spikes.params.kmeans_clustersize<=15, ncols=3;
elseif spikes.params.kmeans_clustersize>15 && spikes.params.kmeans_clustersize<=20, ncols=4;
elseif spikes.params.kmeans_clustersize>20 && spikes.params.kmeans_clustersize<=25, ncols=5;
else error('Too many (or zero) clusters')
end

nrows=ceil(spikes.params.kmeans_clustersize/ncols);

figure
set(gcf, 'Position', [600 150 575 875])
for ind=1:spikes.params.kmeans_clustersize
	subplot(nrows,ncols,ind)
	aa=find(spikes.assigns==ind);

	%check plot option, if necessary remove indices & waveforms
	if plotopt==1 %plot all events
	elseif plotopt==2 %non-superposition spikes only
		[bb,ndx1]=intersect(aa,[spikes.firstsupndx spikes.secndsupndx]);
		spikes.waveforms_clust{ind}(ndx1,:,:)=[];
	elseif plotopt==3 %superposition events only
		[bb,ndx1]=intersect(aa,[spikes.firstsupndx spikes.secndsupndx]);
		ndx_bad=setdiff(1:size(spikes.waveforms_clust{ind},1), ndx1);
		spikes.waveforms_clust{ind}(ndx_bad,:,:)=[];
	elseif plotopt==4 %1st spike of superpositions only
		[bb,ndx1]=intersect(aa,[spikes.firstsupndx]);
		ndx_bad=setdiff(1:size(spikes.waveforms_clust{ind},1), ndx1);
		spikes.waveforms_clust{ind}(ndx_bad,:,:)=[];
	elseif plotopt==5 %2nd spike of superpositions only
		[bb,ndx1]=intersect(aa,[spikes.secndsupndx]);
		ndx_bad=setdiff(1:size(spikes.waveforms_clust{ind},1), ndx1);
		spikes.waveforms_clust{ind}(ndx_bad,:,:)=[];
	else error('Please choose a plotopt from 1 to 5') %bad value
	end %plot all events
		
	if size(spikes.waveforms_clust{ind},1)==0, continue, end
	
	%did this so that the colors of the waveforms can display information about time. time increases from blue to red		
	hold all;	
	if ~showTime
		plot(spikes.waveforms_clust{ind}', '--'); 
	else		
		eventTimes = spikes.unwrapped_times(spikes.assigns==ind);	
		mymax = max(spikes.unwrapped_times);
		mycolors = jet(50); % I don't need to do this every iteration of the loop, but keepint it here for easy of following
		colorIndices = 1 + floor(50*eventTimes/(mymax+1));
		for i = 1:size(spikes.waveforms_clust{ind},1)		
			plot(spikes.waveforms_clust{ind}(i,:),'--','color',mycolors(colorIndices(i),:));
		end		
	end	
	
	plot(median(spikes.waveforms_clust{ind}), 'k', 'LineWidth', 2)
	title(['Clust # ' num2str(ind) ', N=' num2str(size(spikes.waveforms_clust{ind},1))], 'Color', spikes.info.kmeans.colors(ind,:))
	set(gca, 'XColor', spikes.info.kmeans.colors(ind,:), 'YColor', spikes.info.kmeans.colors(ind,:))
	axis tight
	yls(ind,:)=get(gca, 'YLim');
end

yls2(1)=min(yls(:,1)); yls2(2)=max(yls(:,2));

winx=((spikes.params.window_size*spikes.params.Fs/1e3):(spikes.params.window_size*spikes.params.Fs/1e3):(spikes.params.window_size*spikes.params.Fs/1e3)*(size(spikes.waveforms, 3)-1))+1;

for ind=1:spikes.params.kmeans_clustersize
	subplot(nrows,ncols,ind)
	plot(repmat(winx,2,1), repmat(yls2',1,(size(spikes.waveforms, 3)-1)), 'Color', spikes.info.kmeans.colors(ind,:))
	if ind>spikes.params.kmeans_clustersize-ncols, xlabel('Sample'), else set(gca, 'XTickLabel', []), xlabel([]),  end
end
%figure(gcf)
% yzoom(gcf, yls2(1), yls2(2))


% added a new figure showing the median waveforms for all clusters in
%same plot
if plotMedians
	figure; hold on; 
	for ind=1:spikes.params.kmeans_clustersize
		plot(median(spikes.waveforms_clust{ind}), '--', 'color', spikes.info.kmeans.colors(ind,:),'Linewidth',2)	
	end
	axis tight
	plot(repmat(winx,2,1), repmat(yls2',1,(size(spikes.waveforms, 3)-1)),'color','k')
end