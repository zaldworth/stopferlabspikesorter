function spikes=stitchtrials2(spikes1,spikes2);





spikes=spikes1;
spikes.assigns(end+1:end+length(spikes2.assigns))=spikes2.assigns;
spikes.spiketimes(end+1:end+length(spikes2.spiketimes))=spikes2.spiketimes;
%spikes.unwrapped_times(end+1:end+length(spikes2.unwrapped_times))=spikes2.unwrapped_times+spikes.info.detect.dur*max(spikes.trials);
spikes.unwrapped_times(end+1:end+length(spikes2.unwrapped_times))=spikes2.unwrapped_times+sum(spikes1.dur)/spikes1.params.Fs;
spikes.trials(end+1:end+length(spikes2.trials))=spikes2.trials+max(spikes.trials);
%for ind=1:length(qq), qq3(ind)=qq2(qq(ind));, end
spikes.files(end+1:end+length(spikes2.files))=spikes2.files;
spikes.dur(end+1:end+length(spikes2.dur))=spikes2.dur;
spikes.zs(end+1:end+length(spikes2.zs))=spikes2.zs;
spikes.info.trial_ndx(end+1:end+length(spikes2.info.trial_ndx))=spikes2.info.trial_ndx;
spikes.info.detect.event_channel(end+1:end+length(spikes2.info.detect.event_channel))=spikes2.info.detect.event_channel;

if isfield(spikes, 'firstsupndx')
	spikes=rmfield(spikes, 'firstsupndx');
	spikes=rmfield(spikes, 'secndsupndx');
	spikes=rmfield(spikes, 'waveforms');
	spikes=rmfield(spikes, 'waveforms_pca');
	spikes=rmfield(spikes, 'waveforms_clust');
	spikes=rmfield(spikes, 'waveforms_clust_pca');
end

%Need to add:
%spikes.info.trial_ndx, spikes.info.detect.event_chan, spikes.info.kmeans.assigns


%Should carry over:  
%spikes.labels
%spikes.info.prefix, spikes.info.clustmeth, spikes.info.spkchan, spikes.info.rig, spikes.info.madk, spikes.info.align, spikes.info.interface_energy, spikes.info.tree
%spikes.info.detect.align_sample, spikes.info.detect.cov, spikes.info.detect.dur, spikes.info.detect.stds, spikes.info.detect.mdx, spikes.info.detect.thresh
%spikes.info.pca.s, spikes.info.pca.u, spikes.info.pca.v, spikes.info.pca.ndim
%spikes.info.kmeans.B, spikes.info.kmeans.centroids, spikes.info.kmeans.colors,spikes.info.kmeans.iteration_count,spikes.info.kmeans.mse,spikes.info.kmeans.num_clusters,
%	spikes.info.kmeans.randn_state, spikes.info.kmeans.T, spikes.info.kmeans.W
%spikes.params.Fs, spikes.params.detect_method, spikes.params.thresh , spikes.params.window_size , spikes.params.shadow , spikes.params.cross_time, spikes.params.refractory_period
%	spikes.params.max_jitter, spikes.params.agg_cutoff, spikes.params.kmeans_clustersize, spikes.params.jitterreps, spikes.params.jitterspent, spikes.params.threshv
%all of spikes.params.display

%Should we keep for assessment?:  spikes.waveforms, spikes.waveforms_pca, spikes.waveforms_clust, spikes.waveforms_clust_pca
