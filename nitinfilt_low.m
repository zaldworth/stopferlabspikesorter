function filt_signal=nitinfilt_low(signal, hfro, fs)

%  filt_signal=nitinfilt_low(signal, hfro, fs);
%
%  Function for low passing a signal using 3-pole butterworth
%  Input variables:
%  hfro= high-frequency-roll-off (in Hz)
%  fs= sampling frequency of signal (in Hz)
%
%  2012-01-28, by ZNA, based on code by NG

%define frequency vector in normalized units (1=fs/2)
hf=hfro/(fs/2);

%building the filter
[b, a]=butter(3, hf, 'low');

%filtering the original signal
filt_signal=filtfilt(b,a,signal);
