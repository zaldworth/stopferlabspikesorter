function etimes=sortdata2spikes(spikes,prefix,clustndx,nin,nlfp,neag,npid);

% etimes=sortdata2spikes(spikes,prefix,clustndx,nin,nlfp,neag,npid);
%
% Function to take the output of spike sorting and translate into the typical '*.spikes.mat'
% output structure for intracellular spike times.  If *.spikes.mat file already exists 
% (presumably from intracellular recording), then this will open the file up and simply
% append the new cells to the existing 'etimes' variable.
% Inputs:
% spikes- output structure from spike classification
% prefix- prefix of filename for saving
% clustndx- index of which clusters from 'spikes' to save- it is important that some quality
%	assessment has been done to assure that these represent good clusters (i.e. false negative/
%	false positive rates less than 2%)
% nin- index of input (i.e. stimulus) channels
% nlfp- index of lfp channel (use '[]' if not recorded)
% neag- index of eag channel (use '[]' if not recorded)
% npid- index of pid channel (use '[]' if not recorded)
% Outputs:
% etimes- etimes structure similar to that from 'thresh_s.m', that is, cell array of spikes
%	with first index for different cells, second index for different trials.
%
% July 20, 2012, ZNA

outname=[prefix '.spikes.mat'];

if ~exist(outname,'file') %If file does not exist, need to set up parameters
	fs=spikes.params.Fs;
	ms=fs/1e3;
	trialndx=unique(spikes.trials);
	etimes=cell(0,length(trialndx));
	nout=repmat(spikes.info.spkchan(1),1,length(clustndx));
% 	nout=[];
	misc_chan=[];

else %Otherwise load it (will overwrite nin, nlfp, neag)
   	disp('Spike file already exists')
	load(outname);
end %If file does not exist, need to set up parameters

for ind1=1:length(clustndx) %for each cell to save
	ndx=find(spikes.assigns==clustndx(ind1)); %spikes belonging to this cell
%     nout(end+1)=mode(spikes.info.detect.event_channel(ndx));
	numcells=size(etimes,1); %number of cells
	for ind2=trialndx %for each trial to examine
		tndx=find(spikes.trials==ind2); %spikes belonging to this trial
		ctndx=intersect(ndx,tndx); %spikes belonging to this cell and this trial
		etimes{numcells+1,ind2}=round(spikes.spiketimes(ctndx)*spikes.params.Fs)';
		
	end %for each trial to examine
end %for each cell to save

if ~exist(outname,'file') %If file does not exist, save reduced number of variables
	evals=etimes;
	drops=0;
%	seg_end=sum(spikes.dur);
	seg_end=spikes.dur(1,1);
	N=seg_end;
	trialnum=repmat(trialndx(end)+1,1,size(etimes,1));
	gr=size(etimes,1);
	save(outname,'etimes','evals','drops','seg_end','N','fs','ms', 'nin', 'nout', 'nlfp', 'neag', 'npid', 'misc_chan', 'trialndx', 'trialnum', 'gr')
else %Need to re-save all original *.spikes.mat variables
	save(outname,'etimes','evals','drops','seg_end','N','fs','ms', 'nin', 'nout', 'nlfp', 'neag', 'npid', 'misc_chan', 'trialndx', 'trialnum', 'gr')
end