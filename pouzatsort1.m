function spikes=pouzatsort1(opts)

% spikes=pouzatsort1(opts);
%
% Function to build models for automated spike classification in matlab according 
% to methods of Pouzat (Pouzat et al, J Neurosci Methods 2002,
% http://www.biomedicale.univ-paris5.fr/physcerv/C_Pouzat/ReproducibleDataAnalysis/LocustDataSet/locust.html ).
% Clustering is accomplished through either use of 'kmeans' method or expectation-maximization ('EM')
% method with kmeans seeding.
%
% Inputs:
% opts- structure array with the following fields:
%	prefix- list (character or cell array) of prefix names of files to analyze
%	fs- sampling frequency of files to analyze
%	spkchan- index of channels in file for which to analyze
%	ntrials- total number of trials to analyze (include skipped files in count)
%	bdndx- index of trials to skip
%	clustmeth- method for clustering data, currently either 'kmeans' or 'EM' or 'auto'
%	nclusts- number of classes for clustering- can be single valued or a list of class values, in which 
%		case Bayesian Information Criteria will be used to select best level
%	ndim- number of PCA dimensions to use for projection into low-D space, prior to clustering
%	spwin- two element vector describing the window for spike waveforms.  The first element is the time
%		(in ms) back from peak to start the spike window, and the second element i the time length (in ms)
%		of the spike window 
%	mf- multiplicative factor by which to multiply physiological data prior to finding spike times.  The value
%		should be either -1 or +1, reflecting whether the 'peak' of the spike is negative or positive, respectively
%	rthresh- threshold value for finding spikes in extracellular data (a single value for all channels- spikes only
%		need exceed thresh value on a single channel to be detected)
%	threshv- scale value of mean+threshv*SD above which to reject waveforms as outliers
%	threshv2- scale value for finding range of mean spike waveform+threshv2*SD below which we look for superposition events
%	band_lims- highpass (lower) and lowpass (upper) frequency limits for bandpass filter, defaults to [50 min([opts.fs/2-1 3e3])]
%	furtherMultipliersTo_mf - vector of 1s and -1s, same length as spkchan, telling whether to invert the data values. Default: Leave empty for no flip
%
% Note- fields for 'nclusts', 'ndim', 'spwin', 'rthresh', 'threshv', 'threshv2', and 'mf' can be left empty, in
% which case a user interfaces will be available for entering values
%
% Outputs:
% spikes- structure array largely following convention of Kleinfeld et al's UltraMegaSort2000,
%	with a few specific additions.
%	(see http://physics.ucsd.edu/neurophysics/lab/UltraMegaSort2000%20Manual.pdf for details)
%
% ZNA, May 5th, 2012.

%Default parameter
if ~isfield(opts, 'band_lims')||isempty(opts.band_lims), opts.band_lims=[50 min([opts.fs/2-1 3e3])]; end%if band_lim parameter not given

%Preliminary generation of Kleinfeld structure
if ~strfind(path, 'kleinsort'), addkleinsort, end
spikes=ss_default_params(opts.fs); %need to adjust for .params: thresh, window size, shadow, cross time, refract period, max_jitter, kmeans_clustersize
spikes.params.detect_method='manual'; % differs from kleinfeld auto/manual paradigm
spikes.params.refractory_period=2.5; % ms, refractory period (for calculation refractory period violations)
spikes.params.shadow= 2/3; % ms, enforced dead region after each spike 
spikes.params.xf=round(opts.fs/1e3/3); %define 1/3 ms buffer- dejittering parameter
spikes.params.max_jitter=spikes.params.xf/opts.fs*1e3; % ms, width of window used to detect peak after threshold crossing
spikes.params.jitterreps=5; %maximum of 10 realignment iterations- dejittering parameter
spikes.params.jitterpen=0.1*opts.fs/1e3; %penalty term (SD of assumed underlying gaussian) for dejittering
spikes.params.rsmp=10; %resampling factor for dejittering
spikes.params.djlab='dprod'; %do dot product dejittering (faster)
spikes.info.prefix=opts.prefix; %Names of files used to build models
% spikes.info.clustmeth=opts.clustmeth; %Method used for clustering data --- moved below
spikes.info.spkchan=opts.spkchan; %which channls used to build models
spikes.info.trial_ndx=setdiff(1:opts.ntrials, opts.bdndx); %index of which trials to look at
spikes.info.madk=1.4826;  %normalization constant from R implementation of MAD

if iscell(opts.prefix)
	nfiles=length(opts.prefix);
else
	nfiles=size(opts.prefix,1);
end

rng(0); %initialize random number generator to fixed state

%Read in Data & Filter
disp('Reading in data ...')
tic
if ~isfield(opts,'furtherMultipliersTo_mf') || isempty(opts.furtherMultipliersTo_mf) 
	opts.furtherMultipliersTo_mf = ones(size(opts.spkchan)); 
end
for ind1=1:nfiles %for each file
	for ind2=1:length(spikes.info.trial_ndx) %for each trial
		for ind3=1:length(opts.spkchan) %for each recorded channel
			if iscell(opts.prefix), pf=opts.prefix{ind1};, else pf=opts.prefix(ind1,:);, end %filename
			data1{ind1,ind2}(ind3,:)=opts.furtherMultipliersTo_mf(ind3) * parseOneChannel('.',pf,spikes.info.trial_ndx(ind2),...
				[1:spikes.info.trial_ndx(ind2)-1],opts.spkchan(ind3)); %Read in data 
			data1{ind1,ind2}(ind3,:)=nitinfilt(detrend(data1{ind1,ind2}(ind3,:), 'linear'),opts.band_lims(1),opts.band_lims(2),opts.fs); %band pass filter
%			data1{ind1,ind2}(ind3,:)=parsePouzatChannel([], pf, opts.spkchan(ind3));
		end %for each recorded channel
		dur1(ind1,ind2)=size(data1{ind1,ind2},2); %duration of each trial, in sample points
	end %for each trial
end %for each file
spikes.info.detect.dur=dur1/opts.fs; %duration of each trial- generate as data is loaded, assume all trials have same duration
data=zeros(length(opts.spkchan),0); %initialize variable
for ind1=1:nfiles, for ind2=1:length(spikes.info.trial_ndx), data(:,end+1:end+spikes.info.detect.dur(ind1,ind2)*opts.fs)=detrend(data1{ind1,ind2}','linear')';, end, end %fold data in
t1=toc;
disp(['Data loaded in ' num2str(t1) ' seconds.'])
clear daqbits data1 dur1 ind1 ind2 ind3 pf spkvoltlim %save some space

%Determine channel-wise MAD, normalize
if ~isfield(opts, 'mf')||isempty(opts.mf) %if mf parameter not given
	tf=(1:opts.fs)/opts.fs;
	f1=figure;
	set(f1, 'Position', [560 30 600 900])
	for ind=1:length(opts.spkchan) %for each data channel
		subplot(length(opts.spkchan),1,ind)
		plot(tf,data(ind,1:opts.fs), 'k')
		ylabel(['Chan ' num2str(opts.spkchan(ind))]) 
	end %for each data channel
	opts.mf=input('Select multiplier facter ''1'' or ''-1'': '); %look at peaks or troughs
	spikes.info.mf=opts.mf;
	clear ind tf
else %if mf parameter is given
	spikes.info.mf=opts.mf;
end %if mf parameter not given
if abs(opts.mf)~=1, error('Please select ''1'' or ''-1'''), end %error check
disp('Filtering and rectifying data ...')
tic
for ind=1:length(opts.spkchan), spikes.info.detect.mds(ind,:)=spikes.info.madk*mad(data(ind,:),1);, end                

%Normalize by MAD
data_m=zeros(size(data));
for ind=1:length(opts.spkchan), data_m(ind,:)=data(ind,:)/spikes.info.detect.mds(ind);, end
for ind=1:length(opts.spkchan), spikes.info.detect.stds(ind)=spikes.info.madk*mad(data_m(ind,:),1);, end %actually MAD in this case
clear data

%Rectify
data_r=spikes.info.mf*data_m;

% Summed Channel Thresholds commented below
% if ~isfield(opts, 'rthresh')||isempty(opts.rthresh) %if rthresh parameter not given
% 	opts=getthreshvalueSummedChannels(opts,data_r);
% end %if rthresh parameter not given
% spikes.params.thresh=repmat(opts.rthresh,1,length(opts.spkchan)); % for 'pouzat', set number of MADs above background noise
% t1=toc;
% disp(['Filtering completed in ' num2str(t1) ' seconds.'])
% %Spike detect on summed, rectified, filtered data
% spikes.info.detect.thresh=ones(size(opts.spkchan))*opts.rthresh; %per channel thresh value- not strictly true here since thresh was on summed channels
% disp('Detecting spikes ...')
% tic
% sp1=schmidttriggerZA_auto(sum(data_r,1),[],2/3*opts.rthresh/std(sum(data_r,1)),opts.rthresh/std(sum(data_r,1)));
% t1=toc;
% disp(['Detected ' num2str(length(sp1)) ' spikes in ' num2str(t1) ' seconds.'])

clear data_f
t1=toc;
disp(['Filtering completed in ' num2str(t1) ' seconds.'])
 

all_sp = []; all_sizes = [];
for ind=1:length(opts.spkchan) % 
	if ~isfield(opts, 'rthresh')|| length(opts.rthresh) ~= length(opts.spkchan) %if rthresh parameter not given
		opts.rthresh(ind)=getthreshvalue(opts,data_r(ind,:));
	end
    tic
    [events eventSizes] = schmidttriggerZA_auto(data_r(ind,:),[],2/3*opts.rthresh(ind)/std(data_r(ind,:),1),opts.rthresh(ind)/std(data_r(ind,:),1));
    all_sp =[all_sp events];        
	all_sizes = [all_sizes eventSizes];
    disp(sprintf(['Channel: ' num2str(opts.spkchan(ind)) '\t| Threshold:\t' num2str(opts.rthresh(ind)) '\t| Spikes ' num2str(length(events))]));
%	close 
end
disp('Merging spikes across channels')
spikeArray = zeros(1, max(all_sp)+1);
[all_sp all_indices] = sort(all_sp);
spikeArray(all_sp)=all_sizes(all_indices);
[jnk, locations] = findpeaks(spikeArray,'minpeakdistance',ceil(spikes.params.shadow*opts.fs/1000)); % Use matlab's findpeaks function
  % we are basically interested in the minpeakdistance feature here, to remove smaller peaks that are within the shadow period. 
sp1 = locations;
clear spikeArray data_r all_sp all_sizes
t1=toc;
disp(['Detected ' num2str(length(sp1)) ' spikes in ' num2str(t1) ' seconds.'])
spikes.params.thresh=opts.rthresh; % for 'pouzat', set number of MADs above background noise 
spikes.info.detect.thresh=opts.rthresh; %per channel thresh value- not strictly true here since thresh was on summed channels 

%Extract spike events
disp('Extracting events ...')
tic
if ~isfield(opts, 'spwin')||isempty(opts.spwin) %if spike window range not given
	bdndx=find(sp1<3.5*opts.fs/1e3+spikes.params.xf);
	sp1(bdndx)=[]; %getting rid of spikes too close to beginning of file
	bdndx=find(sp1+(3.5)*opts.fs/1e3>length(data_m));
	sp1(bdndx)=[]; %getting rid of spikes too close to end of file
	subs1=getwinz(data_m',sp1-round(3.5*opts.fs/1e3)-1,7*opts.fs/1e3+1);
	subs1=reshape(subs1, length(sp1), size(subs1,2)/length(opts.spkchan), length(opts.spkchan)); %waveforms broken out per channel
	[jnk,evch]=max(squeeze(max(subs1,[],2)),[],2); %index of channel which had biggest signal for every spike waveform
	for ind=1:length(evch), subs2(ind,:)=squeeze(subs1(ind,:,evch(ind)));, end
	clear subs1
	tf=(-3.5*opts.fs/1e3:3.5*opts.fs/1e3)/opts.fs*1e3;
	f3=figure;
	plot(tf, median(subs2), 'k', 'LineWidth', 2)
	hold on
	plot(tf, spikes.info.madk*mad(subs2,1), 'r', 'LineWidth', 2)
	plot([tf(1) tf(end)], [1 1], '--', 'Color', [0 0 1], 'LineWidth', 2)
	plot([tf(1) tf(end)], [0 0], 'k--')	
	legend('Spike Median', 'Spike MAD', 'Channel Noise', 'Location', 'SouthEast')
	display('Select time back from peak and time forward from peak for spike window')
	figure(f3)
	[xs1,jnk]=ginput(2);
	xs1=sort(round(xs1*10)/10);
	opts.spwin(1)=-xs1(1); %ms back from spike time for events
	opts.spwin(2)=diff(xs1); %ms extracted for spikewaveform
	clear subs2 xs1 jnk tf evch subs1 bdndx
else %if spike window range IS given
	bdndx=find(sp1<opts.spwin(1)*opts.fs/1e3+spikes.params.xf);
	sp1(bdndx)=[]; %getting rid of spieks too close to beginning of file
	bdndx=find(sp1+spikes.params.xf+(opts.spwin(2)-opts.spwin(1))*opts.fs/1e3>length(data_m));
	sp1(bdndx)=[]; %getting rid of spikes too close to end of file
	clear bdndx
end %if spike window range not given
spikes.params.window_size=opts.spwin(2); % ms, width of a spike
spikes.params.cross_time=opts.spwin(1); % ms, alignment point for peak of waveform
spikes.info.detect.align_sample=opts.spwin(1)*opts.fs/1e3; %sample point in waveforms for threshold crossing (actually peak here)
subs=getwinz(data_m',sp1-round((opts.spwin(1)*opts.fs/1e3))-(spikes.params.xf+1),round(opts.spwin(2)*opts.fs/1e3)+2*spikes.params.xf);
subs1=reshape(subs,size(subs,1), round(opts.spwin(2)*opts.fs/1e3)+2*spikes.params.xf, length(opts.spkchan)); clear subs %break into component channels
[jnk,spikes.info.detect.event_channel]=max(squeeze(max(spikes.info.mf*subs1,[],2)),[],2); %channel index for biggest signal for every spike waveform

%Extract noise sample events
ntimes=round(rand(1,8e3)*(length(data_m)-opts.spwin(2)*opts.fs/1e3)-1)+1; %random sample points from throughout recording.
bdndx=[]; %initializing list of bad random times
for ind=1:length(ntimes) %for each random time
	tmpvect=min(abs(sp1-ntimes(ind))); %time to nearest spike event
	if tmpvect<(opts.spwin(2)*opts.fs/1e3)*.5, %if too close to nearest spike
		bdndx(end+1)=ind;
	end
end
ntimes(bdndx)=[];
subs_n=getwinz(data_m',ntimes,round(opts.spwin(2)*opts.fs/1e3));
spikes.info.detect.cov=cov(detrend(subs_n)); %noise covariance
t1=toc;
disp(['Event extraction completed in ' num2str(t1) ' seconds.'])
clear subs_n ntimes bdndx tmpvect jnk

%Dejittering
disp('Aligning samples ...')
tic
subs2=zeros(size(subs1,1),round(size(subs1,2)*spikes.params.rsmp),length(opts.spkchan));
for ind=1:length(opts.spkchan), subs2(:,:,ind)=resample(subs1(:,:,ind)', spikes.params.rsmp, 1)';, end 
subs3=zeros(size(subs2,1),size(subs2,2));
for ind=1:size(subs2,1), subs3(ind,:)=squeeze(subs2(ind,:,spikes.info.detect.event_channel(ind)));, end
[mean2,sndx]=realign2_wrapper_ext(subs3,round(spikes.params.xf*spikes.params.rsmp),round(opts.spwin(2)*opts.fs/1e3*spikes.params.rsmp),...
	round((opts.spwin(2)*opts.fs/1e3)*spikes.params.rsmp),round((spikes.params.xf-6)*spikes.params.rsmp),spikes.params.jitterreps,...
	round(spikes.params.jitterpen*spikes.params.rsmp),spikes.params.djlab);  %max dot product dejittering
clear subs1
[subs_dj2]=extract_dj_waves(subs2,sndx,spikes.params.xf,spikes.params.rsmp);
sp1=sp1+round(sndx/spikes.params.rsmp);
subs_dj=resample(reshape(subs_dj2,size(subs_dj2,1),size(subs_dj2,2)*size(subs_dj2,3)  )', 1, spikes.params.rsmp)'; clear subs_dj2
spikes.info.align=1; %Set to 1 if alignment was run
t1=toc;
disp(['Alignment completed in ' num2str(t1) ' seconds.'])
clear subs2 subs3 sndx mean2

%Get clean events- reject superpositions & other outliers
disp('Finding ''clean'' events ...')
tic
[opts,sndx_good,sndx_bad]=set_env1(subs_dj,opts,spikes.info.madk); %set parameters (if necessary), get clean events
spikes.params.threshv=opts.threshv; %scale value of mad above which to reject waveforms as outliers
subs_good=subs_dj(sndx_good,:);
spikes.waveforms=reshape(subs_good, length(sndx_good), size(subs_good,2)/length(opts.spkchan), length(opts.spkchan)); %aligned spike waveforms
spikes.outliers.times=sp1(sndx_bad); %outlier spike times
spikes.outliers.waveforms=subs_dj(sndx_bad,:); %aligned spike waveform
clear subs_dj
sp1=sp1(sndx_good);
spikes.unwrapped_times=sp1/opts.fs; %Not trial dependent
spikes.info.detect.event_channel=spikes.info.detect.event_channel(sndx_good);
t1=toc;
disp([num2str(length(sndx_bad)) ' events rejected in ' num2str(t1) ' seconds.'])

%PCA on spike waveforms
disp('Projecting into PCA space ...')
tic
[spikes.info.pca.v,spikes.info.pca.s]=eig(cov(subs_good)); %eigenvalues & principal component vectors
spikes.info.pca.v=fliplr(spikes.info.pca.v); spikes.info.pca.s=fliplr(flipud(spikes.info.pca.s)); %inverting order so that biggest pca dimension comes first
if ~isfield(opts, 'ndim')||isempty(opts.ndim) %if # of pca dimensions not given
	f5=pcaplot(subs_good, spikes.info.pca.v, spikes.info.pca.s);
	display('How many PCA dimensions to use in clustering?')
	figure(f5)
	opts.ndim=input('Dimensionality = ');
end %if # of pca dimensions not given
spikes.info.pca.ndim=opts.ndim; %number of principal components used in clustering

%Projecting into PCA subspace
nspikes=length(sndx_good); %total number of 'good' spikes
spikes.info.kmeans.randn_state=randn('state'); %seed value of random number generator before running kmeans
spvect=randperm(nspikes); %random reshuffling
modspikes=spvect(1:round(nspikes/4*3)); %spikes for building model (3/4 of all spikes)
testspikes=setdiff(spvect, modspikes); %spikes for testing model (1/4 of all spikes)
spikes.waveforms_pca=subs_good(modspikes,:)*spikes.info.pca.v(:,1:opts.ndim); %aligned spike waveforms projected into pca space
testwaves=subs_good(testspikes,:)*spikes.info.pca.v(:,1:opts.ndim); %aligned spike waveforms projected into pca space
t1=toc;
disp(['Projection into PCA space completed in ' num2str(t1) ' seconds.'])

%Number of clusters for subsequent clustering algorithms?
if ~isfield(opts, 'nclusts')||isempty(opts.nclusts) %if # of clusters not given
	f6=clusterorbitplot2(spikes);
	display('Observe projected data and select range of clusters')
	figure(f6)
	opts.nclusts=input('Cluster range = ');
end %if # of clusters not given

% this is a quick fix to achieve a good trade-off between speed (EM) and robustness to crash for small number of spikes (kmeans)
if strmatch(opts.clustmeth, 'auto')  
	if nspikes > 100*max(opts.nclusts)
		opts.clustmeth = 'EM';
	else
		opts.clustmeth = 'kmeans';
	end
	disp(['Choosing clustmeth = ' opts.clustmeth]);
end
spikes.info.clustmeth=opts.clustmeth; %Method used for clustering data

%Do Preliminary clustering with partial data
for ind=1:length(opts.nclusts) %for every desired level of clustering	
	%Do clustering
	if strmatch(opts.clustmeth, 'kmeans') %Kmeans in projected space
		tic	
		idx1(ind,:)=kmeans(spikes.waveforms_pca,opts.nclusts(ind),'Replicates',200); t1=toc;		
	elseif strmatch(opts.clustmeth, 'EM') %EM algorithm, projected space
		tic
		options=foptions; %setting up default options for matlab's optimization routine
		options(1)=-1; %don't display warning or error messages
		options(14)=5; %running kmeans algorithm 5 iterations
		mix=gmm(opts.ndim,opts.nclusts(ind),'full',options); %initialize mixture models to have 'nclusts' centers, each of 'ndim' dimensionality, and 'spherical' covariance structure'
		mix=gmminit_z(mix,spikes.waveforms_pca,options); %initial data layout from kmeans
		options(14)=200; %running EM algorithm for  max. 30 iterations
		mix=gmmem(mix,spikes.waveforms_pca,options); %optimize models using EM algorithm
		[post,act]=gmmpost(mix, spikes.waveforms_pca); %obtaining posterior probabilities from model
		[jnk,idx1(ind,:)]=max(post,[],2); t1=toc; %using maximum posterior probability as label		
	else error('Improper clustering method, please use ''kmeans'' or ''EM''')
	end % which type of clustering?
	display(['Initial clustering to ' num2str(opts.nclusts(ind)) ' clusters using ' opts.clustmeth ' methodology, finished in ' num2str(t1) 'seconds'])
	%Get bayesian information criteria for cluster level
	spikes.assigns=idx1(ind,:); 
	spikes.params.bv(ind)=getbic(spikes,testwaves);   
end %for every desired level of clustering

%Re-do clustering with min. BIC level & full data set
clear idx1 modspikes testspikes testwaves spvect
figure
plot(opts.nclusts, spikes.params.bv)
xlabel('# of clusters')
ylabel('BIC')
title([opts.clustmeth ' clustering'])
[jn,nd1]=min(spikes.params.bv);
spikes.params.kmeans_clustersize=opts.nclusts(nd1); %  number of clusters, NOT miniclusters as in Kleinfeld
spikes.labels=[1:opts.nclusts(nd1); ones(1,opts.nclusts(nd1))]'; %Cx2 (C=# of clusters)
colormap jet
clrs=colormap;
cndx=round(1:(63/(opts.nclusts(nd1)-1)):64);
spikes.info.kmeans.colors=clrs(cndx,:); clear clrs cndx %color matrix storing color values (1x3) for each cluster
display(['BIC minimum at ' num2str(opts.nclusts(nd1)) ' clusters'])
spikes.waveforms_pca=subs_good*spikes.info.pca.v(:,1:opts.ndim); %aligned spike waveforms projected into pca space
if strmatch(opts.clustmeth, 'kmeans') %Kmeans in projected space
	tic
	idx1=kmeans(spikes.waveforms_pca,opts.nclusts(nd1),'Replicates',200); t1=toc;
elseif strmatch(opts.clustmeth, 'EM') %EM algorithm, projected space
	tic, %addpath /Users/zane/matlab/matlabcode/alex/netlab/ -BEGIN
	options=foptions; %setting up default options for matlab's optimization routine
	options(14)=5; %running kmeans algorithm 5 iterations
	mix=gmm(opts.ndim,opts.nclusts(nd1),'full',options); %initialize mixture models to have 'nclusts' centers, each of 'ndim' dimensionality, and 'spherical' covariance structure'
	mix=gmminit_z(mix,spikes.waveforms_pca,options); %initial data layout from kmeans
	options(14)=200; %running EM algorithm for  max. 30 iterations
	mix=gmmem(mix,spikes.waveforms_pca,options); %optimize models using EM algorithm
	[post,act]=gmmpost(mix, spikes.waveforms_pca); %obtaining posterior probabilities from model
	[jnk,idx1]=max(post,[],2);, t1=toc; %using maximum posterior probability as label
else error('Improper clustering method, please use ''kmeans'' or ''EM''')
end % which type of clustering?
display(['Final clustering using ' opts.clustmeth ' methodology, finished in ' num2str(t1) 'seconds'])

%Sort clusters from largest to smallest
disp('Final dejittering per class ...')
tic
for ind1=1:opts.nclusts(nd1)
	qq=median(subs_good(idx1==ind1,:),1);
	qq2=reshape(qq,size(spikes.waveforms,2),length(opts.rthresh));
	qq3=max(qq2*opts.mf);
	szval(ind1)=max(qq3);
end %size of each cluster
clear id_ord
[jnk,id_ord]=sort(szval,'descend'); %sort clusters by size
clear idx
for ind1=1:opts.nclusts(nd1), idx(idx1==id_ord(ind1))=ind1;, end %apply new label to clusters
spikes.assigns=idx'; %indices of labels for cluster ID for each waveform
clear szval id_ord idx1

%ID single largest event channel for entire cluster
if length(spikes.info.spkchan)>1
	for ind=1:opts.nclusts(nd1) %for each cluster
		subs1=spikes.waveforms(find(spikes.assigns==ind),:);
		subs1=reshape(subs1,size(subs1,1),size(subs1,2)/length(opts.spkchan), length(opts.spkchan));
		subs2=squeeze(mean(subs1,1));
		[j1,chan_nd]=max(squeeze(max(spikes.info.mf*subs2,[],1))./spikes.info.detect.thresh,[],2);
		spikes.info.detect.event_channel(find(spikes.assigns==ind))=chan_nd;
	end
	clear subs1 subs2 chan_nd
else
	spikes.info.detect.event_channel=ones(size(spikes.assigns));
end

%Align spike waveforms (dejitter) within clusters
for ind=1:opts.nclusts(nd1)
	ndx=find(idx==ind);
	subs_buff=getwinz(data_m',sp1(ndx)-round((opts.spwin(1)*opts.fs/1e3))-(spikes.params.xf+1),round(opts.spwin(2)*opts.fs/1e3)+2*spikes.params.xf);
	subs_buff1=reshape(subs_buff,size(subs_buff,1), round(opts.spwin(2)*opts.fs/1e3)+2*spikes.params.xf, length(opts.spkchan)); %break into component channels
	subs_buff2=zeros(size(subs_buff1,1),round(size(subs_buff1,2)*spikes.params.rsmp),length(opts.spkchan));
	for ind1=1:length(opts.spkchan), subs_buff2(:,:,ind1)=resample(subs_buff1(:,:,ind1)', spikes.params.rsmp, 1)';, end 
	subs_buff3=zeros(size(subs_buff2,1),size(subs_buff2,2));
	for ind1=1:size(subs_buff2,1), subs_buff3(ind1,:)=squeeze(subs_buff2(ind1,:,spikes.info.detect.event_channel(ndx(ind1))));, end
	[mean2,sndx]=realign2_wrapper_ext(subs_buff3,round(spikes.params.xf*spikes.params.rsmp),round((opts.spwin(2)*opts.fs/1e3)*spikes.params.rsmp),...
		round((opts.spwin(2)*opts.fs/1e3)*spikes.params.rsmp),round((spikes.params.xf-6)*spikes.params.rsmp),spikes.params.jitterreps,...
		round(spikes.params.jitterpen*spikes.params.rsmp),spikes.params.djlab);  %max dot product dejittering
	clear subs_buff subs_buff1 subs_buff3
	[subs_dj2]=extract_dj_waves(subs_buff2,sndx,spikes.params.xf,spikes.params.rsmp);
	clear subs_buff2
	sp1(ndx)=sp1(ndx)+round(sndx/spikes.params.rsmp);
	spikes.waveforms_clust{ind}=resample(reshape(subs_dj2,size(subs_dj2,1),size(subs_dj2,2)*size(subs_dj2,3)  )', 1, spikes.params.rsmp)'; clear subs_dj2
	spikes.waveforms_clust_pca{ind}=spikes.waveforms_clust{ind}*spikes.info.pca.v(:,1:opts.ndim);
end
t1=toc;
disp(['Dejittering completed in ' num2str(t1) ' seconds.'])
clear sndx mean2 jnk data_m

%Generate rest of Kleinfeld structure
spikes.spiketimes=spikes.unwrapped_times; %initialize
spikes.trials=zeros(size(sp1)); spikes.files=spikes.trials; %initialize

ut = spikes.unwrapped_times;
f_start = 0; % time that represents the start of the file (updated in each loop)
t_start = 0; % time that represents the start of the trial (updated in each loop)
for f_ind=1:nfiles %for each file analyzed	
	for t_ind=1:length(spikes.info.trial_ndx) % for each trial 		
		t_end = t_start+spikes.info.detect.dur(f_ind,t_ind);
		spikes.spiketimes(ut >= t_start & ut < t_end) = spikes.spiketimes(ut >= t_start & ut < t_end) - t_start;
		spikes.trials(ut >= t_start & ut < t_end) = t_ind * ones(size(spikes.spiketimes(ut >= t_start & ut < t_end)));
		t_start = t_end; % for next round
	end 	
	f_end = t_end;
	spikes.files(ut >= f_start & ut < f_end) = f_ind * ones(size(spikes.spiketimes(ut >= f_start & ut < f_end)));
	f_start = f_end; % for next round
end
spikes.opts=opts; %additionally saving input parameters

%%%%%%%%%%%%spikes.info array not completely appropriate for Pouzat sorting
%Minicluster info not relevant to this sorting- intitialize fields just in case they need to be there
%spikes.info.kmeans.assigns=idx'; %minicluster membership for each waveform (cheat so remove outliers works)
%spikes.info.kmeans.B=[]; %Between cluster scatter matrix
%spikes.info.kmeans.centroids=[]; %mean waveform form each minicluster
%spikes.info.kmeans.T=[]; %total scatter matrix
%spikes.info.kmeans.W=[]; %within-cluster scatter matrix
%spikes.info.interface_energy=[]; %between cluster interface energy- not relevant to this sorting
%spikes.info.kmeans.iteration_count=[]; %number of iterations of k-means performed on each pass
%spikes.info.kmeans.mse=[]; %mean squared distance of each waveform from appropriate minicluster centroid
%spikes.info.kmeans.num_clusters=opts.nclusts(nd1); %number or miniclusters
%spikes.info.tree=[]; %minicluster aggregation tree- not relevant to this sorting
