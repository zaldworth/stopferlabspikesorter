function spikes1=dejitterclusts(spikes,show);


spikes1=spikes; %copy all of structure
subs_sort=spikes.waveforms(find(spikes.assigns==show),:,:);
sz1=size(subs_sort); %how many spikes in cluster & how long spike is (time points/chan*#of channs)
if sz1(1)>1 %if there are samples to dejitter
	if length(spikes.info.spkchan)>1
        subs_buff=zeros([sz1(1) sz1(2)+2*spikes.params.xf sz1(3)]); %initialize variable
    else
        subs_buff=zeros([sz1(1) sz1(2)+2*spikes.params.xf]); %initialize variable
    end
    initv=subs_sort(:,1,:);, exitv=subs_sort(:,sz1(2),:); %initial and final values of subs
	subs_buff(:,1:spikes.params.xf,:)=repmat(initv,1,spikes.params.xf); %buffer with front with first point value
	subs_buff(:,spikes.params.xf+sz1(2)+1:sz1(2)+2*spikes.params.xf,:)=repmat(exitv,1,spikes.params.xf); %buffer back with last point value
	subs_buff(:,spikes.params.xf+1:spikes.params.xf+sz1(2),:)=subs_sort; %buffer subs
	subs_buff2=zeros(sz1(1),round((sz1(2)+2*spikes.params.xf)*spikes.params.rsmp),length(spikes.info.spkchan));
	for ind=1:length(spikes.info.spkchan), subs_buff2(:,:,ind)=resample(subs_buff(:,:,ind)', spikes.params.rsmp, 1)';, end 
	subs_buff3=squeeze(subs_buff2(:,:,mode(spikes.info.detect.event_channel(find(spikes.assigns==show)))));
	[mean2,sndx]=realign2_wrapper_ext(subs_buff3,round(spikes.params.xf*spikes.params.rsmp),round(spikes.params.window_size*spikes.params.Fs/1e3*spikes.params.rsmp),...
		round((spikes.params.window_size*spikes.params.Fs/1e3)*spikes.params.rsmp),round((spikes.params.xf-6)*spikes.params.rsmp),spikes.params.jitterreps,...
		round(spikes.params.jitterpen*spikes.params.rsmp),spikes.params.djlab);  %max dot product dejittering
	
	[subs_dj2]=extract_dj_waves(subs_buff2,sndx,spikes.params.xf,spikes.params.rsmp);
	spikes1.waveforms_clust{show}=resample(reshape(subs_dj2,size(subs_dj2,1),size(subs_dj2,2)*size(subs_dj2,3)  )', 1, spikes.params.rsmp)';
else
	spikes1.waveforms_clust{show}=reshape(subs_sort, size(subs_sort,1), size(subs_sort,2)*size(subs_sort,3));  % to handle the case when the cluster has only 1 event
end
%subs_dj=reshape(resample(reshape(subs_dj2,size(subs_dj2,1),size(subs_dj2,2)*size(subs_dj2,3)  )', 1, spikes.params.rsmp)',sz1);

