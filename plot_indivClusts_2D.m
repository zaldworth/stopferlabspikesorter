function plot_indivClusts_2D(spikes)
% plot_indivClusts_2D(spikes)
% A 2-D PCA plot for each cluster, with best PCAs selected separately for each cluster
% NG

figure
numC = spikes.params.kmeans_clustersize;
n1 = floor(sqrt(numC));
n2 = ceil(numC/n1);
mymax = max(spikes.unwrapped_times);

for ind = 1:numC
%	nitinsubplot(n1,n2,ind); hold all;
	subplot(n1,n2,ind); hold all;
	[e1,e2] = eig(cov(spikes.waveforms_clust{ind}));	
	eventTimes = spikes.unwrapped_times(spikes.assigns==ind);		
	
	selecedWaveforms = spikes.waveforms_clust{ind};		
	x1 = selecedWaveforms*e1(:,end);	y1 = selecedWaveforms*e1(:,end-1);	
	plot(x1, y1, 'o', 'MarkerSize', 4, 'Color', [0 0 0]);		
		
	set(gca,'XTick',[]); set(gca,'YTick',[]); title([num2str(ind) ' : ' num2str(length(eventTimes)) ' spikes']);
end

end
		
