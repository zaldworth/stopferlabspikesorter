function [ymin,ymax]=ymzoom
% [ymin,ymax]=ymzoom;
% function sets all xlims in the current figure to the values xmin and xmax
% Based on Zane Aldworth's xzoom
% use mouse: EST 10-14-2002
% press button one for normal zoom in
% press button two to set xlims to auto: Zane Aldworth 10-16-2002
% press button three to zoom out by 1/2 the curent width: EST 10-17-2002

aa=get(gcf, 'Children'); % get figure
bb=length(aa);
[X Y button]=ginput(2);
ymin=min(Y);
ymax=max(Y);
 if button == 2 %use mouse button two to zoom all the way out (apple/command)
	for ind=1:bb,
	   axes(aa(ind))
	   ylim auto
	end
elseif button==1  %use mouse button one to zoom in
   for ind=1:bb,
   	axes(aa(ind))
   	ylim([ymin ymax])
   end
elseif button==3 %use mouse button three to zoom out by 1/2 the curent width (ctrl)
   for ind=1:bb,
   	axes(aa(ind))
    CurrYLim=ylim; % get current xlimits
    ymin=CurrYLim(1,1)-(diff(CurrYLim)/4);
    ymax=CurrYLim(1,2)+(diff(CurrYLim)/4);
   	ylim([ymin ymax])
   end
end
 