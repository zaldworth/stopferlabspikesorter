function wav2=subtract_sup2(wav1,indices,clust_meds2,ndx,ndx2);



wavtmp=zeros(1,size(clust_meds2,2),size(clust_meds2,3));
wavtmp(1,ndx2{length(ndx)+1-indices(3)},:)=clust_meds2(indices(1),ndx{length(ndx)+1-indices(3)},:);
wavtmp=wavtmp(:)';


wav2=wav1-wavtmp;