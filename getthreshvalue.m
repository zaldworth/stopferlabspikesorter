function rthresh=getthreshvalue(opts,data_r)  

% opts=getthreshvalue(opts,data_r);
%
% Function to select a threshold level for detecting spikes.
% An initial figure suggests several threshold levels and allows user
% to select one with a GUI.  After selection of threshold, user can
% step through data to confirm that threshold level is appropriate.
%
% July 12, 2012, ZNA

stepsize=6; 
tndx=1:round(stepsize*opts.fs);
thv=repmat([2 4 6 8 10], 2, 1);
f2=figure;
set(f2, 'Position', [100 500 1400 450])
plot(tndx/opts.fs, data_r(tndx), 'Color', [.8 .8 .8]) % 
hold on
plot([tndx(1)/opts.fs tndx(end)/opts.fs], thv, '--', 'LineWidth', 2)
plot([tndx(1)/opts.fs tndx(end)/opts.fs], -1*thv, '--', 'LineWidth', 2)
xlabel('Time (sec)')
ylabel('SD of data')
% legend('Median', 'thresh=2', 'thresh=4', 'thresh=6', 'thresh=8', 'thresh=10', 'Location', 'Southwest') 
display('Select vertical threshold level')
figure(f2)
[jnk,rthresh]=ginput(1);


status_out=1;
while status_out==1 
	figure(f2)
	hold off
	plot(tndx/opts.fs, data_r(tndx), 'Color', [.8 .8 .8])  % 
	hold on
	plot([tndx(1)/opts.fs tndx(end)/opts.fs], [rthresh rthresh], 'r--', 'LineWidth', 2)
	xlim([tndx(1)/opts.fs tndx(end)/opts.fs])
	xlabel('Time (sec)')
	ylabel('SD of data')
	%legend('Data', 'ProposedThresh', 'Location', 'SouthEast')

	disp('Type');
	disp('0	for using selected threshold and exiting.');
	disp('1	for using selected threshold and stepping to next time window.');
	disp('2	for using selected threshold and jumping to user-defined time point.');
	disp('3	for selecting new treshold level.');
	disp('4	for selecting new step size.');
	opt=input('Option? ');	
	if (opt==0) % continue option
		status_out=0;
	elseif (opt==1)
		if round(stepsize*opts.fs)+tndx(end)<=size(data_r,2)%if enough data to step
			tndx=tndx+round(stepsize*opts.fs);
		else%if not enough data, shorten step
			tndx=fliplr(size(data_r,2):-1:size(data_r,2)-length(tndx)+1);
		end%if enough data to step
	elseif (opt==2)
		disp('Enter desired step, in seconds')
		tstep=input('Stepsize: ');
		if round(tstep*opts.fs)+tndx(end)<=size(data_r,2)%if enough data to step
			tndx=tndx+round(tstep*opts.fs);
		else%if not enough data, shorten step
			tndx=fliplr(size(data_r,2):-1:size(data_r,2)-length(tndx)+1);
		end%if enough data to step
	elseif (opt==3)
		display('Select vertical threshold level')
		figure(f2)
		[jnk,rthresh]=ginput(1);
	elseif (opt==4)
		disp('Enter desired stepsize, in seconds')
		stepsize=input('Stepsize: ');
		if round(stepsize*opts.fs)+tndx(1)-1<=size(data_r,2)%if enough data to step
			tndx=tndx(1):tndx(1)+round(stepsize*opts.fs)-1;
		else%if not enough data, shorten step
			tndx=fliplr(size(data_r,2):-1:size(data_r,2)-round(stepsize*opts.fs)+1);
		end%if enough data to step
	else disp('Sorry, invalid option; try again.');
	end %if (opt==0) 
end	%while status==1



