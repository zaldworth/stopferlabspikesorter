function d1 = fldplot(spikes,nd1,nd2)

% fldplot(spikes,nd1,nd2)
%
% Function to plot distribution of spike waveforms projected along FLD for pair of clusters.
d1 = nan; % initialize  
subs1=spikes.waveforms_pca(spikes.assigns==nd1,:);
subs2=spikes.waveforms_pca(spikes.assigns==nd2,:);

if length(subs1)*length(subs2) == 0
	return 
end

c1=cov(subs1);
c2=cov(subs2);
m1=mean(subs1);
m2=mean(subs2);
fldv=(m1-m2)/(c1+c2);
[jnk,xx1]=hist(subs1*fldv');
[jnk,xx2]=hist(subs2*fldv');
if isempty(xx1) || isempty(xx2) || max(abs(xx1)) > 1000000 || max(abs(xx2)) > 1000000
	return 
end

mn1=mean(subs1*fldv'); mn2=mean(subs2*fldv'); %cluster means in fisher space 
sd1=std(subs1*fldv'); sd2=std(subs2*fldv'); %cluster SDs in fisher space
d1a=abs(mn1-mn2); %distance between means
d1=d1a/max([sd1 sd2]); %maximal SD gives minimal distance

dv1=min([xx1(1) xx2(1)]):min([diff(xx1(1:2)) diff(xx2(1:2))]):max([xx1(end) xx2(end)]);
if isempty(dv1)
	return 
end
hvec=hist(subs1*fldv', dv1);
hvec(2,:)=hist(subs2*fldv', dv1);
%figure
b1=bar(dv1,hvec',2, 'histc'); 
set(b1(1), 'FaceColor', spikes.info.kmeans.colors(nd1,:), 'EdgeColor','none'); 
set(b1(2), 'FaceColor', spikes.info.kmeans.colors(nd2,:), 'EdgeColor','none'); 
axis tight; box off; axis off;

%for ind1=1:nclusts-1, for ind2=ind1+1:nclusts
%	subplot(nclusts-1,nclusts-1,(ind1-1)*(nclusts-1)+ind2-1)
%	fldplot(spikes.waveforms_pca,spikes.assigns,ind1,ind2,spikes.info.kmeans.colors)
%	if ind1==1, title(['#' num2str(ind2)]), end
%	if ind2==ind1+1, ylabel(['#' num2str(ind1)]), end
%end, end
