function plotClusterDrift(spikes)
% plotClusterDrift(spikes)
% Divides all spikes (per cluster) into three time-bands and projects them on two PCAs
% NG

figure
numC = spikes.params.kmeans_clustersize;
n1 = floor(sqrt(numC));
n2 = ceil(numC/n1);
mymax = max(spikes.unwrapped_times);

c1  = [0.5 0.5 1];
c2 = [0 1 0];
c3 = [1 0.3 0.3];

for ind = 1:numC
%	nitinsubplot(n1,n2,ind); hold all;
	subplot(n1,n2,ind); hold all;
	[e1,e2] = eig(cov(spikes.waveforms_clust{ind}));	
	eventTimes = spikes.unwrapped_times(spikes.assigns==ind);	
	
	firstIndices = logical(eventTimes <= mymax/3);
	secondIndices = logical((eventTimes > mymax/3) & (eventTimes <= 2*mymax/3));
	thirdIndices = logical(eventTimes > 2*mymax/3);		

	% first cluster
	selecedWaveforms = spikes.waveforms_clust{ind}(firstIndices,:);		
	x1 = selecedWaveforms*e1(:,end);	y1 = selecedWaveforms*e1(:,end-1);	
	plot(x1, y1, '+', 'MarkerSize', 3, 'Color', c1);	
	
	% second cluster
	selecedWaveforms = spikes.waveforms_clust{ind}(secondIndices,:);		
	x2 = selecedWaveforms*e1(:,end); y2 = selecedWaveforms*e1(:,end-1);	
	plot(x2, y2, 'o', 'MarkerSize', 3, 'Color', c2);	
		
	% third cluster
	selecedWaveforms = spikes.waveforms_clust{ind}(thirdIndices,:);		
	x3 = selecedWaveforms*e1(:,end); y3 = selecedWaveforms*e1(:,end-1);	
	plot(x3, y3, 'x', 'MarkerSize', 4, 'Color', c3);	
	
	% plot ellipses
	h = ellipse(iqr(x1),iqr(y1),0,median(x1),median(y1),c1); 
	set(h,'LineWidth',2,'LineStyle','--');
	h = ellipse(iqr(x2),iqr(y2),0,median(x2),median(y2),c2);
	set(h,'LineWidth',2,'LineStyle','--');
	h = ellipse(iqr(x3),iqr(y3),0,median(x3),median(y3),c3);
	set(h,'LineWidth',2,'LineStyle','--');
	
	set(gca,'XTick',[]); set(gca,'YTick',[]); title([num2str(ind) ' : ' num2str(length(eventTimes)) ' spikes']);
end

end
		
