function yzoom(figurenum,ymin,ymax)

% yzoom(figurenum,ymin,ymax);
% function sets all ylims in the figure 'figurenum' to the values ymin and ymax

aa=get(figurenum, 'Children');
bb=length(aa);
for ind=1:bb,
   axes(aa(ind))
   ylim([ymin ymax])
end
