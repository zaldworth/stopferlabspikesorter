function spikes1=pouzatclass1(spikes,opts)

% spikes1=pouzatclass1(spikes,opts);
%
% Function to use models from spike sorting ('pouzatsort1.m') in order to cluster
% clean spikes and superposition events from data. Events are clustered according to
% which model minimizes the mahalanobis distance from noise for residuals (see Pouzat
% et al, J Neurosci Methods, 2002).
%
% Inputs:
% opts- structure array with the following fields:
%	prefix- list (character or cell array) of prefix names of files to analyze
%	ntrials- total number of trials to analyze (include skipped files in count)
%	bdndx- index of trials to skip
%	xval- threshold value of mahalanobis distance above which events are considered superpositions,
%		and below which they are considered to be 'clean' spiking events from a single neuron
%
% Note- the field for 'xval' can be left empty, in which case a user interface will be available for 
%	entering values
%
% Outputs:
% spikes1- structure array largely following convention of Kleinfeld et al's UltraMegaSort2000,
%	with a few specific additions.
%	(see http://physics.ucsd.edu/neurophysics/lab/UltraMegaSort2000%20Manual.pdf for details)
%	(input field 'xval' is also included in the output structure)
%
% ZNA, May 5th, 2012.

%Start building output
spikes1.params=spikes.params; %param values, including sampling rate of files
spikes1.info.spkchan=spikes.info.spkchan; %channel indices
spikes1.info.madk=spikes.info.madk; %divisor to get SD value of each channel
spikes1.info.mf=spikes.info.mf; %positive or negative spikes
spikes1.info.clustmeth=spikes.info.clustmeth; %method used for clustering data
spikes1.info.detect.stds=spikes.info.detect.stds; %SD value of each channel
spikes1.info.detect.thresh=spikes.info.detect.thresh; %threshold value for finding spikes
spikes1.info.detect.cov=spikes.info.detect.cov; %noise covariance matrix
spikes1.info.detect.mds=spikes.info.detect.mds; %normalization coefficients
spikes1.info.pca=spikes.info.pca; %eigenvalues, eigenvectors, and # of dimensions to use
spikes1.info.kmeans.colors=spikes.info.kmeans.colors; %color list for each cluster
spikes1.info.trial_ndx=setdiff(1:opts.ntrials, opts.bdndx); %index of which trials to look at
spikes1.opts=spikes.opts; %model build params
spikes1.opts.prefix=opts.prefix; %update file name(s)

opts.spkchan = spikes.opts.spkchan; 
opts.fs = spikes.opts.fs;
opts.mf = spikes.opts.mf;

if iscell(opts.prefix)
	nfiles=length(opts.prefix);
else
	nfiles=size(opts.prefix,1);
end

rng(0); %: initialize random number generator to fixed state

%Read in Data & Filter
disp('Reading in data ...')
tic
for ind1=1:nfiles %for each file
	for ind2=1:length(spikes1.info.trial_ndx) %for each trial
		for ind3=1:length(spikes1.info.spkchan) %for each recorded channel
			if iscell(opts.prefix), pf=opts.prefix{ind1};, else pf=opts.prefix(ind1,:);, end %filename
			data1{ind1,ind2}(ind3,:)=spikes.opts.furtherMultipliersTo_mf(ind3) * parseOneChannel('.',pf,spikes1.info.trial_ndx(ind2),[1:spikes1.info.trial_ndx(ind2)-1],spikes1.info.spkchan(ind3)); %Read in data
			data1{ind1,ind2}(ind3,:)=nitinfilt(detrend(data1{ind1,ind2}(ind3,:), 'linear'),spikes.opts.band_lims(1),spikes.opts.band_lims(2),spikes.params.Fs); %band pass filter
%			data1{ind1,ind2}(ind3,:)=parsePouzatChannel([], pf, spikes1.info.spkchan(ind3));
		end %for each recorded channel
		dur1(ind1,ind2)=size(data1{ind1,ind2},2); %duration of each trial, in sample points
	end %for each trial
end %for each file

data=zeros(length(spikes1.info.spkchan),0); %initialize variable
for ind1=1:nfiles, for ind2=1:length(spikes1.info.trial_ndx), data(:,end+1:end+dur1(ind1,ind2))=detrend(data1{ind1,ind2}','linear')';, end, end %fold data in
clear data1 %save some space
t1=toc;
disp(['Data loaded in ' num2str(t1) ' seconds.'])

%Normalize by MAD
disp('Filtering and rectifying data ...')
tic
data_m=zeros(size(data));
for ind=1:length(spikes1.info.spkchan), data_m(ind,:)=data(ind,:)/spikes.info.detect.mds(ind);, end
clear data

t1=toc;
disp(['Filtering completed in ' num2str(t1) ' seconds.'])

disp('Detecting spikes ...')
tic

% %Spike detect on summed, rectified, filtered data
% sp1=schmidttriggerZA_auto(sum(spikes1.info.mf*data_m,1),[],2/3*spikes1.info.detect.thresh(1)/std(sum(data_m,1)),spikes1.info.detect.thresh(1)/std(sum(data_m,1)));
% bdndx2=find(sp1<2*(spikes1.params.cross_time*spikes1.params.Fs/1e3+spikes1.params.xf)|...
% 	sp1+2*(spikes1.params.window_size-spikes1.params.cross_time)*spikes1.params.Fs/1e3>length(data_m));
% sp1(bdndx2)=[]; %getting rid of spikes too close to end of file
% clear data_r
% t1=toc;
% disp(['Detected ' num2str(length(sp1)) ' spikes in ' num2str(t1) ' seconds.'])

data_r=spikes.info.mf*data_m;
all_sp = []; all_sizes = [];
rthresh = spikes1.info.detect.thresh;
for ind=1:length(opts.spkchan) 
	[events eventSizes] = schmidttriggerZA_auto(data_r(ind,:),[],2/3*rthresh(ind)/std(data_r(ind,:),1),rthresh(ind)/std(data_r(ind,:),1));    	
    all_sp =[all_sp events];        
	all_sizes = [all_sizes eventSizes];    
	disp(sprintf(['Channel: ' num2str(opts.spkchan(ind)) '\t| Threshold:\t' num2str(rthresh(ind)) '\t| Spikes ' num2str(length(events))]));
end
spikeArray = zeros(1, max(all_sp)+1);
[all_sp all_indices] = sort(all_sp);
spikeArray(all_sp)=all_sizes(all_indices);
[jnk, locations] = findpeaks(spikeArray,'minpeakdistance',ceil(spikes.params.shadow*opts.fs/1000)); % Use matlab's findpeaks function
  % we are basically interested in the minpeakdistance feature here, to remove smaller peaks that are within the shadow period. 
sp1 = locations;
clear spikeArray all_sp all_sizes all_indices;

bdndx2=find(sp1<2*(spikes1.params.cross_time*spikes1.params.Fs/1e3+spikes1.params.xf)|...
	sp1+2*(spikes1.params.window_size-spikes1.params.cross_time)*spikes1.params.Fs/1e3>length(data_m));
sp1(bdndx2)=[]; %getting rid of spikes too close to end of file
clear data_r
t1=toc;
disp(['Detected ' num2str(length(sp1)) ' spikes in ' num2str(t1) ' seconds.'])


%Extract noise sample events
disp('Extracting noise events ...')
tic
ntimes=round(rand(1,8e3)*(length(data_m)-round(spikes1.params.window_size*spikes1.params.Fs/1e3))-1)+1; %random sample points from throughout recording.
bdndx=[]; %initializing list of bad random times
for ind=1:length(ntimes) %for each random time
	tmpvect=min(abs(sp1-ntimes(ind))); %time to nearest spike event
	if tmpvect<round(spikes1.params.window_size*spikes1.params.Fs/1e3)*.5, %if too close to nearest spike
		bdndx(end+1)=ind;
	end
end
ntimes(bdndx)=[];
subs_n=getwinz(data_m',ntimes,round(spikes1.params.window_size*spikes1.params.Fs/1e3));
spikes1.info.detect.cov=cov(detrend(subs_n)); %noise covariance
t1=toc;
disp(['Noise event extraction completed in ' num2str(t1) ' seconds.'])
clear subs_n ntimes bdndx tmpvect jnk

%Extract spike events
disp('Extracting events ...')
tic
%subs=getwinz(data_m',sp1-(tback*fs/1e3)-1,spikes1.params.window_size*fs/1e3);
subs=getwinz(data_m',sp1-round((spikes1.params.cross_time*spikes1.params.Fs/1e3))-(round(spikes1.params.max_jitter*spikes1.params.Fs)/1e3+1),...
	round(spikes1.params.window_size*spikes1.params.Fs/1e3)+2*round(spikes1.params.max_jitter*spikes1.params.Fs)/1e3);
subs1=reshape(subs,size(subs,1), round(spikes1.params.window_size*spikes1.params.Fs/1e3)+2*round(spikes1.params.max_jitter*spikes1.params.Fs)/1e3,...
	length(spikes1.info.spkchan)); clear subs %break into component channels
[jnk,event_channel]=max(squeeze(abs(max(subs1,[],2))),[],2); %channel index for biggest signal for every spike waveform
t1=toc;
disp(['Event extraction completed in ' num2str(t1) ' seconds.'])

%Dejittering
disp('Aligning samples ...')
tic
rsmp=spikes.params.rsmp;
subs2=zeros(size(subs1,1),round(size(subs1,2)*rsmp),length(spikes1.info.spkchan));
for ind=1:length(spikes1.info.spkchan), subs2(:,:,ind)=resample(subs1(:,:,ind)', rsmp, 1)';, end 
subs3=zeros(size(subs2,1),size(subs2,2));
for ind=1:size(subs2,1), subs3(ind,:)=squeeze(subs2(ind,:,event_channel(ind)));, end
[mean2,sndx]=realign2_wrapper_ext(subs3,round(round(spikes1.params.max_jitter*spikes1.params.Fs)/1e3*rsmp),round((spikes1.params.window_size*spikes1.params.Fs/1e3)*rsmp),...
	round((spikes1.params.window_size*spikes1.params.Fs/1e3)*rsmp),round((round(spikes1.params.max_jitter*spikes1.params.Fs)/1e3-6)*rsmp),spikes1.params.jitterreps,...
	round(spikes1.params.jitterpen*rsmp),spikes.params.djlab);  %max dot product dejittering
%clear subs1

[subs_dj2]=extract_dj_waves(subs2,sndx,round(spikes1.params.max_jitter*spikes1.params.Fs)/1e3,rsmp);
sp2=sp1;
sp1=sp1+round(sndx/rsmp);
subs_dj=resample(reshape(subs_dj2,size(subs_dj2,1),size(subs_dj2,2)*size(subs_dj2,3)  )', 1, rsmp)'; clear subs_dj2
t1=toc;
disp(['Alignment completed in ' num2str(t1) ' seconds.'])

%Build clean model
disp('Building ''clean'' models ...')
tic
chanleng=round(spikes1.params.window_size*spikes1.params.Fs/1e3);
clust_meds=zeros(spikes1.params.kmeans_clustersize,chanleng,length(spikes1.info.spkchan)); %initializing
for ind=1:spikes1.params.kmeans_clustersize %building per channel median for each event
	clust_meds1(ind,:)=median(spikes.waveforms_clust{ind},1);
	clust_meds2(ind,:,:)=reshape(median(spikes.waveforms_clust{ind},1),chanleng,length(spikes1.info.spkchan));
end %building per channel median for each event
t1=toc;
disp(['Built ''clean'' models in ' num2str(t1) ' seconds.'])

%Set up Mahalanobis parameters
disp('Clustering ''clean'' events ...')
tic
num_dims=size(subs_dj,2);
num_spikes=size(subs_dj,1);
[v,d] = eig(spikes.info.detect.cov); % get PCs of noise covariance
p_var=(cumsum(fliplr(diag(d)'))/sum(fliplr(diag(d)'))); %porportion of variance explained by cumulative set of eigenvectors
[jnk,r]=min(abs(p_var-.9)); %use enough pcs to explain 90% of variance
%r=round(num_dims/8); %rank of pcs to use
last =[1:r]+num_dims-r;
v = v(:,last); % use last r dimensions
zs = zeros([1 num_spikes]);
dinv = inv(d(last,last));

%Classify all events according to mahalanobis distance
zs=zeros(1,num_spikes); idx1=zs; %initialize
for ind1=1:num_spikes %for each spike event
	wavtmp=repmat(subs_dj(ind1,:),spikes1.params.kmeans_clustersize,1);
%	ztmp=(sum((wavtmp-clust_meds1).^2,2)/mean(diag(spikes.info.detect.cov))); %assume spherical covariance
	res1=wavtmp-clust_meds1;
	res1=res1*v; %subtract mean and project into pca space
%	res1=detrend(res1, 'constant')*v; %subtract mean and project into pca space
	for ind2=1:spikes1.params.kmeans_clustersize
		ztmp(ind2)=res1(ind2,:)*dinv*res1(ind2,:)';
	end
	[zs(ind1),idx1(ind1)]=min(ztmp);
end

if ~isfield(opts, 'xval')||isempty(opts.xval) %if xval parameter not given
	xval=chi2inv(.99,r); %finding threshold value for rejecting events as superpositions
	
	%Making sure potential # of superposition events is close to expected value
	status_out=1; %status of output
	while status_out==1 
	
		%Expected value of superpositions (assumes uncorrelated poisson spiking)
		for ind1=1:spikes1.params.kmeans_clustersize %for each cluster
			M=length(sp1)-length(find(idx1(find(zs<=xval))==ind1)); %Number of spikes not in cluster
			rt(ind1)=(spikes1.params.window_size/1000)*M/(sum(dur1(:))/spikes1.params.Fs); %Expected rate of missing spikes for cluster
			fspikes=length(find(idx1(find(zs<=xval))==ind1)); %number of spikes found in cluster
			espikes=round(fspikes/(1-rt(ind1))); %expected number of total spikes in cluster, given found spikes
			emspikes(ind1)=espikes-fspikes; %expected number of missing spikes
		end %for each cluster
		
		%Compare expected superpositions to projected superpositions
		f1=figure;
		[hval,xs1]=hist(zs, 4*sqrt(length(zs)));
		bar(xs1,hval,1, 'FaceColor', [.7 .7 .7])
		hold on
		plot(xs1, chi2pdf(xs1,r)*num_spikes*diff(xs1(1:2)), 'c')
		yls=get(gca, 'YLim');
		plot([xval xval], yls, 'r--')
		legend('Data', 'Chi2-distributed distances', 'Proposed thresh', 'Location', 'Best')
		title(['N=' num2str(length(zs)) ' events'])
		xlabel('Malhalanobis distance from noise')
		figure(f1)
		disp(['Expected superposition events: ' num2str(sum(emspikes)/2)])
		disp(['Found superpositions: ' num2str(length(find(zs>xval)))])
		disp('Type 0 for continuing with these values')
		disp('Type 1 for choosing different threshold value for superpositions')
		opt=input('Option= ');
		if (opt==0) %use current values
			status_out=0; %break out of loop
			close(f1)		
		elseif (opt==1) %select new value
			[hval,xs1]=hist(zs, 4*sqrt(length(zs)));
			figure(f1)
			clf
			bar(xs1,hval,1, 'FaceColor', [.7 .7 .7])
			hold on
			plot(xs1, chi2pdf(xs1,r)*num_spikes*diff(xs1(1:2)), 'c')
			legend('Data', 'Chi2-distributed distances', 'Location', 'Best')
			title(['N=' num2str(length(zs)) ' events'])
			xlabel('Malhalanobis distance from noise')
			disp('Select horizontal limit (to the right will be outliers)')
			pause(1), figure(gcf)
			[xval,jnk]=ginput(1);
		else disp('Sorry, invalid option; try again.');		
		end %use current values
	end %while status_out==1
	
	disp(['xval = ' num2str(xval)])
elseif strmatch(opts.xval, 'auto') %if using 99% threshold
	xval=chi2inv(.99,r); %finding threshold value for rejecting events as superpositions
	disp(['xval = ' num2str(xval)])
else %if xval parameter IS given
	xval=opts.xval;
end %if xval parameter not given
spikes1.xval=xval; %returning value

%If #'s OK, begin to label outputs
sup_spks=find(zs>xval); %index of superposition spikes
sndx_good=find(zs<=xval); %index of clean spikes
idx1(sup_spks)=[]; %remove outlier times
spikes1.zs=zs(sndx_good);
t1=toc;
disp(['Clean spikes clustered in ' num2str(t1) ' seconds (' num2str(length(sup_spks)) '/' num2str(length(zs)) ' events rejected as superpositions).'])

%Build Superposition super-model
%buffpct=0.1; %percent of spike window to be buffered (superpositions following in 1/2 of this on each side of window will be ignored)
%buffpts=floor(buffpct*chanleng/2);
%disp('Building ''supermodel'' ...')
%tic
%supmod=zeros(spikes1.params.kmeans_clustersize,spikes1.params.kmeans_clustersize,2*chanleng-1,chanleng*length(spikes1.info.spkchan)); %initializing 4-D array for superposition model
%for ind1=1:spikes1.params.kmeans_clustersize %for central event
%	for ind2=1:spikes1.params.kmeans_clustersize %for superposed events
%		wav1=zeros(2*chanleng-1,chanleng,length(spikes1.info.spkchan));
%		if ind1~=ind2 %if both clusters not the same
%			for ind=1:chanleng %building channel-wise
%				ndx{ind}=(chanleng-ind+1):chanleng;
%				ndx2{ind}=1:ind;
%			end
%			for ind=(chanleng+1):(2*chanleng-1)
%				ndx{ind}=(1):2*chanleng-ind;
%				ndx2{ind}=ind-(chanleng-1):chanleng;
%			end
%			for ind=1:length(ndx)
%				wav1(ind,:,:)=clust_meds2(ind1,:,:);
%				wav1(ind,ndx2{ind},:)=wav1(ind,ndx2{ind},:)+clust_meds2(ind2,ndx{ind},:);
%			end
%		else %if ind1 DOES equal ind2
%			wav1=repmat(clust_meds2(ind1,:,:),[2*chanleng-1 1 1]); %All diagonal events are equivalent- not superposition models, but 'clean' models
%		end
%		supmod(ind1,ind2,:,:)=reshape(wav1,2*chanleng-1,chanleng*length(spikes1.info.spkchan));
%	end
%end
%supmod(:,:,[1:buffpts end-buffpts+1:end],:)=[]; %get rid of buffer windows at both ends of data set
%supmod2=reshape(supmod, spikes1.params.kmeans_clustersize*spikes1.params.kmeans_clustersize*(2*chanleng-1-2*buffpts),chanleng*length(spikes1.info.spkchan)); clear supmod
disp('Building ''supermodel'' ...')
tic
buffpct=0.1; %percent of spike window to be buffered (superpositions following in 1/2 of this on each side of window will be ignored)
buffpts=floor(buffpct*chanleng/2);
supmod=zeros(spikes1.params.kmeans_clustersize,spikes1.params.kmeans_clustersize,2*chanleng-1-2*buffpts,chanleng*length(spikes1.info.spkchan)); %initializing 4-D array for superposition model
for ind1=1:spikes1.params.kmeans_clustersize %for central event
	for ind2=1:spikes1.params.kmeans_clustersize %for superposed events
		wav1=zeros(2*chanleng-1-2*buffpts,chanleng,length(spikes1.info.spkchan));
		if ind1~=ind2 %if both clusters not the same
			for ind=1:chanleng-buffpts %building channel-wise
				ndx{ind}=(chanleng-ind+1-buffpts):chanleng;
				ndx2{ind}=1:ind+buffpts;
			end
			for ind=(chanleng+1-buffpts):(2*chanleng-1-2*buffpts)
				ndx{ind}=(1):2*chanleng-ind-buffpts;
				ndx2{ind}=ind-(chanleng-1-buffpts):chanleng;
			end
			for ind=1:length(ndx)
				wav1(ind,:,:)=clust_meds2(ind1,:,:);
				wav1(ind,ndx2{ind},:)=wav1(ind,ndx2{ind},:)+clust_meds2(ind2,ndx{ind},:);
			end
		else %if ind1 DOES equal ind2
			wav1=repmat(clust_meds2(ind1,:,:),[2*chanleng-1-2*buffpts 1 1]); %All diagonal events are equivalent- not superposition models, but 'clean' models
		end
		supmod(ind1,ind2,:,:)=reshape(wav1,2*chanleng-1-2*buffpts,chanleng*length(spikes1.info.spkchan));
	end
end
supmod2=reshape(supmod, spikes1.params.kmeans_clustersize*spikes1.params.kmeans_clustersize*(2*chanleng-1-2*buffpts),chanleng*length(spikes1.info.spkchan)); clear supmod
t1=toc;
disp(['Built ''supermodel'' in ' num2str(t1) ' seconds.'])

%Assigning class of superposition events from min(sum(square(data-model)))
disp('Testing superposition models ...')
tic
indices=zeros(length(sup_spks),3); %initialize variable
zs3=zeros(length(sup_spks),1); %initialize variable
for ind1=1:length(sup_spks) %for each outlier event
	[zs3(ind1),indices(ind1,:),pos1(ind1)]=pickfromsupermod(subs_dj,supmod2,sup_spks,ind1,length(spikes1.info.spkchan),chanleng,spikes1.params.kmeans_clustersize,v,dinv,r);
end

%%%%%%%%%%%%%Account for two spikes from same model
bdndx3=find(indices(:,1)==indices(:,2)); %first and second class in model are the same
if ~isempty(bdndx3) %Account for two spikes from same model
	idx1(end+1:end+length(bdndx3))=indices(bdndx3,1);
	spikes1.zs(end+1:end+length(bdndx3))=zs3(bdndx3);	
	sndx_good(end+1:end+length(bdndx3))=sup_spks(bdndx3);
	sup_spks(bdndx3)=[];
	indices(bdndx3,:)=[];
	zs3(bdndx3)=[];
	disp([num2str(length(bdndx3)) ' of outliers actually best fit by ''clean'' model.'])
end %Account for two spikes from same model

wav2=zeros(length(sup_spks),size(spikes.waveforms,2)*size(spikes.waveforms,3));
for ind=1:length(sup_spks), 
	wav2(ind,:)=subtract_sup1(subs_dj(sup_spks(ind),:),indices(ind,:),clust_meds2,ndx,ndx2);, 
end %get 1st spike of superpos. waveforms

wav3=zeros(length(sup_spks),size(spikes.waveforms,2)*size(spikes.waveforms,3));
if length(sup_spks)>0
	subs=getwinz(data_m',sp1(sup_spks)-round((spikes1.params.cross_time*spikes1.params.Fs/1e3))...
		-round((spikes1.params.window_size*spikes1.params.Fs/1e3-indices(:,3))'),round(spikes1.params.window_size*spikes1.params.Fs/1e3)); %%%%%%%%%%%%%%%%%

	for ind=1:length(sup_spks), wav3(ind,:)=subtract_sup2(subs(ind,:),indices(ind,:),clust_meds2,ndx,ndx2);, end %get 2nd spike of superpos. waveforms
end
t1=toc;
disp(['Clustered ' num2str(2*length(sup_spks)) ' superimposed spikes in ' num2str(t1) ' seconds.'])


%ID all superposition events, or throw out some over threshold criteria?


%Bundle output to run quality metrics
spikes1.assigns=idx1'; %indices of labels for cluster ID for each waveform
spikes1.waveforms=reshape(subs_dj(sndx_good,:),length(sndx_good),size(spikes.waveforms,2), size(spikes.waveforms,3));
spikes1.waveforms(end+1:end+length(sup_spks),:,:)=reshape(wav2,size(wav2,1),size(spikes.waveforms,2), size(spikes.waveforms,3));
spikes1.waveforms(end+1:end+length(sup_spks),:,:)=reshape(wav3,size(wav3,1),size(spikes.waveforms,2), size(spikes.waveforms,3));
spikes1.info.detect.event_channel=event_channel(sndx_good);
spikes1.info.detect.event_channel(end+1:end+length(sup_spks))=event_channel(sup_spks);
spikes1.info.detect.event_channel(end+1:end+length(sup_spks))=event_channel(sup_spks);

spikes1.assigns(end+1:end+length(sup_spks))=indices(:,1); %1st spike of superpositions
spikes1.assigns(end+1:end+length(sup_spks))=indices(:,2); %2nd spike of superpositions
spikes1.zs(end+1:end+length(sup_spks))=zs3; %1st spike of superpositions
spikes1.zs(end+1:end+length(sup_spks))=zs3; %2nd spike of superpositions
spikes1.labels=[1:spikes1.params.kmeans_clustersize; ones(1,spikes1.params.kmeans_clustersize)]'; %Need to look at this to figure out- Cx2 (C=# of clusters)
spikes1.unwrapped_times=sp1(sndx_good)/spikes1.params.Fs; %Not trial dependent
supspike_ndx1=length(spikes1.unwrapped_times)+1:length(spikes1.unwrapped_times)+length(sup_spks);
spikes1.unwrapped_times(end+1:end+length(sup_spks))=sp1(sup_spks)/spikes1.params.Fs; %1st spike of superpositions
supspike_ndx2=length(spikes1.unwrapped_times)+1:length(spikes1.unwrapped_times)+length(sup_spks);
spikes1.unwrapped_times(end+1:end+length(sup_spks))=sp1(sup_spks)/spikes1.params.Fs-(spikes1.params.window_size*spikes1.params.Fs/1e3-indices(:,3))'/spikes1.params.Fs; %2nd spike of superpositions
[spikes1.unwrapped_times, ndxz]=sort(spikes1.unwrapped_times); %sort into ascending order
for ind=1:length(supspike_ndx1), [jn,spikes1.firstsupndx(ind)]=find(ndxz==supspike_ndx1(ind));, [jn,spikes1.secndsupndx(ind)]=find(ndxz==supspike_ndx2(ind));, end
if ~isfield(spikes1,'firstsupndx') % if it doesn't exist
	spikes1.firstsupndx=zeros(1,0);
	spikes1.secndsupndx=zeros(1,0);
end %if it doesn't exist
spikes1.assigns=spikes1.assigns(ndxz); %matching order of spiketimes
spikes1.zs=spikes1.zs(ndxz); %matching order of spiketimes
spikes1.info.detect.event_channel=spikes1.info.detect.event_channel(ndxz); %matching order of spiketimes
spikes1.waveforms=spikes1.waveforms(ndxz,:,:);
spikes1.waveforms_pca=spikes1.waveforms(:,:)*spikes.info.pca.v(:,1:spikes1.info.pca.ndim);


%ID single largest event channel for entire cluster
if length(spikes.info.spkchan)>1
	for ind=1:spikes1.params.kmeans_clustersize %for each cluster
		subs1=spikes1.waveforms(find(spikes1.assigns==ind),:);
		subs1=reshape(subs1,size(subs1,1),size(subs1,2)/length(spikes1.info.detect.thresh), length(spikes1.info.detect.thresh));
		subs2=squeeze(mean(subs1,1));
		[j1,chan_nd]=max(squeeze(max(spikes1.info.mf*subs2,[],1))./spikes1.info.detect.thresh,[],2);
		spikes1.info.detect.event_channel(find(spikes1.assigns==ind))=chan_nd;
	end
	clear subs1 subs2 chan_nd
else
	spikes.info.detect.event_channel=ones(size(spikes.assigns));
end


disp('Final dejittering per class ...')
tic
for ind=1:spikes1.params.kmeans_clustersize
	spikes1=dejitterclusts(spikes1,ind); %dejitter per cluster
	if isfield(spikes1, 'waveforms_clust') %if field exists
		if ~isempty(find(spikes1.assigns==ind)) %if cluster isn't empty
			spikes1.waveforms_clust_pca{ind}=spikes1.waveforms_clust{ind}*spikes1.info.pca.v(:,1:spikes1.info.pca.ndim);
		end %if cluster isn't empty
	end %if field exists
end
t1=toc;
disp(['Dejittering completed in ' num2str(t1) ' seconds.'])

spikes1.spiketimes=spikes1.unwrapped_times; %initialize
spikes1.trials=ones(size(spikes1.unwrapped_times)); %initialize
spikes1.files=ones(size(spikes1.unwrapped_times)); %initialize

ut = spikes1.unwrapped_times;
f_start = 0; % time that represents the start of the file (updated in each loop)
t_start = 0; % time that represents the start of the trial (updated in each loop)
for f_ind=1:nfiles %for each file analyzed	
	for t_ind=1:length(spikes1.info.trial_ndx) % for each trial 		
		t_end = t_start+dur1(f_ind,t_ind)/opts.fs;
		spikes1.spiketimes(ut >= t_start & ut < t_end) = spikes1.spiketimes(ut >= t_start & ut < t_end) - t_start;
		spikes1.trials(ut >= t_start & ut < t_end) = t_ind * ones(size(spikes1.spiketimes(ut >= t_start & ut < t_end)));
		t_start = t_end; % for next round
	end 	
	f_end = t_end;
	spikes1.files(ut >= f_start & ut < f_end) = f_ind * ones(size(spikes1.spiketimes(ut >= f_start & ut < f_end)));
	f_start = f_end; % for next round
end	
spikes1.dur=dur1;
spikes1.info.detect.dur=dur1/opts.fs; 

refractviol=0;
exclper=round(0.15*opts.fs/1e3); %set to .15 ms- 3 sample points at 20kHz
totspikes=length(spikes1.assigns);
%Last, remove double-counts of spikes
for ind=1:spikes1.params.kmeans_clustersize %look for double counts within each cluster
	clustndx=find(spikes1.assigns==ind); %spikes within this cluster
	isis1=diff(spikes1.unwrapped_times(clustndx)*spikes1.params.Fs); %ISIs in sample points
	zerosndx=find(isis1<=exclper)+1; %all isis with<=3 samples between spikes (actually second spike within isi)
	refractviol=refractviol+length(zerosndx);
	spikes1.assigns(clustndx(zerosndx))=[]; %removing offending spikes
	spikes1.waveforms(clustndx(zerosndx),:,:)=[]; %removing offending spikes
	spikes1.waveforms_pca(clustndx(zerosndx),:)=[]; %removing offending spikes
	spikes1.waveforms_clust{ind}(zerosndx,:)=[]; %removing offending spikes
	spikes1.waveforms_clust_pca{ind}(zerosndx,:)=[]; %removing offending spikes
	spikes1.spiketimes(clustndx(zerosndx))=[]; %removing offending spikes
	tmpspikes=spikes1.unwrapped_times; %temporary reference back to old indices
	spikes1.unwrapped_times(clustndx(zerosndx))=[]; %removing offending spikes
	spikes1.trials(clustndx(zerosndx))=[]; %removing offending spikes
	spikes1.files(clustndx(zerosndx))=[]; %removing offending spikes
	spikes1.zs(clustndx(zerosndx))=[]; %removing offending spikes
	spikes1.info.detect.event_channel(clustndx(zerosndx))=[]; %removing offending spikes
    if isfield(spikes1,'firstsupndx') % if it exists
        [jnk,rmvndx1]=intersect(spikes1.firstsupndx, clustndx(zerosndx)); %first spike of superposition indices belonging to cluster to remove
        [jnk,rmvndx2]=intersect(spikes1.secndsupndx, clustndx(zerosndx)); %second spike of superpoition indices belonging to cluster to remove
	    spikes1.firstsupndx(rmvndx1)=[]; %get rid of these indices
		spikes1.secndsupndx(rmvndx2)=[]; %get rid of these indices
	 	[jnk,keepndx]=intersect(tmpspikes, spikes1.unwrapped_times); %mapping the kept spikes back into the original indices
		[jnk,spikes1.firstsupndx]=intersect(keepndx,spikes1.firstsupndx); %mapping old sup. indices to new values
		[jnk,spikes1.secndsupndx]=intersect(keepndx,spikes1.secndsupndx); %mapping old sup. indices to new values
	else
		spikes1.firstsupndx=zeros(1,0);
		spikes1.secndsupndx=zeros(1,0);
    end %if it exists
end %look for double counts within each cluster
disp(['Removed ' num2str(refractviol) ' of ' num2str(totspikes) ' spikes due to double occurrence within ' num2str(exclper) ' samples.'])

% Flat lines removal
tr=[]; %initialize bad waveform index
for currSpike=1:size(spikes1.zs,2)
	concatSpike1=opts.mf * squeeze(spikes1.waveforms(currSpike,:,spikes1.info.detect.event_channel(currSpike))); %select only event channel for current spike
	if max(concatSpike1)<spikes1.info.detect.thresh(spikes1.info.detect.event_channel(currSpike)) %if spike peak not greater than threshold
		tr(end+1)=currSpike; %increase index of bad waveforms
	end
end
disp(['Errors fixed by flat-line removal: ' num2str(length(tr))]);
%index for removing waveforms objects
spikes1.zs(tr)=[];
spikes1.waveforms(tr,:,:)=[];
spikes1.unwrapped_times(tr)=[];
spikes1.waveforms_pca(tr,:)=[];
spikes1.spiketimes(tr)=[];
spikes1.trials(tr)=[] ;
spikes1.files(tr)=[];
spikes1.info.detect.event_channel(tr)=[]; 
[jnk,bad_firstsupndx]=intersect(spikes1.firstsupndx,tr);
[jnk,bad_secndsupndx]=intersect(spikes1.secndsupndx,tr);
spikes1.firstsupndx(bad_firstsupndx)=[];
spikes1.secndsupndx(bad_secndsupndx)=[];
for clust=1:size(spikes1.waveforms_clust,2)
	[jnk,bad_clustndx]=intersect(find(spikes1.assigns==clust),tr);
    spikes1.waveforms_clust{clust}(bad_clustndx,:)=[];
    spikes1.waveforms_clust_pca{clust}(bad_clustndx,:)=[];
end
spikes1.assigns(tr)=[];
%
% count = 0; 
% tr=zeros(size(spikes1.zs));
% for currSpike=1:size(spikes1.zs,2)
%     concatSpike1=squeeze(spikes1.waveforms(currSpike,:,:));
%     concatSpike=reshape(concatSpike1, 1, size(concatSpike1,1)*size(concatSpike1,2));    
%     if max(opts.mf * concatSpike) <= 0.95*min(spikes1.params.thresh)  
%         tr(currSpike)=1;        count=count+1;  
%     end
% end
% disp(['Errors fixed by flat-line removal: ' num2str(count)]);
% %index for removing waveforms objects
% spikes1.zs(tr==1)=[];
% spikes1.assigns(tr==1)=[];
% spikes1.waveforms(tr==1,:,:)=[];
% spikes1.unwrapped_times(tr==1)=[];
% spikes1.waveforms_pca(tr==1,:)=[];
% spikes1.spiketimes(tr==1)=[];
% spikes1.trials(tr==1)=[] ;
% spikes1.files(tr==1)=[];
% spikes1.info.detect.event_channel(tr==1)=[]; 
% sup1=zeros(1,currSpike);
% sup2=zeros(1,currSpike);
% sup1(spikes1.firstsupndx)=1;
% sup2(spikes1.secndsupndx)=1;
% sup1(tr==1)=0;
% sup2(tr==1)=0;
% spikes1.firstsupndx=find(sup1==1);
% spikes1.secndsupndx=find(sup2==1);
% for clust=1:size(spikes1.waveforms_clust,2)
%     currWaveforms1=spikes1.waveforms(spikes1.assigns==clust,:,:);
%     spikes1.waveforms_clust{clust}=reshape(currWaveforms1,size(currWaveforms1,1),size(currWaveforms1,2)*size(currWaveforms1,3));
%     spikes1.waveforms_clust_pca{clust}=spikes1.waveforms_pca(spikes1.assigns==clust,:);
% end

end
