function spikes2=adjustmodel(spikes,spikes1,p)

% spikes2=adjustmodel(spikes,spikes1,p)
%
% Function to modify a spikes model structure, by adjusting the cluster means
% by a specified proportion with new data.  Use of this function should allow
% models to change over time in order to follow e.g. unit drift.
% Inputs:
% spikes- original spikes model structure to be modified
% spikes1- output spikes structure of subsequent classification step.  A proportion
%	of spikes from this structure will be used to modify the original model. (Note,
%	this should be composed of 'clean' spikes in the same way that the original model
%	was cleaned).
% p- proportion of waveforms from 'spikes' structure to be kept (the proportion 1-p
%	will then come from the 'spikes1' structure;  default p=0.2)
% Outputs:
% spikes2- new spikes model structure with modified means (note that the other fields
%	within the spikes2 structure may no longer be valid, such as spikes2.unwrapped_times.
%	Also note that the parameters for detecting and filtering spikes will not change.  The
%	PCA dimension, however, will change.)
%
% July 14, 2012, by ZNA

%meaningless change

if nargin<3||isempty(p), p=0.2; end %default proportion

spikes2=spikes; %copying over all the appropriate fields, formats, and detection values
sz=size(spikes2.waveforms_clust{1});
wavs=zeros(0,sz(2));
spikes2.assigns=[];
for ind=1:spikes2.params.kmeans_clustersize %building per cluster set of waveforms
	nspikes_old=length(find(spikes.assigns==ind)); %number of spikes in cluster in original model
	ord_old=randperm(nspikes_old); %random re-ordering
	nspikes_new=length(find(spikes1.assigns==ind)); %number of spikes in cluster in original model
	ord_new=randperm(nspikes_new); %random re-ordering
	newmodspikes=min([nspikes_old nspikes_new]); %use minimum so no need for over-representing
	n_origspikes=round(p*newmodspikes); %number of spikes to draw from original model
	n_newspikes=newmodspikes-n_origspikes; %number of spikes to draw from new data	
	spikes2.waveforms_clust{ind}=spikes.waveforms_clust{ind}(ord_old(1:n_origspikes),:); %waveforms from old data
	spikes2.waveforms_clust{ind}(n_origspikes+1:newmodspikes,:)=spikes1.waveforms_clust{ind}(ord_new(1:n_newspikes),:); %waveforms from new data
	wavs(end+1:end+newmodspikes,:)=spikes2.waveforms_clust{ind};
	spikes2.assigns(end+1:end+newmodspikes,1)=repmat(ind,newmodspikes,1); %cluster assignments
end %building per cluster set of waveforms

%Adjust noise model
spikes2.info.detect.cov=spikes1.info.detect.cov*(1-p)+spikes.info.detect.cov*p;

%Make new PCA space (will still have same number of dimensions
spikes2.waveforms=reshape(wavs,size(wavs,1), round(size(wavs,2)/length(spikes.info.spkchan)), length(spikes.info.spkchan));
[spikes2.info.pca.v,spikes2.info.pca.s]=eig(cov(wavs)); %eigenvalues & principal component vectors
spikes2.info.pca.v=fliplr(spikes2.info.pca.v); %inverting order so that biggest pca dimension comes first
spikes2.info.pca.s=fliplr(flipud(spikes2.info.pca.s)); %inverting order so that biggest pca dimension comes first
spikes2.waveforms_pca=wavs*spikes2.info.pca.v(:,1:spikes2.info.pca.ndim); %aligned spike waveforms projected into pca space

%Project per cluster waveforms into PCA space
for ind=1:spikes2.params.kmeans_clustersize %building per cluster set of projected waveforms
	spikes2.waveforms_clust_pca{ind}=spikes2.waveforms_clust{ind}*spikes.info.pca.v(:,1:spikes2.info.pca.ndim);
end %building per cluster set of projected waveforms

%Clean up to get rid of potentially misleading fields
if isfield(spikes2, 'outliers') %if these fields exist
	spikes2=rmfield(spikes2, 'outliers'); %structure containing outlier information
	spikes2=rmfield(spikes2, 'unwrapped_times'); %time stamp, trial-less, for each spike
	spikes2=rmfield(spikes2, 'spiketimes'); %time stamp, per trial, for each spike
	spikes2=rmfield(spikes2, 'trials'); %index of which trial each spike came from
	spikes2=rmfield(spikes2, 'files'); %index of which files each spike came from
%	spikes2=rmfield(spikes2, 'opts'); %options to build original model (includes filenames, etc. inappropriate)	
end %if these fields exist	
	