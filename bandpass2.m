function filt_signal=bandpass2(signal, lfro, hfro, fs, ord, opt)

%  filt_signal=bandpass2(signal, lfro, hfro, fs, ord, opt);
%  Function for band passing a signal
%  Input variables:
%  lfro= low-frequency-roll-off- absolute minimum permissable value of 1 (in Hz)
%  hfro= high-frequency-roll-off (in Hz)
%  fs= sampling frequency of signal (in Hz)
%  ord- order of band pass filter (see fir2- default=1e3)
%  opt= if opt==1, then a figure of the power spectra is displayed (takes a long time,
%   default opt=0)
%
% extra comment

if nargin<5||isempty(ord), ord=1e3; end %default filter order
if nargin<6||isempty(opt), opt=0; end %default- no figure

%What is the maximum in the power spectra?
maxspect=1;

%define frequency vector in normalized units (1=fs/2)
lf=lfro/(fs/2); hf=hfro/(fs/2);
freq=[0 lf-.01/(fs/2) lf hf hf+.01/(fs/2) 1];
%freq=[0 lf lf hf hf 1];
filtervector=[0 0 maxspect maxspect 0 0];

%building the filter
[b,a]=fir2(ord, freq, filtervector);

%filtering the original signal
tempsignal=zeros(length(signal)+4*length(b),1);
tempsignal(2*length(b)+1:length(signal)+2*length(b))=signal;  %Buffering
temp_filt_signal=filter(b,a,tempsignal);
filt_signal=temp_filt_signal(round(2.5*length(b)):round(2.5*length(b))+length(signal)-1);
%%%%%%%% May have to buffer, etc here %%%%%%%%%%

if opt==1;
    figure
	subplot(2,1,1)
	[pxx,freq]=mtpsd(signal,5,9,pow2(nextpow2(fs)),fs,fs,1/2*fs);
	titlestring='Power Spectra of Original Signal, MT method';
	psdplot(pxx,freq,'Hz','db',titlestring);
	%plot(freq,pxx)
	subplot(2,1,2)
	[pxx,freq]=mtpsd(filt_signal,5,9,pow2(nextpow2(fs)),fs,fs,1/2*fs);
	titlestring='Power Spectra of Filtered Signal, MT method';
	psdplot(pxx,freq,'Hz','db',titlestring);
	%plot(freq,pxx)
end