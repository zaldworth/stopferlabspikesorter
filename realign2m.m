function [mstim2,sndx,newstim]=realign2m(stim2,mstim2,anchor,range,s,zero_shift)
% function [newmstim,sndx,newstim]=realign2m(stim,mstim,anchor, range[,s,zero_shift])
% INPUTS:
% stim: nsamp x dim x (1 or 2) set of stimuli. for now pass 1 channel only!
% mstim: stimulus model on which the stimulus is aligned.
% anchor: time in stim2 where mstim2 ends
% range: 1 x (1 or 2) allowed range for shifts relative to the anchor. matches 
% considered only in those ranges.
% s = guess for the jitter variance. Penalizes larger excursions. 
% suggest hard range > about 2 soft range.
% zero_shift = 0 : don't align the median to 0; 
%            = 1 : align the median shift to 0.
% OUTPUTS:
% newstim: nsamp x dim x 1 or 2 set of aligned stimuli
% newmstim: new model
% sndx: set of shifts to produce the new mean. shifts relative to anchor.
%
% to produce an initial model, define the model type, e.g.
%    mstim2 = gmm(dim_mean*num_chan,1,'spherical');
% and then fit to an approriate portion of
%    opts = [];    % since matlab 7 cannot do opts=foptions;
%    mstim2=gmminit(mstim2,reshape(stim(:,(anchor-dim_mean+1):anchor,:),size(stim,1),mstim2.nin),opts); 
% dim_mean is an extra parameter, making the size of the mean to which we align explicit.
% opts irrelevant for this case - mean and covariance estimated directly for 1-component model.



if nargin<5
    s=Inf; % revert back to hard limits; no penalty. 
	       % hmm, may not work here because of the model	
end
if nargin<6
    zero_shift=0;
end

if numel(range)==1
    % this redefine a matlab command : range!
    range=abs(range);
    range = [-range range];
    % one element makes a symmetric range. two are preserved. need to issue
    % an error message for larger ranges.
else
    range=sort(range);
    % get them in order. Still not checking if there are only 2 in range.
end

[r,c,ch]=size(stim2);
%if ch>1, error('single channel for now'), end
% single channel for now...

shiftmodel = gmm(1,1,'spherical');
shiftmodel.centres=0; shiftmodel.covars=s;
shiftpenalty = loggmmactiv(shiftmodel,(1:c)'-anchor)';
%shiftpenalty = -.5*(((1:c)-anchor)./s ).^2;
% penalty for shifting too far - the square of the shift. Need a proper normalization 
% at the filtering stage. Now can control it with s - large s is less
% penalty.

matches=zeros(r,c);
%for i1=1:size(stim2,3) 
% work with one channel only. anyway, with a gmm as
% distance, 2 channels are treated differnetly - have to be concatenated
for s1=1:r
    matches(s1,:)=gmmfilt2(mstim2,stim2(s1,:,:),@loggmmactiv);%+shiftpenalty;
end % s1
% hmm, since I am looping anyway, I can put the penalty AND search here as
% well, and only return the match index...
% ??? shouldn't this be -loggmmactiv???. No, below I'm doing max, which is
% equivalent to min distance.
matches = matches + repmat(shiftpenalty,size(matches,1),1);
[jnk,sndx]=max(matches(:,(range(1):range(2))+anchor),[],2);
% min distance; min -loglikelihood so ! MAX loglikelihood
% ?? how did this work in realign2? it was running max along the columns!
% ok, I had to transpose the matches for filter to work in realign2

sndx=sndx';  % to have the same dimensions as realign2...
if zero_shift
    sndx = sndx - round(median(sndx));
else
    sndx=sndx+range(1)-1;
end

% or should I say, so that there is no net shift,
% if there is no zero net shift, I don't need to retain sndx - it would
% just be re-calculated again next time, and the shift should be smaller.

newstim = stimshift(stim2,mstim2.nin/ch,anchor,round(sndx));

% opts=foptions; % not in R14 anymore; below expression works with [], but
% just 1 center!
opts=[];
mstim2=gmminit(mstim2,reshape(newstim,r,mstim2.nin),opts); 
% opts irrelevant for this case - mean and covariance re-assigned.
% this is also another way to create compatible gmms - use one to fit and
% get another.
% ok, this is how a model should be constructed (as usual).
% define as
% mstim2 = gmm(stimlen*n_chan,1,'diag') % or some other type of covariance
% then it would work here...