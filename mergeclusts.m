function spikes1=mergeclusts(spikes,clustndx1,clustndx2);

% spikes1=mergeclusts(spikes,clustndx1,clustndx2);
%
% Function to merge selected clusters into a single clusters
%
%
rng(0); % initialize random number generator to fixed state

if clustndx2 < clustndx1  %the program seems to cause issues if clustndx2 is smaller
	tmp = clustndx1;
	clustndx1 = clustndx2;
	clustndx2 = tmp;
end

oldndx=setdiff(1:spikes.params.kmeans_clustersize, [clustndx1 clustndx2]); %index of original clusters, minus clusters to be merged

%Do merging
idx1=spikes.assigns; %list of original assigns
idx3=find(idx1==clustndx2); %list of assigns belonging to cluster to merge
idx1(idx3)=clustndx1; %2nd member of merge is assigned clusternumber of 1st member

for ind=clustndx2:spikes.params.kmeans_clustersize %for each cluster after 2nd member of merge
	idx1(idx1==ind)=idx1(idx1==ind)-1; %decrease cluster number by 1
end %for each cluster after 2nd member of merge

subs_good=reshape(spikes.waveforms, size(spikes.waveforms,1), size(spikes.waveforms,2)*size(spikes.waveforms,3)); %aligned spike waveforms

%Sort clusters from largest to smallest
nclusts=spikes.params.kmeans_clustersize-1; %decreasing cluster number by 1
%for ind=1:nclusts, szval(ind)=sum(abs(median(subs_good(idx1==ind,:))));, end %size of each cluster
for ind1=1:nclusts
	qq=median(subs_good(idx1==ind1,:));
	qq2=reshape(qq,size(spikes.waveforms,2),length(spikes.opts.rthresh));
	qq3=max(qq2*spikes.opts.mf);
	szval(ind1)=max(qq3);
end %size of each cluster
clear id_ord
[jnk,id_ord]=sort(szval,'descend'); %sort clusters by size
clst=find(id_ord==clustndx1) %1st new cluster
[jnk,newndx]=setdiff(id_ord, clst); %index of new clusters, minus newly formed merge cluster
idx=idx1;
for ind=1:nclusts
	idx(idx1==id_ord(ind))=ind;
end %apply new label to clusters

%Update Kleinfeld structure
spikes1=spikes;
spikes1.assigns=idx'; %indices of labels for cluster ID for each waveform
spikes1.labels=[1:nclusts; ones(1,nclusts)]'; %Need to look at this to figure out- Cx2 (C=# of clusters)
colormap jet
clrs=colormap;
cndx=round(1:(63/(nclusts-1)):64);
spikes1.info.kmeans.colors=clrs(cndx,:); %color matrix storing color values (1x3) for each cluster
spikes1.params.kmeans_clustersize=nclusts; %number or miniclusters
spikes1.waveforms_clust(id_ord(newndx))=spikes.waveforms_clust(oldndx); %just copy over old cluster waveforms
spikes1.waveforms_clust_pca(id_ord(newndx))=spikes.waveforms_clust_pca(oldndx); %just copy over old cluster waveforms
spikes1.waveforms_clust(nclusts+1)=[];
spikes1.waveforms_clust_pca(nclusts+1)=[];

%ID single largest event channel for entire cluster
if length(spikes.info.spkchan)>1
	subs1=spikes1.waveforms(find(spikes1.assigns==clst),:);
	subs1=reshape(subs1,size(subs1,1),size(subs1,2)/length(spikes1.info.detect.thresh), length(spikes1.info.detect.thresh));
	subs2=squeeze(mean(subs1,1));
	[j1,chan_nd]=max(squeeze(max(spikes1.info.mf*subs2,[],1))./spikes1.info.detect.thresh,[],2);
	spikes1.info.detect.event_channel(find(spikes1.assigns==clst))=chan_nd;
	clear subs1 subs2 chan_nd
else
	spikes1.info.detect.event_channel=ones(size(spikes.assigns));
end


%Align spike waveforms (dejitter) within clusters
for ind=1:nclusts, subs_sort{ind}=subs_good(idx==ind,:);, end
spikes1=dejitterclusts(spikes1,clst);
spikes1.waveforms_clust_pca{clst}=spikes1.waveforms_clust{clst}*spikes.info.pca.v(:,1:spikes.info.pca.ndim);

% code for checking spilt/merge errors
for ind=1:nclusts, nspa(ind)=length(find(spikes1.assigns==ind)); end
for ind=1:nclusts, nspb(ind)=size(spikes1.waveforms_clust{ind},1); end
if ~isequal(nspa,nspb)
	warning('Cluster index error in mergeclusts');
end

