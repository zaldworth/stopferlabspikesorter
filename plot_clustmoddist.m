function plot_clustmoddist(spikes)

% plot_clustmoddist(spikes)
%
% Function to plot per-cluster histogram distances of waveforms from cluster center.


%set up number of columns in figure
if max(spikes.assigns)>1&max(spikes.assigns)<=5, ncols=1;
elseif max(spikes.assigns)>5&max(spikes.assigns)<=10, ncols=2;
elseif max(spikes.assigns)>10&max(spikes.assigns)<=15, ncols=3;
elseif max(spikes.assigns)>15&max(spikes.assigns)<=20, ncols=4;
else error('Too many (or zero) clusters')
end

nrows=ceil(max(spikes.assigns)/ncols);
figure
set(gcf, 'Position', [600 150 575 875])
for ind=1:max(spikes.assigns)
	subplot(nrows,ncols,ind) 
%	plot_distances_z(spikes,ind,2); %plot distances from noise model
	plot_distances_z(spikes,ind,1); %plot distances from cluster centers
	title(['Clust # ' num2str(ind) ', N=' num2str(size(spikes.waveforms_clust{ind},1))], 'Color', spikes.info.kmeans.colors(ind,:))
	set(gca, 'XColor', spikes.info.kmeans.colors(ind,:), 'YColor', spikes.info.kmeans.colors(ind,:))
	axis tight
	if ind>max(spikes.assigns)-ncols, xlabel('Mal. Dist'), else xlabel([]), end
end
