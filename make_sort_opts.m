function opts=make_sort_opts;

% opts=make_sort_opts;
%
% Function which sets up required fields with blank entries for spike sorting input 
% structure 'opts'.
% Outputs:
% opts- structure array with the following fields:
%	prefix- list (character or cell array) of prefix names of files to analyze
%	fs- sampling frequency of files to analyze
%	spkchan- index of channels in file for which to analyze
%	ntrials- total number of trials to analyze (include skipped files in count)
%	bdndx- index of trials to skip
%	clustmeth- method for clustering data, currently either 'kmeans' or 'EM' or 'auto'
%	nclusts- number of classes for clustering- can be single valued or a list of class values, in which 
%		case Bayesian Information Criteria will be used to select best level
%	ndim- number of PCA dimensions to use for projection into low-D space, prior to clustering
%	spwin- two element vector describing the window for spike waveforms.  The first element is the time
%		(in ms) back from peak to start the spike window, and the second element i the time length (in ms)
%		of the spike window 
%	mf- multiplicative factor by which to multiply physiological data prior to finding spike times.  The value
%		should be either -1 or +1, reflecting whether the 'peak' of the spike is negative or positive, respectively
%	rthresh- threshold value for finding spikes in extracellular data (a single value for all channels- spikes only
%		need exceed thresh value on a single channel to be detected)
%	threshv- scale value of mean+threshv*SD above which to reject waveforms as outliers
%	threshv2- scale value for finding range of mean spike waveform+threshv2*SD below which we look for superposition events
%	band_lims- highpass (lower) and lowpass (upper) frequency limits for bandpass filter
% 	furtherMultipliersTo_mf - vector of 1s and -1s, same length as spkchan, telling whether to invert the data values. Default: Leave empty for no flip
%
% Note- fields for 'nclusts', 'ndim', 'spwin', 'rthresh', 'threshv', 'threshv2', and 'mf' can be left empty, in
% which case a user interfaces will be available for entering values during spikes sorting.
%
% June 13, 2012, ZNA



opts = struct('prefix',[],'fs',[],'spkchan',[],'ntrials',[],'bdndx',[],'clustmeth',[],'nclusts',[],'ndim',[],'spwin',...
	[],'mf',[],'rthresh',[],'threshv',[],'threshv2',[],'band_lims',[],'furtherMultipliersTo_mf',[]);