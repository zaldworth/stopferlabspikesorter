function [newmean,sndx,newstim,jnk]=realign2s(stim2,mstim2,anchor,range,s,zero_shift)
% function [newmean,sndx,newstim]=realign2(stim,mstim,anchor, range[,s,zero_shift])
% INPUTS:
% stim: nsamp x dim x (1 or 2) set of stimuli
% mstim: 1 x dim' x (1 or 2) mean, on which the stimulus is aligned.
% anchor: time in stim2 where mstim2 ends
% range: 1 x (1 or 2) allowed range for shifts relative to the anchor. matches 
% considered only in those ranges.
% s = soft range, penalizes larger excursions. Something like STD, so
% suggest hard range > about 2 soft range.
% zero_shift = 0 : don't align the median to 0; =1: align the median shift
% to 0.
% OUTPUTS:
% ndx: current align. added to obtain new align. sndx returns new align,
% can be used to iterate.
% 
% newstim: nsamp x dim x 1 or 2 set of aligned stimuli
% newmean: new mean
% sndx: set of shifts to produce the new mean. shifts relative to anchor.


if nargin<5
    s=Inf; %revert back to hard limits; no penalty
end
if nargin<6
    zero_shift=0;
end

if numel(range)==1
    % this redefine a matlab command : range!
    range=abs(range);
    range = [-range range];
    % one element makes a symmetric range. two are preserved. need to issue
    % an error message for larger ranges.
else
    range=sort(range);
    % get them in order. Still not checking if there are only 2 in range.
end

matches=0;
for i1=1:size(stim2,3)
    matches=matches+filter(fliplr(mstim2(:,:,i1))./norm(mstim2(:,:,i1).^2),1,stim2(:,:,i1)');
    % normalize by mstim2^2, to reduce dependence on size
	% was norm(mstim2(:,:,i1).^2), which was using mstim^4. this way or as
	% now. switched back to the old way...
end % i1
                    
[jnk,sndx]=max(matches((range(1):range(2))+anchor,:)-repmat(((range(1):range(2))./s)'.^2,1,size(matches,2)));
% penalty for shifting too far - the square of the shift. Need a proper normalization 
% at the filtering stage. Now can control it with s - large s is less
% penalty.

if zero_shift
    sndx = sndx - round(median(sndx));
else
    sndx=sndx+range(1)-1;
end

% or should I say, so that there is no net shift,
% if there is no zero net shift, I don't need to retain sndx - it would
% just be re-calculated again next time, and the shift should be smaller.

newstim = stimshift(stim2,size(mstim2,2),anchor,sndx);
%newstim=zeros(size(stim2,1),size(mstim2,2), size(stim2,3));
%for i1=1:length(sndx)   
%    newstim(i1,:,:)=stim2(i1, (-size(newstim,2):-1)+(anchor+sndx(i1)+1),:);
%end

newmean=mean(newstim);
